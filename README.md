# Java Image Binarisation

A parallel implementation of the [NICK binarisation algorithm][nick] in Java.

[nick]: https://doi.org/10.1117/12.805827

## Background

Developed for use in [Transcribo][transcribo] to improve the quality of
Tesseract OCR output on [Arthur Schnitzler Digital Edition][schnitz-edition]
content by providing visibility of and control over the binarisation process.

Tesseract uses the Otsu algorithm — a global thresholding binarisation algorithm
— which performs poorly on Schnitzler (and other less-than-perfect) documents.
The NICK algorithm is a straightforward local binarisation method which does
well on documents with inconsistent backgrounds.

[transcribo]: http://transcribo.org/en/
[schnitz-edition]: http://www.arthur-schnitzler.org/

## Status

This is functional, but still new and evolving. Artefacts are not yet published
to a repository, sorry.

## Components

This repository contains several artefacts:

### `binarise-nick`

The main library implementing the NICK binarisation method along with related
functionality (most importantly `Array2D` used to represent two-dimentional
image data).

See:

* [`ParallelNickBinarisationMethod`][nick-parallel] — A parallel implementation
  which distributes work using a `ForkJoinPool`
* [`DefaultNickBinarisationMethod`][nick-default] — Functionally identical to
  the parallel version, but single-threaded
* [`SchnitzBinarisationMethod`][schnitz] — A collection of several steps to
  binarise colour images, based around NICK
* The [command line interface][cli] for an example of usage

[nick-parallel]: binarise-nick/src/main/java/uk/ac/cam/lib/imageproc/filter/binarise/ParallelNickBinarisationMethod.java
[nick-default]: binarise-nick/src/main/java/uk/ac/cam/lib/imageproc/filter/binarise/DefaultNickBinarisationMethod.java
[schnitz]: binarise-nick/src/main/java/uk/ac/cam/lib/imageproc/filter/binarise/SchnitzBinarisationMethod.java
[cli]: binarise-nick-cmdline/src/main/groovy/uk/ac/cam/lib/imageproc/binarise/cmdline/CommandLineInterface.groovy

### `binarise-nick-cmdline`
A command line program which uses `binarise-nick` (specifically, the
`SchnitzBinarisationMethod`) to binarise images.

### `array2d-awt` and `array2d-swt`

Interoperability between AWT and SWT images and the functionality in
`binarise-nick`.

Functions are provided to convert from AWT and SWT images to the `Array2D`
objects used by the `binarise-nick` library.

### `binarise-nick-benchmarks`
A fairly random set of benchmarks using [JMH] used to inform the implementation.

[JMH]: http://openjdk.java.net/projects/code-tools/jmh/
