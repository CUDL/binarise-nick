"""
Generate JSON representations of the basic images from PngSuite

    http://www.schaik.com/pngsuite/

The <pngsuite-dir> argument should point to a directory containing the
unpacked PngSuite tar/zip.

usage: generate_pngsuite_testdata [options] <pngsuite-dir> [<out-dir>]
"""

import os
from os import path
import json
import tempfile

import docopt
import numpy as np
from scipy import ndimage

basic_files = ['basn0g01', 'basn0g02', 'basn0g04', 'basn0g08', 'basn0g16',
               'basn2c08', 'basn2c16', 'basn3p01', 'basn3p02', 'basn3p04', 
               'basn3p08', 'basn4a08', 'basn4a16', 'basn6a08', 'basn6a16']

class CmdLineError(RuntimeError):
    pass

def create_test_data(name, suite_path):
    img = ndimage.imread(path.join(suite_path, '{}.png'.format(name)))

    max_pixel_value = 2**16 - 1 if name.endswith('16') else 2**8 - 1

    channels = {}

    if img.ndim == 2:
        channels['grey'] = img
    elif img.shape[-1] == 2:
        channels['grey'] = img[:, :, 0]
        channels['alpha'] = img[:, :, 1]
    else:
        assert img.ndim == 3
        assert img.shape[-1] >= 3

        channels['red'] = img[:, :, 0]
        channels['green'] = img[:, :, 1]
        channels['blue'] = img[:, :, 2]
        if img.shape[-1] == 4:
            channels['alpha'] = img[:, :, 3]

    return {
        'size': img.shape[:2],
        'channels': {k: json_test_data(v, max_pixel_value) 
                     for (k, v) in channels.items()}
    }


def json_test_data(channel, max_pixel_value):
    return {
        'max_value': max_pixel_value,
        'values': [int(x) for x in channel.reshape(-1)]
    }


def main():
    args = docopt.docopt(__doc__)

    pngsuite_dir =  args['<pngsuite-dir>']
    out_dir = args['<out-dir>'] or args['<pngsuite-dir>']

    if not path.isdir(out_dir):
        raise CmdLineError('output directory is not a dir: {}'
                           .format(out_dir))

    for name in basic_files:
        fh, location = tempfile.mkstemp()
        with os.fdopen(fh, 'w') as f:
            json.dump(create_test_data(name, pngsuite_dir), f)
            os.rename(location, path.join(out_dir, '{}.json'.format(name)))



if __name__ == '__main__':
    main()
