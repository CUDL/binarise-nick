package uk.ac.cam.lib.imageproc.testimages

import groovy.json.JsonSlurper


@Singleton
class TestImages {

    private static final Set<String> TEST_CASES = [
            'basn0g01', 'basn0g02', 'basn0g04', 'basn0g08', 'basn0g16',
            'basn2c08', 'basn2c16', 'basn3p01', 'basn3p02', 'basn3p04',
            'basn3p08', 'basn4a08', 'basn4a16', 'basn6a08', 'basn6a16'
    ].asImmutable()

    def slurper = new JsonSlurper()

    def getTestCaseNames() {
        TEST_CASES
    }

    private void validateName(name) {
        if(!TEST_CASES.contains(name))
            throw new IllegalArgumentException("Unknown test case: ${name}")
    }

    def getTestImage(String name) {
        validateName(name)
        getClass().getResourceAsStream("${name}.png")
    }

    def getTestJson(String name) {
        validateName(name)
        slurper.parse(getClass().getResourceAsStream("${name}.json"))
    }

    def Iterable<Tuple> getTestImages() {
        return {
            getTestCaseNames().stream().map({
                name -> new Tuple(name, getTestImage(name), getTestJson(name))
            })
            .iterator();
        }
    }

    def normalise(channel) {
        channel.values.collect { int val -> val / (channel.max_value as float) }
    }
}
