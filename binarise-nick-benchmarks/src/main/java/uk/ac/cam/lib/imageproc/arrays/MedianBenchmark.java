package uk.ac.cam.lib.imageproc.arrays;

import org.openjdk.jmh.annotations.*;
import uk.ac.cam.lib.imageproc.arrays.Arrays;

import java.util.Random;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class MedianBenchmark {

    @Param({"1234"})
    public long seed;

    @Param({"1", "2", "3", "9", "18", "19", "100", "101", "1000", "1001",
            "100000", "100001"})
    public int arrayLength;

    private float[] data;
    private float[] input;

    @Setup
    public void setup() {
        if(arrayLength < 1)
            throw new IllegalArgumentException(
                    "arrayLength must be at least 1, got: " + arrayLength);

        Random r = new Random(seed);

        float[] values = new float[arrayLength];
        for(int i = 0; i < values.length; ++i) {
            values[i] = r.nextFloat();
        }
        data = values;
        input = new float[arrayLength];
    }

    /**
     * Get the the test array with data reset to the initial random order.
     */
    private float[] getInput() {
        // Reset the state of input to match data each time, otherwise we'll
        // be sorting an already-sorted array each iteration, leading to skewed
        // results.
        System.arraycopy(data, 0, input, 0, data.length);
        return input;
    }

    @Benchmark
    public float benchNormalMedian() {
        return Arrays.medianInPlace(getInput());
    }

    @Benchmark
    public float benchSelectionMedian() {
        return Arrays.medianInPlace_selection(getInput());
    }
}
