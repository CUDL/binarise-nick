package uk.ac.cam.lib.imageproc;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;


public class ModBenchmarks {

    @BenchmarkMode(Mode.Throughput)
    @State(Scope.Benchmark)
    public static class IntModBenchmark {

        @Param({"9", "9738"})
        public int divisor;

        @Param({"8", "9862"})
        public int dividend;

        @Benchmark
        public int benchModOperator() {
            return dividend % divisor;
        }

        @Benchmark
        public int benchFloorMod() {
            return Math.floorMod(dividend, divisor);
        }

        @Benchmark
        public int benchMod() {
            return Maths.floorMod(dividend, divisor);
        }

        @Benchmark
        public int bench_Mod() {
            return _mod(dividend, divisor);
        }
    }

    @BenchmarkMode(Mode.Throughput)
    @State(Scope.Benchmark)
    public static class FloatModBenchmark {

        @Param({"1.2", "-1.3"})
        public float divisor;

        @Param({"0.9", "1.84"})
        public float dividend;

        @Benchmark
        public float benchModOperator() {
            return dividend % divisor;
        }

        @Benchmark
        public float benchModBasedMod() {
            return modBasedMod(dividend, divisor);
        }

        @Benchmark
        public float bench_Mod() {
            return _mod(dividend, divisor);
        }

        @Benchmark
        public float benchFloorMod() {
            return Maths.floorMod(dividend, divisor);
        }

        @Benchmark
        public float bench_Mod2() {
            return _mod2(dividend, divisor);
        }
    }

    /**
     * {@code a % b} where the sign of the result follows that of {@code b}.
     */
    public static int modBasedMod(int a, int b) {
        int result = a % b;
        if((b > 0 && a < 0) || (b < 0 && a > 0))
            return (result + b) % b;
        return result;
    }

    private static final int FLOAT_SIGN_MASK = 0x01 << 31;

    /**
     * {@code a % b} where the sign of the result follows that of {@code b}.
     */
    public static float modBasedMod(float a, float b) {
        float result = a % b;
        if((Float.floatToIntBits(a) & FLOAT_SIGN_MASK) !=
                (Float.floatToIntBits(b) & FLOAT_SIGN_MASK))
            return (result + b) % b;
        return result;
    }

    /**
     * {@code a % b} where the sign of the result follows that of {@code b}.
     */
    public static int _mod(int a, int b) {
        return a - b * Math.floorDiv(a, b);
    }

    public static float _mod(float a, float b) {
        // negative zero not produced
        return a - b * (float)Math.floor(a / b);
    }

    public static float _mod2(float a, float b) {
        // negative zero not produced
        return a - b * Math.round((a / b) - 0.5f);
    }
}
