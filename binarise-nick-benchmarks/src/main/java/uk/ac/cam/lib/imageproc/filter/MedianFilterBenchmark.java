package uk.ac.cam.lib.imageproc.filter;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.Blackhole;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.GridArrayReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;
import uk.ac.cam.lib.imageproc.filter.DefaultMedianFilter;
import uk.ac.cam.lib.imageproc.filter.MedianFilter;
import uk.ac.cam.lib.imageproc.filter.ParallelMedianFilter;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class MedianFilterBenchmark {

    public long seed = 42;

    public int testImageWidth = 256;
    public int testImageHeight = 256;

    @Param({"1", "2", "4", "6", "8", "common-pool"})
    public String parallelism;

    public int maxSynchronous = 1024;

    @Param({"parallel", "default"})
    public String implementation;

    @Param({"simple", "gridarray"})
    public String samplingMethodName;

    public int windowWidth = 20;
    public int windowHeight = 20;


    public SamplingMethod<float[]> samplingMethod;

    private MedianFilter<float[]> medianFilter;

    private Array2D<float[]> testImage;
    private Array2D<float[]> dest;

    private Array2D<float[]> createTestImage(int width, int height, long seed) {

        Random r = new Random(seed);

        float[] pixels = new float[width * height];
        for(int i = 0; i < pixels.length; ++i) {
            pixels[i] = r.nextFloat();
        }

        return Array2D.withData(float[].class, pixels, width);
    }

    private ForkJoinPool getForkJoinPool() {
        if(parallelism.equals("common-pool"))
            return ForkJoinPool.commonPool();

        return new ForkJoinPool(Integer.parseInt(parallelism));
    }

    private boolean isParallelRun() {
        return getForkJoinPool().getParallelism() > 1;
    }

    @Setup
    public void setup() {
        ForkJoinPool pool = getForkJoinPool();

        if("simple".equals(samplingMethodName))
            samplingMethod = new ReflectSamplingMethod();
        else if("gridarray".equals(samplingMethodName))
            samplingMethod = new GridArrayReflectSamplingMethod();
        else
            throw new IllegalArgumentException("Unknown samplingMethodName: " +
                    samplingMethodName);

        switch(implementation) {
            case "parallel":
                this.medianFilter = new ParallelMedianFilter(
                        windowWidth, windowHeight, samplingMethod,
                        pool, maxSynchronous);
                break;
            case "default":
                // Avoid testing > 1 thread with non-parallel implementation
                if(isParallelRun())
                    System.exit(0);

                this.medianFilter = new DefaultMedianFilter(
                        windowWidth, windowHeight, samplingMethod);
                break;
            default:
                throw new AssertionError("Unknown implementation: " +
                        implementation);
        }

        this.testImage = createTestImage(testImageWidth, testImageHeight, seed);
        this.dest = Array2D.allocate(
                float[].class, testImageWidth, testImageHeight);
    }

    @TearDown
    public void tearDown() {
        this.medianFilter = null;
    }

    @Benchmark
    public void benchmark(Blackhole bh) {
        bh.consume(medianFilter.apply(testImage, dest));
    }
}
