package uk.ac.cam.lib.imageproc.objectlabel;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter;
import uk.ac.cam.lib.imageproc.objectlabel.RegionLabelFilter;
import uk.ac.cam.lib.imageproc.interop.awt.Array2DIoAwt;
import uk.ac.cam.lib.imageproc.objectlabel.ObjectBoxer;

import javax.imageio.ImageIO;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter.Connectivity.FULL;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class RegionLabelingBenchmark {

    private Array2D<byte[]> input;
    private Array2D<int[]> output;
    private DefaultRegionLabelFilter regionLabelFilter;

    private Array2D<int[]> compressLabelRangeData;
    private Array2D<int[]> compressLabelRangeInput;

    @Setup
    public void setup() throws IOException {
        BufferedImage img = ImageIO.read(
                new File("/tmp/MS-A-00020-00010-000-00015.bin.png"));

        Array2D<float[]> ia = new Array2DIoAwt().getNormalisedChannels(
                img.getData(new Rectangle(0, 0, 256, 256))).get(0);

        input = Array2D.allocateSameShape(byte[].class, ia);
        float[] iad = ia.data();
        byte[] indata = input.data();

        for(int i = 0; i < iad.length; ++i) {
            indata[i] = (byte)(iad[i] <= 0.5 ? 1 : 0);
        }

        output = Array2D.allocateSameShape(int[].class, input);
        regionLabelFilter = new DefaultRegionLabelFilter(FULL, (byte)0);

        compressLabelRangeData = Array2D.allocateSameShape(int[].class, input);
        compressLabelRangeInput = Array2D.allocateSameShape(int[].class, input);
        regionLabelFilter.apply(input, compressLabelRangeData);
    }

    @Benchmark
    public void label(Blackhole bh) {
        bh.consume(regionLabelFilter.apply(input, output));
    }

    @Benchmark
    public void compressLabelRange(Blackhole bh) {
        System.arraycopy(compressLabelRangeData.data(), 0,
                         compressLabelRangeInput.data(), 0,
                         compressLabelRangeData.size());

        bh.consume(RegionLabelFilter.compressLabelRange(
                compressLabelRangeInput));
    }

    @Benchmark
    public void getObjectBounds(Blackhole bh) {
        System.arraycopy(compressLabelRangeData.data(), 0,
                compressLabelRangeInput.data(), 0,
                compressLabelRangeData.size());

        bh.consume(ObjectBoxer.getObjectBounds(compressLabelRangeInput));
    }
}
