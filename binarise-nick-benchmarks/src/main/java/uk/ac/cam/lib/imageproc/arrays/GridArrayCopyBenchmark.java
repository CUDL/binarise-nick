package uk.ac.cam.lib.imageproc.arrays;


import org.openjdk.jmh.annotations.*;
import uk.ac.cam.lib.imageproc.arrays.GridArrays;

import java.util.Random;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Thread)
public class GridArrayCopyBenchmark {

    public long seed = 42;

    private float[] src, dest;
    private int srcWidth;

    @Param({"128", "64", "16", "4", "1"})
    private int destWidth;

    @Param({"true", "false"})
    private boolean flipY;

    @Param({"true", "false"})
    private boolean flipX;

    private int sx, sy, dxa, dxb, dya, dyb;

    @Setup
    public void setup() {
        Random r = new Random(seed);
        srcWidth = 256;
        src = new float[srcWidth * srcWidth];
        for(int i = 0; i < src.length; ++i)
            src[i] = i;

        dest = new float[destWidth * destWidth];

        sx = (srcWidth / 2) - (destWidth / 2);
        sy = (srcWidth / 2) - (destWidth / 2);

        if(flipX) {
            dxb = 0;
            dxa = destWidth - 1;
        }
        else {
            dxa = 0;
            dxb = destWidth - 1;
        }

        if(flipY) {
            dyb = 0;
            dya = destWidth - 1;
        }
        else {
            dya = 0;
            dyb = destWidth - 1;
        }

    }

    @Benchmark
    public void benchElementwise() {
        GridArrays.copyElementwise(
                src, srcWidth, sx, sy,
                dest, destWidth, dxa, dya, dxb, dyb);
    }

    @Benchmark
    public void benchWithArrayCopy() {
        GridArrays.copyWithArrayCopy(
                src, srcWidth, sx, sy,
                dest, destWidth, dxa, dya, dxb, dyb);
    }
}
