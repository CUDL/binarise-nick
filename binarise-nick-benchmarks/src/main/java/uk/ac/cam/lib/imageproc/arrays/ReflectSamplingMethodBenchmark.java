package uk.ac.cam.lib.imageproc.arrays;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.GridArrayReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Benchmark)
public class ReflectSamplingMethodBenchmark {

    @Param({"simple", "gridarray"})
    public String samplingMethodName;

    @Param({"left", "middle", "right"})
    public String filterPositionHorizontal;

    @Param({"top", "middle", "bottom"})
    public String filterPositionVertical;

    private SamplingMethod<float[]> samplingMethod;

    private Array2D<float[]> src, dest;

    private int filterX, filterY;

    @Setup
    public void setup() {
        if("simple".equals(samplingMethodName))
            samplingMethod = new ReflectSamplingMethod();
        else if("gridarray".equals(samplingMethodName))
            samplingMethod = new GridArrayReflectSamplingMethod();
        else
            throw new IllegalArgumentException("Unknown samplingMethodName: " +
                    samplingMethodName);

        src = Array2D.allocate(float[].class, 512, 512);
        dest = Array2D.allocate(float[].class, 64, 64);

        switch(filterPositionHorizontal) {
            case "left": filterX = 0; break;
            case "middle": filterX = src.width() / 2; break;
            case "right": filterX = src.width() - 1; break;
            default: throw new AssertionError();
        }

        switch(filterPositionVertical) {
            case "top": filterY = 0; break;
            case "middle": filterY = src.height() / 2; break;
            case "bottom": filterY = src.height() - 1; break;
            default: throw new AssertionError();
        }
    }

    @Benchmark
    public void bench(Blackhole bh) {
        bh.consume(samplingMethod.sample(
                src, filterX, filterY, dest.width(), dest.height(), dest));
    }
}
