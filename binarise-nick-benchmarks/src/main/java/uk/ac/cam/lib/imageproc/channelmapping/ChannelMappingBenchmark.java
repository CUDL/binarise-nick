package uk.ac.cam.lib.imageproc.channelmapping;


import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class ChannelMappingBenchmark {

    @Param({"2048"})
    public int width;

    @Param({"2048"})
    public int height;

    @Param({"1", "8", "128", "1024", "2048"})
    public int bufferSize;

    private List<Array2D<int[]>> src;
    private List<Array2D<float[]>> dest;
    private ChannelMapperFactory<int[], float[]> cmf;

    @Setup
    public void setup() {
        Random r = new Random(1234);

        src = IntStream.range(0, 3).mapToObj(x ->
            Array2D.withData(
                    int[].class, r.ints(width * height, 0, 256).toArray(),
                    width)
        ).collect(Collectors.toList());

        dest = Collections.singletonList(
                Array2D.allocate(float[].class, width, height));

        cmf = IntegerToFloatChannelMapper.factory(255, 255, 255)
            .bridgeTo(Greyscale.ColorimetricGreyscaleChannelMapper.factory(), float[].class);

        assert cmf.inputChannelCount() == 3;
        assert cmf.outputChannelCount() == 1;
    }

    @Benchmark
    public void map(Blackhole bh) {

        bh.consume(ChannelMapping.map(cmf, src, dest, bufferSize));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(ChannelMappingBenchmark.class.getSimpleName())
                .forks(0)
                .build();

        new Runner(opt).run();
    }
}
