package uk.ac.cam.lib.imageproc.interop.awt

import spock.lang.Shared
import spock.lang.Specification
import uk.ac.cam.lib.imageproc.testimages.TestImages

import javax.imageio.ImageIO
import java.awt.*
import java.awt.image.DataBufferByte
import java.awt.image.Raster
import java.util.stream.Stream
import java.util.stream.StreamSupport

class Array2DIoAwtTest extends Specification {

    @Shared TestImages ti = TestImages.instance
    @Shared Array2DIoAwt io = new Array2DIoAwt();

    def "mapped channel count matches image bands"(
            int w, int h, int channels) {

        given:
        def raster = Raster.createBandedRaster(new DataBufferByte(
                new byte[channels][w * h], w * h), w, h, 1,
                (0..channels - 1) as int[], [0] * channels as int[],
                new Point(0, 0));

        when:
        def channelArrays = io.getNormalisedChannels(raster)

        then:
        channelArrays.size() == channels

        where:
        [w, h, channels] << [[1, 2, 4], [1, 2, 5], [1, 2, 4]].combinations()
    }

    def "mapped channel array shape matches image shape"(
            int w, int h, int channels) {

        given:
        def raster = Raster.createBandedRaster(new DataBufferByte(
                new byte[channels][w * h], w * h), w as int, h as int, 1,
                (0..channels - 1) as int[], [0] * channels as int[],
                new Point(0, 0));

        when:
        def channelArrays = io.getNormalisedChannels(raster)

        then:
        channelArrays.each { ca ->
            assert ca.width() == w
            assert ca.height() == h
        }

        where:
        [w, h, channels] << [[1, 2, 4], [1, 2, 5], [1, 2, 4]].combinations()
    }

    def "integer values are normalised to [0, 1]"() {
        given:
        def db = new DataBufferByte([0, 127, 255] as byte[], 3)
        def raster = Raster.createBandedRaster(
                db, 3, 1, 3, [0] as int[], [0] as int[], new Point(0, 0))

        when:
        def (c1) = io.getNormalisedChannels(raster)

        then:
        assert c1.data() == [0, 127/255, 1] as float[]
    }

    def "getNormalisedChannels creates arrays matching input data"() {
        given:
        def img = ImageIO.read(rawImage)

        when:
        def channels = io.getNormalisedChannels(img.data)

        then:
        if(channels.size() < 3) {
            channels[0] == ti.normalise(imageData.channels.grey)
        }
        else {
            channels[0] == ti.normalise(imageData.channels.red)
            channels[1] == ti.normalise(imageData.channels.green)
            channels[2] == ti.normalise(imageData.channels.blue)
        }

        if(channels.size() % 2 == 0) {
            channels[-1] == ti.normalise(imageData.channels.alpha)
        }

        where:
        [name, rawImage, imageData] << applicableTestImages
    }

    def getApplicableTestImages() {
        stream(ti.getTestImages()).filter({
            data ->
                def (name) = data
                !(name ==~ /basn3p\d\d/ || name ==~ /basn\w\w16/)
        })
        .iterator()
    }

    def <T> Stream<T> stream(Iterable<T> iterable) {
        StreamSupport.stream(iterable.spliterator(), false)
    }
}
