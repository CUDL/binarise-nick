package uk.ac.cam.lib.imageproc.interop.awt;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapper;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapperFactory;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping;
import uk.ac.cam.lib.imageproc.channelmapping.FloatToByteChannelMapper;
import uk.ac.cam.lib.imageproc.channelmapping.IntegerToFloatChannelMapper;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.lang.Math.min;
import static uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping.DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE;
import static uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping.createTransferBuffers;

public class Array2DIoAwt {

    private final int channelMapperBufferSize;

    /**
     * Create an Array2DIoAwt instance with the
     * {@link ChannelMapping#DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE} of
     * {@value ChannelMapping#DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE}.
     */
    public Array2DIoAwt() {
        this(DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE);
    }

    public Array2DIoAwt(int channelMapperBufferSize) {
        if(channelMapperBufferSize < 1)
            throw new IllegalArgumentException(
                    "channelMapperBufferSize must be at least 1, was: " +
                            channelMapperBufferSize);

        this.channelMapperBufferSize = channelMapperBufferSize;
    }

    public <D> List<Array2D<D>> getMappedChannels(
            Raster raster, ChannelMapperFactory<float[], D> mapper,
            Class<D> destClass) {

        return getMappedChannels(
                raster, mapper, ArrayTypes.arrayType(destClass));
    }

    public <D> List<Array2D<D>> getMappedChannels(
            Raster raster, ChannelMapperFactory<float[], D> mapper,
            ArrayType<D> destType) {

        List<Array2D<D>> dest = createDestArraysFor(
                raster, mapper.outputChannelCount(), destType);

        return getMappedChannels(raster, mapper, dest);
    }

    public <D> List<Array2D<D>> getMappedChannels(
            Raster raster, ChannelMapperFactory<float[], D> mapper,
            List<Array2D<D>> dest) {

        ChannelMapperFactory<int[], D> fullMapper =
                intToFloatMapperFor(raster).bridgeTo(mapper, float[].class);

        return _getMappedChannels(raster, fullMapper, dest);
    }

    public List<Array2D<float[]>> getNormalisedChannels(Raster raster) {

        return _getMappedChannels(
                raster, intToFloatMapperFor(raster),
                createDestArraysFor(raster, raster.getNumBands(),
                                    float[].class));
    }

    private static <T> List<Array2D<T>> createDestArraysFor(
            Raster r, int count, Class<T> cls) {
        return createDestArraysFor(r, count, ArrayTypes.arrayType(cls));
    }

    private static <T> List<Array2D<T>> createDestArraysFor(
            Raster r, int count, ArrayType<T> type) {

        return ChannelMapping.createDestArrays(
                r.getWidth(), r.getHeight(), count, type);
    }

    private static ChannelMapperFactory<int[], float[]> intToFloatMapperFor(
            Raster r) {

        return IntegerToFloatChannelMapper.factory(
                bandMaxValues(r.getSampleModel()));
    }

    private static void ensureIntegerDataBuffer(DataBuffer db) {
        int type = db.getDataType();
        if(!(type == DataBuffer.TYPE_BYTE || type == DataBuffer.TYPE_INT ||
                type == DataBuffer.TYPE_SHORT)) {
            throw new RuntimeException(String.format(
                    "Unsupported DataBuffer type: %d (%s)",
                    type, db.getClass()));
        }
    }

    private <D> List<Array2D<D>> _getMappedChannels(
            Raster raster, ChannelMapperFactory<int[], D> mapper,
            List<Array2D<D>> destArrays) {

        Objects.requireNonNull(raster);
        Objects.requireNonNull(mapper);

        ensureIntegerDataBuffer(raster.getDataBuffer());

        int bands = raster.getNumBands();

        if(destArrays.size() < 1)
            throw new IllegalArgumentException("No destination arrays provided");

        ArrayType<D> destType = destArrays.get(0).type();

        int width = raster.getWidth(), height = raster.getHeight();
        int length = width * height;

        int[] pixels = raster.getPixels(
                0, 0, width, height, new int[length * bands]);

        int[][] channels = new int[bands][channelMapperBufferSize];
        List<int[]> channelBufferSrc =
                Collections.unmodifiableList(Arrays.asList(channels));

        List<D> channelBufferDst = createTransferBuffers(
                destType.arrayType(), destArrays.size(),
                channelMapperBufferSize);

        ChannelMapper<int[], D> map =
                mapper.getMapper(channelMapperBufferSize);

        int pixelsPerBlock = channelMapperBufferSize * bands;
        for(int j = 0; j < pixels.length; j += pixelsPerBlock) {

            // Populate transfer buffer
            int count = min(j + pixelsPerBlock, pixels.length) - j;
            assert count % bands == 0;
            int channelCount = count / bands;

            for(int c = 0; c < channels.length; ++c) {
                int[] channel = channels[c];

                for(int i = 0,
                        end = channelCount,
                        ii = j + c; i < end; ++i, ii += bands) {
                    channel[i] = pixels[ii];
                }
            }

            map.mapChannels(channelBufferSrc, channelBufferDst, channelCount);

            // Copy dest buffer into dest arrays
            for(int i = 0,
                    pos = j / bands;
                i < destArrays.size(); ++i) {
                System.arraycopy(channelBufferDst.get(i), 0,
                                 destArrays.get(i).data(), pos, channelCount);
            }
        }

        return destArrays;
    }

    private static int[] bandMaxValues(SampleModel sm) {
        int[] sizes = sm.getSampleSize();
        for(int i = 0; i < sizes.length; ++i)
            sizes[i] = (1 << sizes[i]) - 1;

        return sizes;
    }

    /**
     * Create a 1-bit binary image from a byte array.
     *
     * <p>byte values are {@code % 2}, after which 1-valued pixels are white and
     * 0-valued pixels are black.
     *
     * @param pixels The pixel colour values
     * @return A binary image of the same size as the pixels array.
     */
    public static BufferedImage binaryImage(Array2D<byte[]> pixels) {
        BufferedImage img = new BufferedImage(
                pixels.width(), pixels.height(), BufferedImage.TYPE_BYTE_BINARY);
        WritableRaster r = img.getWritableTile(0, 0);
        assert r.getWidth() == img.getWidth();
        assert r.getHeight() == img.getHeight();

        r.setDataElements(0, 0, img.getWidth(), img.getHeight(), pixels.data());
        return img;
    }

    public static BufferedImage greyscaleImageFromFloats(
            Array2D<float[]> pixels) {

        return greyscaleImageFromBytes(ChannelMapping.map(
                FloatToByteChannelMapper.factory(1), pixels, byte[].class));
    }

    public static BufferedImage greyscaleImageFromBytes(Array2D<byte[]> pixels) {
        BufferedImage img = new BufferedImage(
                pixels.width(), pixels.height(), BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster r = img.getWritableTile(0, 0);
        assert r.getWidth() == img.getWidth();
        assert r.getHeight() == img.getHeight();

        r.setDataElements(0, 0, img.getWidth(), img.getHeight(), pixels.data());
        return img;
    }
}
