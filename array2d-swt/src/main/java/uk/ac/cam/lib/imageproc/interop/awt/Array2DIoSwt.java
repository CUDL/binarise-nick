package uk.ac.cam.lib.imageproc.interop.awt;


import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapper;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapperFactory;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping;
import uk.ac.cam.lib.imageproc.channelmapping.IntegerToFloatChannelMapper;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.lang.Math.min;
import static uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping.createTransferBuffers;
import static uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping.drainTransferBuffers;

public class Array2DIoSwt {

    private static final int CHANNEL_MAPPER_BUFFER_SIZE = 256;

    private final int channelMapperBufferSize;

    public Array2DIoSwt() {
        this(CHANNEL_MAPPER_BUFFER_SIZE);
    }

    public Array2DIoSwt(int channelMapperBufferSize) {
        if(channelMapperBufferSize < 1)
            throw new IllegalArgumentException(
                    "channelMapperBufferSize was < 1: " +
                            channelMapperBufferSize);

        this.channelMapperBufferSize = channelMapperBufferSize;
    }

    public int getChannelMapperBufferSize() {
        return channelMapperBufferSize;
    }

    private static boolean isGreyscale(PaletteData palette) {
        if(palette.isDirect)
            return false;

        for(RGB r : Objects.requireNonNull(palette.colors))
            if(!(r.red == r.green && r.green == r.blue))
                return false;

        return true;
    }

    private static boolean hasAlpha(ImageData data) {
        return data.alphaData != null;
    }

    private static int channelCount(ImageData data) {
        return (isGreyscale(data.palette) ? 1 : 3) + (hasAlpha(data) ? 1 : 0);
    }

    public List<Array2D<float[]>> getNormalisedChannels(ImageData data) {
        int channels = channelCount(data);

        List<Array2D<float[]>> dest = ChannelMapping.createDestArrays(
                data.width, data.height, channels, float[].class);

        int[] maxValues = new int[channels];
        Arrays.fill(maxValues, 255);

        return _getMappedChannels(
                data, IntegerToFloatChannelMapper.factory(maxValues), dest);
    }

    private <T> List<Array2D<T>> _getMappedChannels(
            ImageData data, ChannelMapperFactory<int[], T> mapperFactory,
            List<Array2D<T>> destArrays) {

        Objects.requireNonNull(data);
        Objects.requireNonNull(mapperFactory);
        Objects.requireNonNull(destArrays);

        if(destArrays.size() < 1 || destArrays.size() > 4)
            throw new IllegalArgumentException(
                    "between 1 and 4 dest arrays must be provided. Got: " +
                            destArrays.size());
        if(hasAlpha(data) && destArrays.size() % 2 != 0)
            throw new IllegalArgumentException(
                    "Received output array for alpha but image has no alpha");

        // TODO: handle typical 8-bit case w/o get pixels

        ChannelMapper<int[], T> mapper = mapperFactory.getMapper(
                channelMapperBufferSize);

        List<T> transferBufferDest = createTransferBuffers(
                destArrays.get(0).type().arrayType(),
                destArrays.size(), channelMapperBufferSize);

        List<int[]> transferBuffer = createTransferBuffers(
                int[].class, destArrays.size(), channelMapperBufferSize);

        int[] tbr = transferBuffer.get(0);
        int[] tbg = transferBuffer.size() > 2 ? transferBuffer.get(1) : null;
        int[] tbb = transferBuffer.size() > 2 ? transferBuffer.get(2) : null;
        int[] tba = transferBuffer.size() % 2 == 0 ?
                transferBuffer.get(transferBuffer.size() - 1) : null;

        boolean isIndexed = !data.palette.isDirect;
        byte[] alphas = tba == null ? null : new byte[channelMapperBufferSize];

        int width = data.width;
        RGB[] index = data.palette.colors;

        for(int j = 0,
                totalPixels = data.width * data.height;
            j < totalPixels; j += channelMapperBufferSize) {

            // Populate transfer buffers
            int count = min(j + channelMapperBufferSize, totalPixels) - j;

            int x = j % width;
            int y = j / width;

            data.getPixels(x, y, count, tbr, 0);

            if(isIndexed) {
                if(tbg == null) {
                    // Greyscale indexed colour image (this covers all greyscale
                    // images). Even 8-bit greyscale images are represented as
                    // index colour.

                    for(int i = 0,
                        maxI = i + count;
                        i < maxI; ++i) {

                        // tbr is the single luminosity output channel. At this
                        // point it holds indices into the colour pallete index.
                        // This is a greyscale image, so r,g,b are equal.
                        tbr[i] = index[tbr[i]].red;
                    }
                }
                else {
                    // RGB indexed colour image
                    for(int i = 0,
                        maxI = i + count;
                        i < maxI; ++i) {
                        RGB colour = index[tbr[i]];
                        tbr[i] = colour.red;
                        tbg[i] = colour.green;
                        tbb[i] = colour.blue;
                    }
                }
            }
            else {
                for (int i = 0,
                     maxI = i + count;
                     i < maxI; ++i) {
                    // At this point tbr holds the packed 24-bit rgb pixel value
                    int pixel = tbr[i];
                    tbr[i] = (pixel >> 16) & 0xff;
                    tbg[i] = (pixel >> 8) & 0xff;
                    tbb[i] = pixel & 0xff;
                }
            }

            if(alphas != null) {
                data.getAlphas(x, y, count, alphas, 0);

                for(int i = 0,
                        maxI = i + count;
                    i < maxI; ++i) {
                    tba[i] = alphas[i] & 0xff;
                }
            }

            mapper.mapChannels(transferBuffer, transferBufferDest, count);
            drainTransferBuffers(transferBufferDest, destArrays, j, count);
        }

        return destArrays;
    }

    public static ImageData binaryImage(Array2D<byte[]> pixels) {
        Objects.requireNonNull(pixels);

        // Represent the binary image as an indexed image with a palette
        // containing a black and white value at index 0 and 1 respectively.
        ImageData data = new ImageData(pixels.width(), pixels.height(), 1,
                new PaletteData(new RGB[]{
                        new RGB(0, 0, 0),
                        new RGB(255, 255, 255)}));

        data.setPixels(0, 0, pixels.size(), pixels.data(), 0);

        return data;
    }
}
