package uk.ac.cam.lib.imageproc.interop.awt

import org.eclipse.swt.graphics.ImageData
import org.eclipse.swt.graphics.ImageLoader
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import uk.ac.cam.lib.imageproc.arrays.Array2D
import uk.ac.cam.lib.imageproc.testimages.TestImages

class Array2DIoSwtTest extends Specification {

    @Shared
    private TestImages ti = TestImages.instance

    def "channel mapper buffer size must be at least 1"() {
        when:
        new Array2DIoSwt(bufferSize)

        then:
        thrown(IllegalArgumentException)

        where:
        bufferSize << [-5, -1, 0]
    }

    def "channel mapper size matches constructor value"() {
        when:
        def io = new Array2DIoSwt(bufferSize)

        then:
        io.channelMapperBufferSize == bufferSize

        where:
        bufferSize << [1, 5, 1000]
    }

    // FIXME: Move this to image-test-data tests but implemented w/ AWT
    def "raw image size matches image data size"() {
        given:
        def (ImageData img) = new ImageLoader().load(rawImage)

        expect:
        [img.width, img.height] == imageData.size

        where:
        [name, rawImage, imageData] << ti.testImages
    }

    @Unroll("getNormalisedChannels() loads normalised version of test image #name correctly")
    def "getNormalisedChannels creates arrays matching input data"() {
        given:
        def (ImageData img) = new ImageLoader().load(rawImage)
        Array2DIoSwt io = new Array2DIoSwt(10)

        when:
        def channels = io.getNormalisedChannels(img)

        // SWT loads 8-bit greyscale w/ alpha images as if they're 32 bit...
        if(channels.size() == 4 && imageData.channels.grey) {
            imageData.channels.red = imageData.channels.grey
            imageData.channels.green = imageData.channels.grey
            imageData.channels.blue = imageData.channels.grey
        }

        then:
        if(channels.size() < 3) {
            channels[0] == ti.normalise(imageData.channels.grey)
        }
        else {
            channels[0] == ti.normalise(imageData.channels.red)
            channels[1] == ti.normalise(imageData.channels.green)
            channels[2] == ti.normalise(imageData.channels.blue)
        }

        if(channels.size() % 2 == 0) {
            channels[-1] == ti.normalise(imageData.channels.alpha)
        }

        where:
        [name, rawImage, imageData] << ti.getTestImages()
    }

    def "binaryImage() maintains pixel values"() {
        given:
        def array = Array2D.withData(byte[], [
            0, 1, 1, 0,
            1, 0, 0, 0,
            1, 1, 1, 1,
            0, 0, 0, 0,
            1, 0, 1, 0
        ] as byte[], 4)

        when:
        def data = new Array2DIoSwt().binaryImage(array)

        then:
        data.palette.isDirect == false

        for(x in 0..<array.width()) {
            for(y in 0..<array.height()) {
                def colour = data.palette.RGBs[data.getPixel(x, y)]

                if(array.get(x, y, Byte.TYPE) == 0) {
                    colour.red == 0
                    colour.green == 0
                    colour.blue == 0
                }
                else {
                    colour.red == 255
                    colour.green == 255
                    colour.blue == 255
                }
            }
        }
    }
}
