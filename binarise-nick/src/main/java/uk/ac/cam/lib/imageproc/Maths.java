package uk.ac.cam.lib.imageproc;


public final class Maths {

    /**
     * {@code a % b} where the sign of the result follows that of {@code b}.
     */
    public static int floorMod(int a, int b) {
        return Math.floorMod(a, b);
    }

    /**
     * {@code a % b} where the sign of the result follows that of {@code b}.
     */
    public static float floorMod(float a, float b) {
        return Math.copySign(a - b * (float)Math.floor(a / b), b);
    }

    private Maths() { throw new RuntimeException(); }
}
