package uk.ac.cam.lib.imageproc.channelmapping;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.filter.MultiArray2DFilter;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class ChannelMapperMultiArray2DFilter<S, D> implements MultiArray2DFilter<S, D> {

    private final ChannelMapperFactory<S, D> channelMapperFactory;
    private final ArrayType<D> destType;

    public ChannelMapperMultiArray2DFilter(
            ChannelMapperFactory<S, D> channelMapperFactory,
            ArrayType<D> destType) {

        this.channelMapperFactory = requireNonNull(channelMapperFactory);
        this.destType = requireNonNull(destType);
    }

    @Override
    public ArrayType<D> destType() {
        return destType;
    }

    @Override
    public int getInputCount() {
        return channelMapperFactory.inputChannelCount();
    }

    @Override
    public int getOutputCount() {
        return channelMapperFactory.outputChannelCount();
    }

    @Override
    public List<Array2D<D>> apply(List<Array2D<S>> src, List<Array2D<D>> dest) {
        return ChannelMapping.map(channelMapperFactory, src, dest);
    }
}
