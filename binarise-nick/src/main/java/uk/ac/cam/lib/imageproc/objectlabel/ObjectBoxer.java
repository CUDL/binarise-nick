package uk.ac.cam.lib.imageproc.objectlabel;

import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Arrays;
import java.util.Objects;

/**
 * Find the bounding boxes of objects in a labeled array.
 */
public final class ObjectBoxer {
    private ObjectBoxer() {}

    /**
     * As {@link #getObjectBounds(Array2D, int)} except the label range in the
     * image is compressed using
     * {@link RegionLabelFilter#compressLabelRange(Array2D)} before finding
     * bounding rectangles.
     */
    public static Array2D<int[]> getObjectBounds(Array2D<int[]> labeledImage) {
        RegionLabelFilter.CompressedLabelRange clr =
                RegionLabelFilter.compressLabelRange(labeledImage);

        return getObjectBounds(
                clr.getCompressedLabelImage(), clr.getMaxLabel());
    }

    /**
     * Determine the bounding rectangle of labeled objects in labeledImage.
     *
     * <p>{@code labledImage} is an image where pixels have integer values
     * representing the label assigned to the pixels sharing the same label
     * value. This function determines (for each label value) the axis-aligned
     * bounding rectangle which contains all pixels with the same label value.
     *
     * <p>The result is a 2D array of width 4 and height equal to the number of
     * labels where each row is the bounding box for the label with the row's
     * {@code y} position. The 4 columns are the top-left x and y coordinates
     * and bottom-right x and y coordinates in that order.
     *
     * <p>Because the return value is a non-sparse array, it makes sense to
     * ensure there are no gaps in the sequence of labels present in the image.
     * This can be achieved using
     * {@link RegionLabelFilter#compressLabelRange(Array2D)}, or the
     * {@link #getObjectBounds(Array2D)} variant of this function.
     *
     * @param labeledImage The image with label values assigned to pixels
     * @param maxLabel The value of the largest label in the image
     * @return The bounding rectangle of each label
     */
    public static Array2D<int[]> getObjectBounds(
            Array2D<int[]> labeledImage, int maxLabel) {

        Objects.requireNonNull(labeledImage);

        Array2D<int[]> bounds = Array2D.allocate(int[].class, 4, maxLabel + 1);

        int[] labelData = labeledImage.data();
        int[] boundsData = bounds.data();
        Arrays.fill(boundsData, -1);
        int w = labeledImage.width();
        int h = labeledImage.height();
        for(int i = 0, y = 0; y < h; ++y) {
            for(int x = 0; x < w; ++x, ++i) {
                int label = labelData[i];

                if(label < 0 || label > maxLabel)
                    throw new IllegalArgumentException(
                            "label out of range: " + label);

                int offset = label * 4;
                // left x
                boundsData[offset] = minIfNotMinusOne(boundsData[offset], x);
                // top y
                boundsData[offset + 1] =
                        minIfNotMinusOne(boundsData[offset + 1], y);
                // right x
                boundsData[offset + 2] =
                        maxIfNotMinusOne(boundsData[offset + 2], x);
                // bottom y
                boundsData[offset + 3] =
                        maxIfNotMinusOne(boundsData[offset + 3], y);
            }
        }

        return bounds;
    }

    private static final int minIfNotMinusOne(int ref, int candidate) {
        return (ref == -1 || ref > candidate) ? candidate : ref;
    }

    private static final int maxIfNotMinusOne(int ref, int candidate) {
        return (ref == -1 || ref < candidate) ? candidate : ref;
    }
}
