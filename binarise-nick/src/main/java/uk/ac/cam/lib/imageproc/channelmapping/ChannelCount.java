package uk.ac.cam.lib.imageproc.channelmapping;

/**
 * Created by hal on 21/02/17.
 */
public interface ChannelCount {
    int inputChannelCount();

    int outputChannelCount();
}
