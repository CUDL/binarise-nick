package uk.ac.cam.lib.imageproc.channelmapping;


import uk.ac.cam.lib.imageproc.arrays.TypeConversion;

public class FloatToByteChannelMapper
        extends BaseOneToOneChannelMapper<float[], byte[]> {

    public FloatToByteChannelMapper(int maxCount, int inputOutputCount) {
        super(maxCount, inputOutputCount);
    }

    public static ChannelMapperFactory<float[], byte[]> factory(
            int channelCount) {

        return new DefaultChannelMapperFactory<>(
                channelCount, channelCount, maxCount ->
            new FloatToByteChannelMapper(maxCount, channelCount));
    }

    protected void mapChannel(float[] src, byte[] dest, int count, int i) {
        TypeConversion.toBytes(src, 0, dest, 0, count);
    }
}
