package uk.ac.cam.lib.imageproc.filter;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.arrays.Arrays;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

import java.util.Objects;
import java.util.concurrent.ForkJoinPool;

/**
 * A parallel implementation of {@link MedianFilter} which executes in a
 * {@link ForkJoinPool}.
 */
public class ParallelMedianFilter extends AbstractMedianFilter<float[]> {

    public static final int DEFAULT_MAX_SYNCHRONOUS =
            Array2DFilterRecursiveAction.REASONABLE_MAX_SYNCHRONOUS;

    private final ForkJoinPool executionPool;
    private final int maxSynchronous;

    public ParallelMedianFilter(int windowWidth, int windowHeight,
                                SamplingMethod<float[]> samplingMethod) {

        this(windowWidth, windowHeight, samplingMethod,
             ForkJoinPool.commonPool(), DEFAULT_MAX_SYNCHRONOUS);
    }

    public ParallelMedianFilter(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, ForkJoinPool executionPool,
            int maxSynchronous) {

        super(windowWidth, windowHeight, samplingMethod,
                ArrayTypes.arrayType(float[].class));

        if(maxSynchronous < 1)
            throw new IllegalArgumentException(
                    "maxSynchronous must be positive, was: " +
                            maxSynchronous);

        this.executionPool = Objects.requireNonNull(executionPool);
        this.maxSynchronous = maxSynchronous;
    }

    @Override
    public Array2D<float[]> apply(Array2D<float[]> src, Array2D<float[]> dest) {

        executionPool.invoke(new MedianFilterRecursiveAction(
                0, src.size(), maxSynchronous, src, dest));

        return dest;
    }

    private class MedianFilterRecursiveAction
            extends Array2DFilterRecursiveAction<float[], float[]> {

        public MedianFilterRecursiveAction(
                int start, int end, int maxSynchronous,
                Array2D<float[]> src, Array2D<float[]> dest) {

            super(start, end, maxSynchronous, src, dest);
        }

        protected void calculate() {
            int imgWidth = this.src.width();

            int xStart = start % imgWidth;

            Array2D<float[]> s = src;
            Array2D<float[]> d = dest;

            int sampleWidth = windowWidth;
            int sampleHeight = windowHeight;

            final Array2D<float[]> sample = Array2D.allocate(
                    destType(), sampleWidth, sampleHeight);

            SamplingMethod<float[]> sampler = samplingMethod;

            float[] samplePixels = sample.data();
            float[] destPixels = d.data();

            int i = start;
            for(int y = start / imgWidth; i < end; ++y) {
                for(int x = xStart; x < imgWidth && i < end; ++x, ++i) {
                    sampler.sample(s, x, y, sampleWidth, sampleHeight, sample);

                    destPixels[i] = Arrays.medianInPlace(samplePixels);
                }

                xStart = 0;
            }
        }

        protected MedianFilterRecursiveAction withRange(int start, int end) {
            return new MedianFilterRecursiveAction(
                    start, end, maxSynchronous, src, dest);
        }
    }
}
