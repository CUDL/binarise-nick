package uk.ac.cam.lib.imageproc.objectlabel;


import com.carrotsearch.hppc.IntHashSet;
import com.carrotsearch.hppc.IntIntHashMap;
import com.carrotsearch.hppc.IntIntMap;
import com.carrotsearch.hppc.IntSet;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.filter.Array2DFilter;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;

/**
 * Label the connected regions of an image. A region is composed of adjacent
 * pixels with equal numeric values.
 *
 * @param <T> The type of the input image array.
 */
public interface RegionLabelFilter<T> extends Array2DFilter<T, int[]> {
    @Override
    default ArrayType<int[]> destType() {
        return ArrayTypes.arrayType(int[].class);
    }

    interface CompressedLabelRange {
        Array2D<int[]> getCompressedLabelImage();
        IntIntMap getLabelMap();
        int getMaxLabel();
    }

    /**
     * Re-map labels in labelImage to remove gaps between labels, while
     * maintaining the overall order between labels.
     *
     * <p>i.e. so that:
     * {@code set(labelImage) == set(range(min(labelImage), max(labelImage) + 1)}.
     * And comparing any pair of labels before and after will yield the same
     * result.
     */
    static CompressedLabelRange compressLabelRange(Array2D<int[]> labelImage) {
        IntSet labelSet = new IntHashSet();
        int[] labels = labelImage.data();

        for(int i = 0; i < labels.length; ++i)
            labelSet.add(labels[i]);
        labelSet.add(0);

        int[] uniqueLabels = labelSet.toArray();
        java.util.Arrays.sort(uniqueLabels);
        IntIntMap labelMap = new IntIntHashMap(uniqueLabels.length);

        for(int i = 0; i < uniqueLabels.length; ++i)
            labelMap.addTo(uniqueLabels[i], i);

        for(int i = 0; i < labels.length; ++i)
            labels[i] = labelMap.get(labels[i]);

        return new CompressedLabelRange() {
            @Override
            public Array2D<int[]> getCompressedLabelImage() {
                return labelImage;
            }

            @Override
            public IntIntMap getLabelMap() {
                return labelMap;
            }

            @Override
            public int getMaxLabel() {
                return uniqueLabels.length - 1;
            }
        };
    }
}
