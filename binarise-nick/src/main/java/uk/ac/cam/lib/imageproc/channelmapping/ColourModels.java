package uk.ac.cam.lib.imageproc.channelmapping;


import uk.ac.cam.lib.imageproc.Maths;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.filter.MultiArray2DFilter;

import java.util.List;
import java.util.Objects;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class ColourModels {

    static ChannelMapperFactory<float[], float[]>
        colourModelChannelMapperFactory(
            ThreeParamColourMapper mapper) {

        return new DefaultChannelMapperFactory<>(3, 3,
                maxCount -> new ColourModelChannelMapper(mapper, maxCount));
    }

    protected static final class ColourModelChannelMapper
        extends BaseChannelMapper<float[], float[]> {

        private final ThreeParamColourMapper mapper;

        public ColourModelChannelMapper(
                ThreeParamColourMapper mapper, int maxCount) {

            super(maxCount, 3, 3);

            this.mapper = Objects.requireNonNull(mapper);
        }

        @Override
        public void mapChannelsInternal(
                List<float[]> src, List<float[]> dest, int count) {

            if(src.size() != 3)
                throw new IllegalArgumentException(
                        "expected three src channels, got: " +
                                src.size());

            if(dest.size() != 3)
                throw new IllegalArgumentException(
                        "expected three dest channels, got: " +
                                src.size());

            mapper.mapColour(src.get(0), src.get(1), src.get(2),
                             dest.get(0), dest.get(1), dest.get(2), count);
        }
    }

    /**
     * Wrap an image filter to convert from RGB to HSV before invoking the
     * filter, and convert the result back to RGB afterwards.
     *
     * @param filter The filter to wrap. Must have 3 inputs and outputs.
     * @return The wrapped filter.
     */
    public static MultiArray2DFilter<float[], float[]> inHsv(
            MultiArray2DFilter<float[], float[]> filter) {

        ArrayType<float[]> type = ArrayTypes.arrayType(float[].class);

        return ChannelMapping.asFilter(
                    colourModelChannelMapperFactory(RGB_TO_HSV), type)
                .andThen(filter)
                .andThen(ChannelMapping.asFilter(
                        colourModelChannelMapperFactory(HSV_TO_RGB), type));
    }

    public interface ThreeParamColourMapper {
        void mapColour(
                float[] s0, float[] s1, float[] s2,
                float[] d0, float[] d1, float[] d2, int count);
    }

    public static final ThreeParamColourMapper HSV_TO_RGB =
            new HsvToRgbColourMapper();

    public static final ThreeParamColourMapper RGB_TO_HSV =
            new RgbToHsvColourMapper();

    public static class RgbToHsvColourMapper implements ThreeParamColourMapper {

        @Override
        public void mapColour(
                float[] sr, float[] sg, float[] sb,
                float[] dh, float[] ds, float[] dv, int count) {

            // See: https://en.wikipedia.org/wiki/HSL_and_HSV#General_approach
            float r, g, b;
            float smin, smax, chroma, hue;
            for(int i = count; i-- != 0;) {
                r = sr[i]; g = sg[i]; b = sb[i];
                smin = min(r, min(g, b));
                smax = max(r, max(g, b));

                chroma = smax - smin;

                if(chroma == 0f) {
                    hue = 0f;
                }
                else if(smax == r) {
                    hue = Maths.floorMod((g - b) / chroma, 6f);
                }
                else if(smax == g) {
                    hue = ((b - r) / chroma) + 2f;
                }
                else {
                    hue = ((r - g) / chroma) + 4f;
                }

                dh[i] = hue / 6;
                dv[i] = smax;
                ds[i] = smax == 0f ? 0 : chroma / smax;
            }
        }
    }

    public static class HsvToRgbColourMapper implements ThreeParamColourMapper {
        @Override
        public void mapColour(
                float[] sh, float[] ss, float[] sv,
                float[] dr, float[] dg, float[] db, int count) {

            float h, s, v, chroma, x, hueSegment, offset, r, g, b;
            for(int i = count; i-- != 0;) {
                h = sh[i];
                s = ss[i];
                v = sv[i];

                hueSegment = Maths.floorMod(h * 6, 6);
                chroma = s * v;
                x = chroma * (1 - Math.abs(Maths.floorMod(hueSegment, 2) - 1));

                switch((int)hueSegment) {
                    case 0:  r = chroma; g = x;      b = 0;      break;
                    case 1:  r = x     ; g = chroma; b = 0;      break;
                    case 2:  r = 0     ; g = chroma; b = x;      break;
                    case 3:  r = 0     ; g = x     ; b = chroma; break;
                    case 4:  r = x     ; g = 0     ; b = chroma; break;
                    default: r = chroma; g = 0     ; b = x;      break;
                }

                offset = v - chroma;
                dr[i] = r + offset;
                dg[i] = g + offset;
                db[i] = b + offset;
            }
        }
    }
}
