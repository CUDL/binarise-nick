package uk.ac.cam.lib.imageproc.channelmapping;

public interface ChannelMapperFactory<S, D> extends ChannelCount {
    /**
     * Get a {@link ChannelMapper} instance for the specified max samples
     * per call. This is to allow efficient pre-allocation of any required
     * intermediate buffers.
     *
     * @param maxCount
     * @return
     */
    ChannelMapper<S, D> getMapper(int maxCount);

    default <D2> ChannelMapperFactory<S, D2> bridgeTo(
            ChannelMapperFactory<D, D2> next, Class<D> intermediateClass) {

        return BridgingChannelMapper.bridgeChannelMappers(this, next, intermediateClass);
    }
}
