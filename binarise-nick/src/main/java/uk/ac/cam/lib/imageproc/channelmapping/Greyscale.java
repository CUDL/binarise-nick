package uk.ac.cam.lib.imageproc.channelmapping;


import java.util.List;

import static java.lang.Math.min;

public class Greyscale {

    public static abstract class GreyscaleChannelMapper
            extends BaseChannelMapper<float[], float[]> {

        public GreyscaleChannelMapper(int maxCount) {
            super(maxCount, 3, 1);
        }

        @Override
        public void mapChannelsInternal(
                List<float[]> src, List<float[]> dest, int count) {

            if(src.size() != 3)
                throw new IllegalArgumentException(
                        "expected three src channels (R, G, B), got: " +
                        src.size());

            if(dest.size() != 1)
                throw new IllegalArgumentException(
                        "expected one dest channel (grey), got: " +
                                src.size());

            mapChannelsInternal(src.get(0), src.get(1), src.get(2),
                                dest.get(0), count);
        }

        protected abstract void mapChannelsInternal(
                float[] red, float[] green, float[] blue,
                float[] grey, int count);
    }

    /**
     * A luminance-preserving conversion from RGB to greyscale.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Grayscale#Colorimetric_.28luminance-preserving.29_conversion_to_grayscale">Colorimetric (luminance-preserving) conversion to grayscale</a>
     */
    public static class ColorimetricGreyscaleChannelMapper
            extends GreyscaleChannelMapper {

        public static final float WEIGHT_R = 0.2126f,
                                  WEIGHT_G = 0.7152f,
                                  WEIGHT_B = 0.0722f;

        public ColorimetricGreyscaleChannelMapper(int maxCount) {
            super(maxCount);
        }

        public static ChannelMapperFactory<float[], float[]> factory() {

            return new DefaultChannelMapperFactory<>(
                    3, 1, ColorimetricGreyscaleChannelMapper::new);
        }

        @Override
        protected void mapChannelsInternal(
                float[] red, float[] green, float[] blue,
                float[] grey, int count) {

            for(int i = 0,
                    max = min(count, red.length); i < max; ++i) {

                grey[i] = red[i] * WEIGHT_R + green[i] * WEIGHT_G + blue[i] * WEIGHT_B;
            }
        }
    }

    public static class BlueBoostColorimetricGreyscaleChannelMapper
            extends ColorimetricGreyscaleChannelMapper {

        public BlueBoostColorimetricGreyscaleChannelMapper(int maxCount) {
            super(maxCount);
        }

        public static ChannelMapperFactory<float[], float[]> factory() {

            return new DefaultChannelMapperFactory<>(
                    3, 1, BlueBoostColorimetricGreyscaleChannelMapper::new);
        }

        @Override
        protected void mapChannelsInternal(
                float[] red, float[] green, float[] blue,
                float[] grey, int count) {

            for(int i = 0,
                max = min(count, red.length); i < max; ++i) {

                float r = red[i], g = green[i], b = blue[i];
                if(b > g && b > r) {
                    grey[i] = (r + g + b) / 3;
                }
                else {
                    grey[i] = r * WEIGHT_R + g * WEIGHT_G + b * WEIGHT_B;
                }
            }
        }
    }

    public static class AverageGreyscaleChannelMapper
            extends ColorimetricGreyscaleChannelMapper {

        public AverageGreyscaleChannelMapper(int maxCount) {
            super(maxCount);
        }

        public static ChannelMapperFactory<float[], float[]> factory() {

            return new DefaultChannelMapperFactory<>(
                    3, 1, AverageGreyscaleChannelMapper::new);
        }

        @Override
        protected void mapChannelsInternal(
                float[] red, float[] green, float[] blue,
                float[] grey, int count) {

            for(int i = 0,
                max = min(count, red.length); i < max; ++i) {

                grey[i] = (red[i] + green[i] + blue[i]) / 3;
            }
        }
    }

    public static class MinChannelGreyscaleChannelMapper
            extends ColorimetricGreyscaleChannelMapper {

        public MinChannelGreyscaleChannelMapper(int maxCount) {
            super(maxCount);
        }

        public static ChannelMapperFactory<float[], float[]> factory() {

            return new DefaultChannelMapperFactory<>(
                    3, 1, MinChannelGreyscaleChannelMapper::new);
        }

        @Override
        protected void mapChannelsInternal(
                float[] red, float[] green, float[] blue,
                float[] grey, int count) {

            for(int i = 0,
                max = min(count, red.length); i < max; ++i) {

                grey[i] = min(red[i], min(green[i], blue[i]));
            }
        }
    }

    private Greyscale() {}
}
