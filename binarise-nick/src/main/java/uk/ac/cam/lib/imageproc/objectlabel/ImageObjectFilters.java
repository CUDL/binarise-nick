package uk.ac.cam.lib.imageproc.objectlabel;

import uk.ac.cam.lib.imageproc.LengthUnit;
import uk.ac.cam.lib.imageproc.objectlabel.ImageObjects.ImageObject;

import java.util.OptionalDouble;
import java.util.function.Predicate;

public class ImageObjectFilters {

    /**
     * Get a predicate which matches {@link ImageObject ImageObjects} with a
     * (bounding box) {@link ImageObject#getArea() area} {@code >= minArea} and
     * {@code <= maxArea}.
     *
     * <p>The resolution of the image the objects are part of is defined by a
     * number of pixels per unit, e.g. 300 DPI (300 pixels per inch) would be
     * simply:
     *
     * <pre>{@code
     *     objectWithPhysicalSizeMatcher(300, LengthUnit.INCHES, ...)
     * }</pre>
     *
     * @param pixelsPer The resolution of the input image in number of pixels
     *                  per {@code resolutionUnit}
     *                  The resolution of the image, specified as the number of
     *                  image pixels needed to equal one {@code resolutionUnit}
     * @param resolutionUnit The unit (cm, inch, etc), one of which is equal to
     *                       the number of pixels specified by {@code pixelsPer}
     * @param minArea The minimum (inclusive) area an object must be to be
     *                retained.
     * @param maxArea The maximum (inclusive) area an object must be to be
     *                retained.
     * @param areaUnit The unit the min and max area are specified in.
     * @return A predicate with the described characteristics
     */
    public static Predicate<ImageObject> objectWithPhysicalSizeMatcher(
            float pixelsPer, LengthUnit resolutionUnit,
            OptionalDouble minArea, OptionalDouble maxArea,
            LengthUnit areaUnit) {

        int minPixelArea = minArea.isPresent() ? pixelLength(
                pixelsPer, resolutionUnit,
                (float)minArea.getAsDouble(), areaUnit) : 0;
        int maxPixelArea = maxArea.isPresent() ? pixelLength(
                pixelsPer, resolutionUnit,
                (float)maxArea.getAsDouble(), areaUnit) : Integer.MAX_VALUE;

        return imgObj -> {
            int area = imgObj.getArea();
            return area >= minPixelArea && area <= maxPixelArea;
        };
    }

    static int pixelLength(float resolution, LengthUnit resolutionUnit,
                           float length, LengthUnit lengthUnit) {
        return Math.round(lengthUnit.to(resolutionUnit, length) * resolution);
    }

    private ImageObjectFilters() { throw new RuntimeException(); }
}
