package uk.ac.cam.lib.imageproc.objectlabel;

import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Objects;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DefaultImageObjects implements ImageObjects {

    private final Array2D<int[]> labelImage;
    private final Array2D<int[]> objectBounds;

    public DefaultImageObjects(Array2D<int[]> labelImage,
                               Array2D<int[]> objectBounds) {

        Objects.requireNonNull(labelImage);
        Objects.requireNonNull(objectBounds);

        if(objectBounds.width() != 4)
            throw new IllegalArgumentException(
                    "objectBounds was not width 4. width: " +
                    objectBounds.width());

        this.labelImage = labelImage;
        this.objectBounds = objectBounds;
    }

    @Override
    public Array2D<int[]> getLabelImage() {
        return labelImage;
    }

    public Array2D<int[]> getObjectBounds() {
        return objectBounds;
    }

    @Override
    public int getMaxLabel() {
        return objectBounds.height() - 1;
    }

    @Override
    public ImageObject getObject(int label) {
        validateLabel(label);

        int[] rects = objectBounds.data();
        if(rects[label * 4] == -1)
            throw new IllegalStateException(
                    "Image has no pixels labeled with: " + label);

        return new DefaultImageObject(label);
    }

    @Override
    public Stream<ImageObject> streamObjects(boolean includeBackground) {
        return IntStream.range(includeBackground ? 0 : 1, this.getMaxLabel() + 1)
                .mapToObj(this::getObject);
    }

    private void validateLabel(int label) {
        if(label < 0 || label > getMaxLabel())
            throw new IllegalArgumentException("label out of range: " +
                    label);
    }

    final class DefaultImageObject implements ImageObject {

        private final int label;

        public DefaultImageObject(int label) {
            // label is validated by parent class
            assert label >= 0 && label <= getMaxLabel();
            this.label = label;
        }

        @Override
        public int getLabel() {
            return label;
        }

        @Override
        public int getArea() {
            int area = 0;
            int label = this.label;
            int[] labels = labelImage.data();
            int imageWidth = labelImage.width();

            for(int y = getYTop(), yEnd = getYBottom(),
                    xStart = getXLeft(), width = (getXRight() - xStart) + 1;
                y <= yEnd; ++y) {

                int i = imageWidth * y + xStart;
                int iEnd = i + width;

                while(i < iEnd) {
                    area += Boolean.compare(labels[i++] == label, false);
                }
            }

            return area;
        }

        @Override
        public int getXLeft() {
            return objectBounds.data()[label * 4];
        }

        @Override
        public int getXRight() {
            return objectBounds.data()[label * 4 + 2];
        }

        @Override
        public int getYTop() {
            return objectBounds.data()[label * 4 + 1];
        }

        @Override
        public int getYBottom() {
            return objectBounds.data()[label * 4 + 3];
        }

        @Override
        public int getWidth() {
            int[] rects = objectBounds.data();
            int xl = rects[label * 4], xr = rects[label * 4 + 2];
            return (xr - xl) + 1;
        }

        @Override
        public int getHeight() {
            int[] rects = objectBounds.data();
            int yt = rects[label * 4 + 1], yb = rects[label * 4 + 3];
            return (yb - yt) + 1;
        }

        @Override
        public void renderTo(Array2D<byte[]> dest, byte value, int x, int y) {
            if(x < 0 || x + getWidth() > dest.width())
                throw new IllegalArgumentException(
                        "object doesn't fit into dest at x position: " + x);

            if(y < 0 || y + getHeight() > dest.height())
                throw new IllegalArgumentException(
                        "object doesn't fit into dest at y position: " + y);

            int[] srcData = getLabelImage().data();
            int srcWidth = getLabelImage().width();
            byte[] destData = dest.data();
            int destWidth = dest.width();
            int sxStart = getXLeft();
            int label = this.getLabel();
            int objWidth = getWidth();

            for(int sy = getYTop(), dy = y, syEnd = getYBottom() + 1;
                sy < syEnd; ++sy, ++dy) {

                for(int si = sy * srcWidth + sxStart, siEnd = si + objWidth,
                        di = dy * destWidth + x;
                    si < siEnd; ++si, ++di) {

                    if(srcData[si] == label) {
                        destData[di] = value;
                    }
                }
            }
        }
    }
}
