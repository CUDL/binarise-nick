package uk.ac.cam.lib.imageproc.channelmapping;

import java.util.Objects;
import java.util.function.IntFunction;

public final class DefaultChannelMapperFactory<S, D>
        extends BaseChannelMapperFactory<S, D> {

    private final IntFunction<ChannelMapper<S, D>> mapperCreator;

    public DefaultChannelMapperFactory(
            int inputChannelCount, int outputChannelCount,
            IntFunction<ChannelMapper<S, D>> mapperCreator) {
        super(inputChannelCount, outputChannelCount);

        this.mapperCreator = Objects.requireNonNull(mapperCreator);
    }

    @Override
    public ChannelMapper<S, D> getMapper(int maxCount) {
        return this.mapperCreator.apply(maxCount);
    }
}
