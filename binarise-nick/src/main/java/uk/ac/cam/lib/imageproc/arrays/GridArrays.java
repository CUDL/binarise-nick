package uk.ac.cam.lib.imageproc.arrays;

public final class GridArrays {

    public static void copy(float[] src, int sw, int sxa, int sya,
                            float[] dst, int dw, int dxa, int dya, int dxb, int dyb) {

        checkGridArraySize(src, sw);
        checkGridArraySize(dst, dw);

        if(dxb > dxa) {
            copyWithArrayCopy(src, sw, sxa, sya,
                              dst, dw, dxa, dya, dxb, dyb);
        }
        else {
            copyElementwise(src, sw, sxa, sya,
                            dst, dw, dxa, dya, dxb, dyb);
        }
    }

    private static void checkGridArraySize(float[] grid, int width) {
        if(grid.length % width != 0)
            reportGridArraySizeError(grid, width);
    }

    private static void reportGridArraySizeError(float[] grid, int width) {
        throw new IllegalArgumentException(String.format(
                "grid length is not a multiple of width. length: %d, width: %d",
                grid.length, width));
    }

    private static void checkBounds(float[] grid, int width, int x, int y) {
        if(x < 0 || y < 0 || x >= width || y >= (grid.length / width))
            reportBoundsError(grid, width, x, y);
    }

    private static void reportBoundsError(
            float[] grid, int width, int x, int y) {
        if(x < 0)
            throw new IllegalArgumentException("x was negative: " + x);
        else if(y < 0)
            throw new IllegalArgumentException("y was negative: " + y);
        else if(x >= width) {
            throw new IllegalArgumentException(String.format(
                    "x was out of range. x: %d, width: %d", x, width));
        }

        assert y >= grid.length / width;
        throw new IllegalArgumentException(
                String.format("y was out of range. y: %d, height: %d", y,
                        grid.length / width));
    }

    static void copyElementwise(
            float[] src, int sw, int sxa, int sya,
            float[] dst, int dw, int dxa, int dya, int dxb, int dyb) {

        int destStrideX = dxa <= dxb ? 1 : -1;
        int destStrideY = dya <= dyb ? 1 : -1;

        int width = Math.abs((dxb - dxa) + destStrideX);
        int height = Math.abs((dyb - dya) + destStrideY);

        checkBounds(src, sw, sxa, sya);
        checkBounds(src, sw, sxa + width - 1, sya + height - 1);
        checkBounds(dst, dw, dxa, dya);
        checkBounds(dst, dw, dxb, dyb);

        for(int isy = sya, idy = dya, lastY = sya + height;
            isy < lastY; isy += 1, idy += destStrideY) {


            for(int is = (isy * sw) + sxa,
                    id = (idy * dw) + dxa,
                    lastIs = is + width; is < lastIs;
                    is += 1, id += destStrideX) {
                dst[id] = src[is];
            }
        }
    }

    static void copyWithArrayCopy(
            float[] src, int sw, int sxa, int sya,
            float[] dst, int dw, int dxa, int dya, int dxb, int dyb) {

        if(dxb - dxa < 0)
            throw new IllegalArgumentException(
                    "Reversed destination X axis is not supported with " +
                            "this implementation");

        int destStrideY = dya <= dyb ? 1 : -1;
        int width = (dxb - dxa) + 1;
        int syb = sya + Math.abs(dyb - dya);

        checkBounds(src, sw, sxa, sya);
        checkBounds(src, sw, sxa + (dxb - dxa), syb);
        checkBounds(dst, dw, dxa, dya);
        checkBounds(dst, dw, dxb, dyb);

        for(int isy = sya, lastY = syb + 1, idy = dya;
            isy < lastY; ++isy, idy += destStrideY) {
            System.arraycopy(src, (isy * sw) + sxa, dst, (idy * dw) + dxa, width);
        }
    }

    private GridArrays() {}
}
