package uk.ac.cam.lib.imageproc.objectlabel;


import uk.ac.cam.lib.imageproc.ImageValues;
import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.stream.Stream;

public interface ImageObjects {

    Array2D<int[]> getLabelImage();

    int getMaxLabel();

    ImageObject getObject(int label);

    interface ImageObject {
        int getLabel();

        int getArea();

        int getXLeft();
        int getXRight();
        int getYTop();
        int getYBottom();

        int getWidth();

        int getHeight();

        default void renderTo(Array2D<byte[]> dest) {
            renderTo(dest, ImageValues.BLACK_1BIT);
        }
        default void renderTo(Array2D<byte[]> dest, byte value) {
            renderTo(dest, value, getXLeft(), getYTop());
        }
        void renderTo(Array2D<byte[]> dest, byte value, int x, int y);
    }

    default Stream<ImageObject> streamObjects() {
        return streamObjects(false);
    }

    Stream<ImageObject> streamObjects(boolean includeBackground);
}
