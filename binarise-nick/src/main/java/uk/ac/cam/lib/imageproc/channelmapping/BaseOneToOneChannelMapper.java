package uk.ac.cam.lib.imageproc.channelmapping;


import java.util.List;

public abstract class BaseOneToOneChannelMapper<S, D> extends BaseChannelMapper<S, D> {
    public BaseOneToOneChannelMapper(int maxCount, int inputOutputCount) {
        super(maxCount, inputOutputCount);
    }

    @Override
    protected void mapChannelsInternal(List<S> src, List<D> dest, int count) {
        assert src.size() == dest.size();

        for(int i = src.size(); i-- != 0;)
            mapChannel(src.get(i), dest.get(i), count, i);
    }

    protected abstract void mapChannel(S src, D dest, int count, int channel);
}
