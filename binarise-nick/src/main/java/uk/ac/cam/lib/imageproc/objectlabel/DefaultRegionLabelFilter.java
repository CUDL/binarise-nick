package uk.ac.cam.lib.imageproc.objectlabel;

import uk.ac.cam.lib.imageproc.ImageValues;
import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Objects;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class DefaultRegionLabelFilter implements RegionLabelFilter<byte[]> {

    private static final boolean DEBUG = "true".equals(System.getProperty(
            "uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter#DEBUG"));

    private final int connectivity;
    private final byte background;

    public enum Connectivity {
        CROSS {
            public int maxHops() { return 1; }
        },
        FULL {
            public int maxHops() { return 2; }
        };

        public abstract int maxHops();
    }

    public DefaultRegionLabelFilter(Connectivity connectivity) {
        this(connectivity, ImageValues.WHITE_1BIT);
    }

    public DefaultRegionLabelFilter(Connectivity connectivity,
                                    byte background) {
        this.connectivity = Objects.requireNonNull(connectivity).maxHops();
        this.background = background;
    }

    @Override
    public Array2D<int[]> apply(Array2D<byte[]> src, Array2D<int[]> dest) {

        // Need to zero the user-supplied dest array
        java.util.Arrays.fill(dest.data(), 0);

        return _apply(src, dest);
    }

    @Override
    public Array2D<int[]> apply(Array2D<byte[]> src) {
        return _apply(src, Array2D.allocateSameShape(int[].class, src));
    }

    private Array2D<int[]> _apply(Array2D<byte[]> src, Array2D<int[]> dest) {
        if(dest.width() != src.width() || dest.height() != src.height())
            throw new IllegalArgumentException("dest is not the same size as src");

        /*
        The strategy used is to pass across the image row-by-row in alternate
        directions, first from the top-left, then from the bottom-right. While
        moving down, matches are found above and to the left. While moving up,
        we look below and right.
         */

        int height = src.height();
        int width = src.width();
        int background = this.background;

        byte[] img = src.data();
        int[] labels = dest.data();
        int lastLabel = 0; // The first non-background label is 1

        // Keep track of the adjacent pixels with the same value with differing
        // labels. This must reduce with each pass.
        int previousConflicts = labels.length;

        for(int direction = 1;; direction = -direction) {
            int conflicts = 0;

            for(int y = (height - 1) * max(0, -direction),
                    yEnd = (height - 1) * max(0, direction) + direction,
                    xEnd = (width - 1) * max(0, direction) + direction,
                    xStart = (width - 1) * max(0, -direction),
                    i = (labels.length - 1) * max(0, -direction);
                y != yEnd; y += direction) {

                int yyStart = min(height - 1, max(0, y + -direction)),
                    yyEnd = y + direction;

                for(int x = xStart; x != xEnd; x += direction, i += direction) {

                    if(img[i] == background)
                        continue;

                    // Keep track of the highest and lowest label found for
                    // matching pixels in our connectivity region.
                    int minLabel = Integer.MAX_VALUE,
                        maxLabel = Integer.MIN_VALUE;

                    for(int yy = yyStart,
                            xxStart = min(width - 1, max(0, x + -direction)),
                            xxEnd = min(width, max(0, x + direction * 2));
                        yy != yyEnd; yy += direction) {
                        for(int xx = xxStart; xx != xxEnd; xx += direction) {
                            int distance = (abs(x - xx) + abs(y - yy)) * direction;
                            if(distance > connectivity || distance == 0)
                                continue;

                            int ii = yy * width + xx;
                            int label = labels[ii];
                            // label will be zero when encountering a
                            // non-background pixel for the first time
                            if(img[ii] == img[i] && label != 0) {
                                minLabel = min(minLabel, label);
                                maxLabel = max(maxLabel, label);
                            }
                        }
                    }

                    int currentLabel = labels[i];
                    if(maxLabel == Integer.MIN_VALUE) {
                        // maxLabel is unchanged, so there must have been no
                        // matching pixels in region. We need to assign a new
                        // label
                        labels[i] = currentLabel == 0 ?
                                ++lastLabel : currentLabel;
                    }
                    else {
                        // We found at least one label, use the smallest label
                        // so that we converge on a consistent labeling order
                        // from smallest to highest (row-wise), top left to
                        // bottom-right.
                        assert minLabel > 0;
                        labels[i] = currentLabel == 0 ?
                                minLabel : min(currentLabel, minLabel);

                        if(minLabel != maxLabel) {
                            // Different labels were encountered for the same
                            // values in the region. Mark this pixel as
                            // conflicted. Another pass will be required to
                            // resolve the conflict with the neighboring pixel
                            // and its neighbors.
                            conflicts += 1;
                        }
                    }
                }
            }

            // When there are no more conflicts the image is fully labeled
            if(conflicts == 0) {
                return dest;
            }

            // Otherwise we must make another pass over the image to merge
            // conflicting regions.
            // FIXME: are conflicting boundary pixels a good measure? Perhaps
            // counting number of pixels with conflicting would be required.
//            assert conflicts < previousConflicts :
//                    "Failed to reduce conflicting pixel boundary";
            previousConflicts = conflicts;
        }

        // FIXME: need final pass to reduce label space
    }
}
