package uk.ac.cam.lib.imageproc.channelmapping;

import java.util.Arrays;

import static java.lang.Math.min;

public final class IntegerToFloatChannelMapper
        extends BaseOneToOneChannelMapper<int[], float[]> {

    private final int[] channelMax;

    public IntegerToFloatChannelMapper(int maxCount, int[] channelMax) {
        super(maxCount, channelMax.length);

        this.channelMax = Arrays.copyOf(channelMax, channelMax.length);
    }

    public static ChannelMapperFactory<int[], float[]> factory(
            int... channelMax) {

        return new DefaultChannelMapperFactory<>(
                channelMax.length, channelMax.length,
                maxCount ->
                    new IntegerToFloatChannelMapper(maxCount, channelMax));
    }

    @Override
    protected void mapChannel(int[] src, float[] dest, int count, int channel) {
        float max = channelMax[channel];

        for(int i = count; i-- != 0;)
            dest[i] = ((float)src[i]) / max;
    }
}
