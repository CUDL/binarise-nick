package uk.ac.cam.lib.imageproc.filter.binarise;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.filter.Array2DFilter;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;

/**
 * Encapsulates a method for binarising (partitioning) a greyscale image into
 * background and foreground data.
 */
public interface BinarisationMethod<S> extends Array2DFilter<S, byte[]> {

    @Override
    default ArrayType<byte[]> destType() {
        return ArrayTypes.arrayType(byte[].class);
    }

    /**
     * Partition a greyscale image into foreground (black, 0-valued) data and
     * background (white, 1-valued) data.
     *
     * @param image The greyscale image to binarise.
     * @param result A byte array to save the binarised result to. Must be the
     *               same length as the greySamples array.
     * @return The binarised result.
     */
    Array2D<byte[]> apply(Array2D<S> image, Array2D<byte[]> result);
}
