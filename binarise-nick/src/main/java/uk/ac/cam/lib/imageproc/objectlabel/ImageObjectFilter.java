package uk.ac.cam.lib.imageproc.objectlabel;


import uk.ac.cam.lib.imageproc.ImageValues;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.filter.Array2DFilter;
import uk.ac.cam.lib.imageproc.objectlabel.RegionLabelFilter.CompressedLabelRange;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Exclude regions of a binary image by detecting objects in the image and
 * excluding objects which don't satisfy a predicate.
 */
public abstract class ImageObjectFilter implements
        Array2DFilter<byte[], byte[]> {

    private final byte colour;
    private final RegionLabelFilter<byte[]> regionLabelFilter;

    public ImageObjectFilter(RegionLabelFilter<byte[]> regionLabelFilter,
                             byte colour) {

        this.regionLabelFilter = Objects.requireNonNull(regionLabelFilter);
        this.colour = colour;
    }

    public abstract Predicate<ImageObjects.ImageObject> isIncluded();

    protected ImageObjects getObjects(Array2D<byte[]> binaryImage) {
        Array2D<int[]> labeled = regionLabelFilter.apply(binaryImage);
        CompressedLabelRange compressedLabels =
                RegionLabelFilter.compressLabelRange(labeled);

        Array2D<int[]> bounds = ObjectBoxer.getObjectBounds(
                labeled, compressedLabels.getMaxLabel());

        return new DefaultImageObjects(labeled, bounds);
    }

    @Override
    public ArrayType<byte[]> destType() {
        return ArrayTypes.arrayType(byte[].class);
    }

    @Override
    public Array2D<byte[]> apply(Array2D<byte[]> src) {
        Array2D<byte[]> dest = Array2D.allocateSameShape(src);
        return apply(src, dest);
    }

    @Override
    public Array2D<byte[]> apply(Array2D<byte[]> src, Array2D<byte[]> dest) {
        Arrays.fill(dest.data(), ImageValues.WHITE_1BIT);
        byte colour = this.colour;

        getObjects(src).streamObjects()
                .parallel()
                .filter(this.isIncluded())
                .forEach(obj -> obj.renderTo(dest, colour));

        return dest;
    }
}
