package uk.ac.cam.lib.imageproc;

/**
 * Represents a function which accepts a float argument and produces a float
 * result.
 */
@FunctionalInterface
public interface FloatToFloatFunction {
    float applyAsFloat(float f);
}
