package uk.ac.cam.lib.imageproc.arrays;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public final class Arrays {

    public static final float sum(float[] array) {
        float sum = 0;
        for(int i = 0, l = array.length; i < l; ++i)
            sum += array[i];
        return sum;
    }

    public static final float mean(float[] array) {
        return sum(array) / array.length;
    }

    /**
     * Square the values in {@code array} in place.
     * @return the same array as the {@code array} argument.
     */
    public static float[] square(float[] array) {
        for(int i = 0, l = array.length; i < l; ++i) {
            float value = array[i];
            array[i] = value * value;
        }
        return array;
    }

    /**
     * The point at which using selection is faster than just sorting the whole
     * array.
     *
     * This value is based on benchmarking the two implementations at
     * different array sizes. Also note that java.util.Arrays.sort
     * itself has a threshold around this value (286) at which it switches
     * between two different algorithms.
     */
    public static final int MEDIAN_SELECTION_THRESHOLD = 290;

    /**
     * Find the median of the given array. The ordering of the array will be
     * modified in order to determine the median value.
     *
     * <p>This implementation automatically chooses between
     * {@link #medianInPlace_selection(float[])} and
     * {@link #medianInPlace_sort(float[])} depending which will yield the best
     * performance.
     *
     * @param array The values to find the median of.
     * @return The median value.
     */
    public static float medianInPlace(float[] array) {
        // For small arrays it's quicker to just sort the whole thing than to
        // use selection (partial sorting).
        if(array.length < MEDIAN_SELECTION_THRESHOLD)
            return medianInPlace_sort(array);
        else
            return medianInPlace_selection(array);
    }

    /**
     * Find the median of the specified values by sorting the array. This tends
     * to be faster than {@link #medianInPlace_selection(float[])} for arrays
     * sized around {@value MEDIAN_SELECTION_THRESHOLD} or less.
     *
     * @see #MEDIAN_SELECTION_THRESHOLD
     * @see #medianInPlace(float[])
     */
    public static float medianInPlace_sort(float[] array) {
        if(array.length == 0)
            throw new IllegalArgumentException("array was empty");

        java.util.Arrays.sort(array);

        int middle = array.length / 2;
        if(array.length % 2 == 1)
            return array[middle];
        else
            return (array[middle - 1] + array[middle]) / 2f;
    }

    /**
     * Find the median of the specified values using a selection algorithm
     * rather than sorting the entire array. This tends to be faster than
     * {@link #medianInPlace_sort(float[])} for arrays sized around
     * {@value MEDIAN_SELECTION_THRESHOLD} or greater.
     *
     * @see #MEDIAN_SELECTION_THRESHOLD
     * @see #medianInPlace(float[])
     */
    public static float medianInPlace_selection(float[] array) {
        if(array.length == 0)
            throw new IllegalArgumentException("array was empty");

        int middle = array.length / 2;
        if(array.length % 2 == 1) {
            select(array, middle, 0, array.length - 1);
            return array[middle];
        }
        else {
            selectRange(array, middle - 1, middle, 0, array.length - 1);
            return (array[middle - 1] + array[middle]) / 2;
        }
    }

    private static void validateArrayRangeIndex(int length, int left, int i,
                                                int right, String indexName) {

        if(left < 0)
            throw new IllegalArgumentException("left was negative");
        if(i < left)
            throw new IllegalArgumentException(indexName + " was < left");
        if(right < i)
            throw new IllegalArgumentException("right was < " + indexName);
        if(right >= length)
            throw new IllegalArgumentException("right was >= length");
    }

    /**
     * Partion the interval {@code [left, right]} of {@code items} into elements
     * less than the partition value and those greater or equal to the
     * partition value. The partition value is specified by the index
     * {@code partition}.
     *
     * <p>{@code items} is updated in place. Elements outside the
     * {@code [left, right]} range are not touched though.
     *
     * <p>The following must hold: {@code 0 <= left <= partition <= right < items.length}
     *
     * @param items The items to partition
     * @param partition The index of the value to partition around
     * @param left The first index of the range to partition
     * @param right The last index of the range to partition
     * @return The index of the pivot following partitioning.
     */
    static int partition(float[] items, int partition, int left, int right) {
        validateArrayRangeIndex(
                items.length, left, partition, right, "partition");

        final float partitionValue = items[partition];
        float tmp;

        // The partition value has to be in top half, so before we start we know
        // the upper half will contain at least the partition value. Therefore
        // place it at the end as the single member of the upper half.
        tmp = items[right];
        items[right] = partitionValue;
        items[partition] = tmp;
        partition = right;

        for(int i = partition - 1; i >= left; --i) {
            if(items[i] >= partitionValue) {
                partition = partition - 1;
                tmp = items[i];
                items[i] = items[partition];
                items[partition] = tmp;
            }
        }

        assert items[right] == partitionValue;

        // Move the partition value to the front of the upper partition to
        // maintain our invariant that elements after the partition are >= than
        // it.
        items[right] = items[partition];
        items[partition] = partitionValue;

        return partition;
    }

    /**
     * Find the {@code i}<sup>th</sup>-smallest element in the <em>unsorted</em>
     * items array.
     *
     * <p>{@code items} will be updated (by reordering) in place. When the
     * function returns, {@code items} will be rearanged such that the element
     * at index {@code i} is at the position it would be in if the array was
     * fully sorted. Elements before i are guaranteed to be less than or equal
     * to i and those following i are greater or equal. However elements other
     * than the ith are not necessarily in their sorted order.
     *
     * <p>This implementation is based on the Quickselect algorithm (related to
     * Quicksort). It exhibits O(n) complexity - superior to sorting an array.
     *
     * @param items The array of items to select from
     * @param i The i<sup>th</sup> (strictly
     *          {@code (i - left)}<sup>th</sup>-smallest) element to select
     * @param left The First index of the range to select from (inclusive)
     * @param right The last index of the range to select from (inclusive)
     */
    public static void select(float[] items, int i, int left, int right) {
        select(items, i, left, right, ThreadLocalRandom.current());
    }

    /**
     * As {@link #select(float[], int, int, int)} but allows specifying the
     * random generator to use when selecting partition points.
     */
    public static void select(float[] items, int i, int left, int right,
                              Random r) {
        validateArrayRangeIndex(items.length, left, i, right, "i");
        Objects.requireNonNull(r);

        int upperIndex;

        while(true) {
            upperIndex = partition(
                    items, left + r.nextInt((right - left) + 1), left, right);

            if(i < upperIndex) {
                right = upperIndex - 1;
            }
            else if(i == upperIndex) {
                return;
            }
            else {
                left = upperIndex + 1;
            }
        }
    }

    /**
     * Select a partially-sorted range from an unsorted list in linear time.
     *
     * <p>Upon returning, items will be sorted such that:
     *
     * <ul>
     *   <li>The values at indexes {@code a} and {@code b}
     * are in the positions they would be in if the (specified region of the)
     * array was sorted.</li>
     *   <li>
     *     Elements other than {@code a} and {@code b} are ordered with
     *     respect to {@code a} and {@code b}, but not to each other.
     *     Specifically the following holds for elements with indexes in the
     *     specified intervals:
     *     <ul>
     *       <li>{@code [left, a) <= a <= (a, b) <= (b, right]}</li>
     *     </ul>
     *   </li>
     *   <li>elements {@code [0, left)} and {@code (right, items.length)} are
     *   unmoved</li>
     * </ul>
     *
     * <p>The following must hold: {@code 0 <= left <= a <= b <= right < items.length}
     *
     * <p>This is based on Quickselect, but extended to select a range rather
     * than one point. Not sure if this was already a thing with a name, but it
     * is now anyway.
     *
     * @param items The array to partially sort
     * @param a The index of the lower value in the range to select
     * @param b The index of the upper value in the range to select
     * @param left The first index of items to search within (inclusive)
     * @param right The last index of items to search within (inclusive)
     */
    public static void selectRange(
            float[] items, int a, int b, int left, int right) {
        selectRange(items, a, b, left, right, ThreadLocalRandom.current());
    }

    /**
     * As {@link #selectRange(float[], int, int, int, int, Random)} but allows
     * specifying the random generator to use when selecting partition points.
     */
    public static void selectRange(
            float[] items, int a, int b, int left, int right, Random rand) {
        validateArrayRangeIndex(items.length, left, a, right, "a");
        if(b < a)
            throw new IllegalArgumentException("b was < a");
        if(right < b)
            throw new IllegalArgumentException("right was < b");

        // Keep track of the lowest previously partitioned point >= b and
        // the highest previously partitioned point <= b.
        // This saves effort in the second phase as we can restrict the
        // portion of the array to select from because the value that ends up
        // at b must be between bLeft and bRight.
        int bLeft = Integer.MIN_VALUE;
        int bRight = Integer.MAX_VALUE;

        int upperIndex, l = left, r = right;


        // First we order for a
        while(true) {
            upperIndex = partition(
                    items, l + rand.nextInt((r - l) + 1), l, r);

            if(upperIndex >= b)
                bRight = Math.min(upperIndex, bRight);
            if(upperIndex <= b)
                bLeft = Math.max(upperIndex, bLeft);

            if(a < upperIndex) {
                r = upperIndex - 1;
            }
            else if(a == upperIndex) {
                break;
            }
            else {
                l = upperIndex + 1;
            }
        }

        // a is now in position. If we already happened to find b then [a, b]
        // is already sorted as we require and nothing remains to be done.
        // Otherwise b must be in [bLeft, bRight] and we must continue to
        // partially order that region to find b's position.
        if(!(bLeft == b || bRight == b)) {
            assert bLeft >= a;

            l = Math.max(a + 1, bLeft);
            r = Math.min(right, bRight);

            assert l <= b;
            assert l >= left;
            assert r >= b;
            assert r <= right;

            while(true) {
                upperIndex = partition(
                        items, l + rand.nextInt((r - l) + 1), l, r);

                if(b < upperIndex) {
                    r = upperIndex - 1;
                }
                else if(b == upperIndex) {
                    break;
                }
                else {
                    l = upperIndex + 1;
                }
            }
        }

    }

    private Arrays() { throw new RuntimeException(); }
}
