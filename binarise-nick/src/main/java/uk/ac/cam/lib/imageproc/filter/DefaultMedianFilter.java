package uk.ac.cam.lib.imageproc.filter;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.arrays.Arrays;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

public class DefaultMedianFilter extends AbstractMedianFilter<float[]> {

    public DefaultMedianFilter(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod) {

        super(windowWidth, windowHeight, samplingMethod,
                ArrayTypes.arrayType(float[].class));
    }

    @Override
    public Array2D<float[]> apply(Array2D<float[]> src, Array2D<float[]> dest) {

        if(!(src.width() == dest.width() && src.height() == dest.height()))
            throw new IllegalArgumentException(
                    "src and dest images have differing dimentions");

        final SamplingMethod<float[]> s = this.samplingMethod;
        final int height = src.height();
        final int width = src.width();
        final int sampleWidth = this.windowWidth;
        final int sampleHeight = this.windowHeight;
        final Array2D<float[]> sample = Array2D.allocate(
                destType(), sampleWidth, sampleHeight);
        final float[] window = sample.data();
        final float[] destPixels = dest.data();

        for(int y = 0, i = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                s.sample(src, x, y, sampleWidth, sampleHeight, sample);

                destPixels[i++] = Arrays.medianInPlace(window);
            }
        }

        return dest;
    }
}
