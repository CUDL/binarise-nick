package uk.ac.cam.lib.imageproc.filter;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;

import java.util.Arrays;
import java.util.List;

import static java.util.Objects.requireNonNull;

public interface SingleOutputMultiArray2DFilter<S, D>
        extends MultiArray2DFilter<S, D> {

    @Override
    default int getOutputCount() {
        return 1;
    }

    @Override
    default List<Array2D<D>> apply(List<Array2D<S>> src,
                                   List<Array2D<D>> dest) {
        assert getOutputCount() == 1;
        MultiArray2DFilter.validateApplyArrays(this, src, dest);

        return Arrays.asList(applyWithSingleOutput(src, dest.get(0)));
    }

    default Array2D<D> applyWithSingleOutput(List<Array2D<S>> src) {
        return applyWithSingleOutput(src,
                Array2D.allocateSameShape(destType(), src.get(0)));
    }

    Array2D<D> applyWithSingleOutput(List<Array2D<S>> src, Array2D<D> dest);

    default <T> SingleOutputMultiArray2DFilter<S, T> andThen(
            Array2DFilter<D, T> next) {

        return new BridgingSingleOutputMultiArray2DFilter<>(this, next);
    }

    class BridgingSingleOutputMultiArray2DFilter<S, I, D>
            implements SingleOutputMultiArray2DFilter<S, D> {

        private final SingleOutputMultiArray2DFilter<S, I> first;
        private final Array2DFilter<I, D> second;

        public BridgingSingleOutputMultiArray2DFilter(
                SingleOutputMultiArray2DFilter<S, I> first,
                Array2DFilter<I, D> second) {

            this.first = requireNonNull(first);
            this.second = requireNonNull(second);
        }

        @Override
        public ArrayType<D> destType() {
            return second.destType();
        }

        @Override
        public int getInputCount() {
            return first.getInputCount();
        }

        @Override
        public Array2D<D> applyWithSingleOutput(List<Array2D<S>> src,
                                                Array2D<D> dest) {

            return second.apply(first.applyWithSingleOutput(src), dest);
        }
    }

    class MultiToSingleOutputMultiArray2DFilterAdapter<S, D>
            implements SingleOutputMultiArray2DFilter<S, D> {

        private final MultiArray2DFilter<S, D> filter;

        public MultiToSingleOutputMultiArray2DFilterAdapter(
                MultiArray2DFilter<S, D> filter) {

            this.filter = requireNonNull(filter);

            if(filter.getOutputCount() != 1)
                throw new IllegalStateException(
                        "Cannot wrap MultiArray2DFilter as SingleOutputMulti" +
                        "Array2DFilter: filter has more than one output");
        }

        @Override
        public ArrayType<D> destType() {
            return filter.destType();
        }

        @Override
        public int getInputCount() {
            return filter.getInputCount();
        }

        @Override
        public Array2D<D> applyWithSingleOutput(
                List<Array2D<S>> src, Array2D<D> dest) {

            return filter.apply(src, Arrays.asList(dest)).get(0);
        }
    }
}
