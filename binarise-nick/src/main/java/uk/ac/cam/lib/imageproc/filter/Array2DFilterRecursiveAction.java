package uk.ac.cam.lib.imageproc.filter;

import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Objects;
import java.util.concurrent.RecursiveAction;

/**
 * A base class for parallel filters over {@link Array2D}s.
 *
 * <p>The {@link #calculate()} method will be invoked with ranges of data
 * {@code <= maxSynchronous} in size.
 */
public abstract class Array2DFilterRecursiveAction<S, D> extends RecursiveAction {
    /**
     * A reasonable baseline value to use for maxSynchronous. Other values may
     * well be better depending on the workload.
     */
    public static final int REASONABLE_MAX_SYNCHRONOUS = 1024;

    protected final int start, end;
    protected final Array2D<S> src;
    protected final Array2D<D> dest;

    /**
     * The maximum number of data that can be computed synchronously by an
     * action.
     */
    protected final int maxSynchronous;

    public Array2DFilterRecursiveAction(
            int start, int end, int maxSynchronous,
            Array2D<S> src, Array2D<D> dest) {

        assert maxSynchronous > 0;

        this.start = start;
        this.end = end;
        this.maxSynchronous = maxSynchronous;

        this.src = Objects.requireNonNull(src);
        this.dest = Objects.requireNonNull(dest);
    }

    @Override
    protected void compute() {
        if (end - start > maxSynchronous) {
            recurse();
        } else {
            calculate();
        }
    }

    /**
     * Calculate the output values for data from {@code [start, end)}, writing
     * results to {@link #dest}.
     */
    protected abstract void calculate();

    protected abstract Array2DFilterRecursiveAction<S, D> withRange(
            int start, int end);

    private void recurse() {
        Array2DFilterRecursiveAction<S, D> left, right;
        int mid = start + (end - start) / 2;

        left = this.withRange(start, mid);
        right = this.withRange(mid, end);

        left.fork();
        right.fork();

        left.join();
        right.join();
    }
}
