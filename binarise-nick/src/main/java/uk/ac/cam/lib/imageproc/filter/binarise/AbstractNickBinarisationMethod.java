package uk.ac.cam.lib.imageproc.filter.binarise;

import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

import java.util.Objects;


public abstract class AbstractNickBinarisationMethod<T>
        implements NickBinarisationMethod<T> {

    public static final float K_MIN = -0.2f;
    public static final float K_MAX = -0.1f;

    /**
     * A {@code k} value which minimises noise but potentially results slightly
     * broken characters.
     */
    public static final float K_MINIMISE_NOISE = K_MIN;

    /**
     * A {@code k} value which prioritises clean extraction of characters at the
     * expense of greater noise.
     */
    public static final float K_MAXIMISE_TEXT_COMPLETENESS = K_MAX;

    protected final float k;
    protected final int windowWidth;
    protected final int windowHeight;
    protected final SamplingMethod<T> samplingMethod;

    public AbstractNickBinarisationMethod(
            float k, int windowWidth,
            SamplingMethod<T> samplingMethod, int windowHeight) {

        if(windowWidth < 1)
            throw new IllegalArgumentException(
                    "windowWidth was < 1: " + windowWidth);

        if(windowHeight < 1)
            throw new IllegalArgumentException(
                    "windowHeight was < 1: " + windowHeight);

        // k is intentionally not subject to validation
        this.k = k;
        this.windowWidth = windowWidth;
        this.samplingMethod = Objects.requireNonNull(samplingMethod);
        this.windowHeight = windowHeight;
    }
}
