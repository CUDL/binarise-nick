package uk.ac.cam.lib.imageproc.arrays;


import uk.ac.cam.lib.imageproc.arrays.Array2D;

import java.util.Objects;

import static java.lang.Math.min;

public class TypeConversion {

    public static Array2D<byte[]> toBytes(Array2D<float[]> src) {
        return toBytes(src, Array2D.allocateSameShape(byte[].class, src));
    }

    /**
     * Convert a normalised float array (with values in interval [0, 1] into a
     * byte array with values evenly distributed from {@link Byte#MIN_VALUE} to
     * {@link Byte#MAX_VALUE} such that:
     *
     * <ul>
     *     <li>{@code 0.0f -> -128} ({@link Byte#MIN_VALUE})</li>
     *     <li>{@code 0.5f -> 0} </li>
     *     <li>{@code 1.0f -> 127} ({@link Byte#MAX_VALUE})</li>
     * </ul>
     *
     * @param src An array of normalised floats.
     * @param dest The byte[] array to return
     * @return The dest array.
     */
    public static Array2D<byte[]> toBytes(
            Array2D<float[]> src, Array2D<byte[]> dest) {

        toBytes(src.data(), 0, dest.data(), 0, src.size());
        return dest;
    }

    public static void toBytes(float[] src, int srcPos,
                               byte[] dest, int destPos, int count) {
        Objects.requireNonNull(src);
        Objects.requireNonNull(dest);

        if(count < 0)
            throw new IllegalArgumentException("count was negative");

        if(srcPos < 0 || srcPos + count > src.length)
            throw new IndexOutOfBoundsException("src");

        if(destPos < 0 || destPos + count > dest.length)
            throw new IndexOutOfBoundsException("dest");

        for(int si = srcPos + count, di = destPos + count; si-- != srcPos;) {
            dest[--di] = (byte)min(src[si] * 256, 255);
        }
    }
}
