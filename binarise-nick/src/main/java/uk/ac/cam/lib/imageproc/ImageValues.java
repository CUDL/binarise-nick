package uk.ac.cam.lib.imageproc;


public final class ImageValues {

    public static final byte BYTE_UNSIGNED_MIN = (byte)0x00;
    public static final byte BYTE_UNSIGNED_MAX = (byte)0xFF;

    public static final byte WHITE_8BIT = BYTE_UNSIGNED_MAX;
    public static final byte BLACK_8BIT = BYTE_UNSIGNED_MIN;

    // zero is black, 1 is white in a binary image
    public static final byte WHITE_1BIT = (byte)0x01;
    public static final byte BLACK_1BIT = (byte)0x00;

    private ImageValues() { throw new RuntimeException(); }
}
