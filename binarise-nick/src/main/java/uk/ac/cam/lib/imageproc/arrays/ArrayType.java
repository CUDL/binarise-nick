package uk.ac.cam.lib.imageproc.arrays;


import java.util.Iterator;

public interface ArrayType<T> {

    Class<?> elementType();
    Class<T> arrayType();

    <ET> Accessor<ET, T> accessor(Class<ET> elementType);
    int length(T array);
    T allocate(int length);

    boolean areEqual(T array1, Object array2);
    int hashCodeOf(T array);

    interface Accessor<ET, AT> {
        ET get(AT array, int i);
        Iterator<ET> iterate(AT array);
    }
}
