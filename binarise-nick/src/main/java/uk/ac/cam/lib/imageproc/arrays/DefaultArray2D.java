package uk.ac.cam.lib.imageproc.arrays;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class DefaultArray2D<T>
        implements Array2D<T> {

    private final ArrayType<T> type;
    private final T data;
    private final int width, height;

    public DefaultArray2D(ArrayType<T> type, T data, int width) {
        this.type = Objects.requireNonNull(type);
        this.data = Objects.requireNonNull(data);

        if(data.getClass() != type.arrayType())
            throw new IllegalArgumentException(
                    "array type and array are mismatched types");

        int length = type.length(data);

        if(width < 0)
            throw new IllegalArgumentException("width was negative: " + width);

        if(width == 0) {
            this.width = 0;
            this.height = 0;

            if(length != 0)
                throw new IllegalArgumentException(
                        "width was 0 but data has length: " + length);
            return;
        }

        if(length % width != 0)
            throw new IllegalArgumentException(String.format(
                    "data array length was not a multiple of width. " +
                    "width: %d length: %d", width, length));

        this.width = width;
        this.height = length / width;
    }

    @Override
    public T data() {
        return data;
    }

    @Override
    public int width() {
        return width;
    }

    @Override
    public int height() {
        return height;
    }

    @Override
    public ArrayType<T> type() {
        return type;
    }

    private static final int COMPRESSED_STRING_REP_THRESHOLD = 1000;
    private static final int COMPRESSED_STRING_REP_PREVIEW_COUNT = 3;

    @Override
    public String toString() {
        if(width() > COMPRESSED_STRING_REP_THRESHOLD ||
           height() > COMPRESSED_STRING_REP_THRESHOLD) {

            return toStringSummary(COMPRESSED_STRING_REP_PREVIEW_COUNT);
        }

        return toStringFull();
    }

    public String toStringSummary(int previewCount) {
        Array2D<T> prev = Array2D.allocate(
                type(), min(previewCount * 2, width()),
                min(previewCount * 2, height()));

        boolean compressX = width() > previewCount * 2;
        boolean compressY = height() > previewCount * 2;

        assert compressX || compressY;

        int topHeight =    min(previewCount, height() / 2);
        int bottomHeight = min(previewCount, height - (height() / 2));
        int leftWidth =    min(previewCount, width() / 2);
        int rightWidth =   min(previewCount, width() - (width() / 2));

        // Top-left
        ArrayCopy.copy(this, 0, 0, prev, 0, 0, leftWidth, topHeight);
        // Top-right
        ArrayCopy.copy(
                this, width() - rightWidth, 0,
                prev, prev.width() - rightWidth, 0,
                rightWidth, topHeight);
        // Bottom-left
        ArrayCopy.copy(
                this, 0, height() - bottomHeight,
                prev, 0, prev.height() - bottomHeight,
                leftWidth, bottomHeight);
        // Bottom-right
        ArrayCopy.copy(
                this, width() - rightWidth, height() - bottomHeight,
                prev, prev.width() - rightWidth, prev.height() - bottomHeight,
                rightWidth, bottomHeight);

        List<String> strValues = stream(
                prev.type().accessor(prev.type().elementType())
                    .iterate(prev.data()))
                .map(Object::toString)
                .collect(Collectors.toList());

        int widest =
                strValues.stream().mapToInt(String::length).max().getAsInt();

        StringBuilder sb = new StringBuilder();
        sb.append("Array2D.withData(").append(type())
                .append(", new ").append(type.arrayType().getSimpleName())
                .append("{\n");

        for(int y = 0; y < prev.height(); ++y) {
            for(int p = 0; p < 8; ++p)
                sb.append(' ');

            if(compressY && y == previewCount)
                sb.append("...,\n        ");

            for(int x = 0; x < prev.width(); ++x) {
                if(compressX && x == previewCount)
                    sb.append("..., ");

                String s = strValues.get(prev.indexOf(x, y));
                for(int p = 0; p < widest - s.length(); ++p)
                    sb.append(' ');

                sb.append(s);

                if(x == prev.width() - 1) {
                    if(y != height() - 1)
                        sb.append(',');
                    sb.append('\n');
                }
                else
                    sb.append(", ");
            }
        }

        sb.append("}, ").append(width()).append(")");
        return sb.toString();
    }

    public String toStringFull() {
        List<String> strValues = stream(type().accessor(type.elementType())
                .iterate(data()))
                .map(Object::toString)
                .collect(Collectors.toList());

        OptionalInt widestElement =
                strValues.stream().mapToInt(String::length).max();

        if(!widestElement.isPresent()) {
            return String.format("DefaultArray2D(%s, new %s[]{}, %d",
                    type(), type.arrayType().getSimpleName(), width());
        }

        StringBuilder sb = new StringBuilder();

        sb.append("Array2D.withData(").append(type())
                .append(", new ").append(type.arrayType().getSimpleName())
                .append("{\n");

        int widest = widestElement.getAsInt();
        for(int y = 0; y < height(); ++y) {
            for(int p = 0; p < 8; ++p)
                sb.append(' ');

            for(int x = 0; x < width(); ++x) {
                String s = strValues.get(indexOf(x, y));
                for(int p = 0; p < widest - s.length(); ++p)
                    sb.append(' ');

                sb.append(s);

                if(x == width() - 1) {
                    if(y != height() - 1)
                        sb.append(',');
                    sb.append('\n');
                }
                else
                    sb.append(", ");
            }
        }

        sb.append("}, ").append(width()).append(")");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(!(obj instanceof Array2D))
            return false;

        Array2D<?> other = (Array2D<?>)obj;
        return this.width == other.width() && this.height == other.height() &&
                type.areEqual(this.data, other.data());
    }

    @Override
    public int hashCode() {
        int result = type.hashCodeOf(this.data);
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }

    protected static void validateWidthHeight(int width, int height) {
        if(width < 0)
            throw new IllegalArgumentException("width was < 0: " + width);
        if(height < 0)
            throw new IllegalArgumentException("height was < 0: " + height);
    }

    private static <T> Stream<T> stream(Iterator<T> it) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(
                it, Spliterator.SIZED | Spliterator.SIZED), false);
    }
}
