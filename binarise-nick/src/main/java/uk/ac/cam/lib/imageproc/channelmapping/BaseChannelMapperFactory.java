package uk.ac.cam.lib.imageproc.channelmapping;

public abstract class BaseChannelMapperFactory<S, D>
        implements ChannelMapperFactory<S, D> {

    private final int inputChannelCount, outputChannelCount;

    public BaseChannelMapperFactory(int inputChannelCount,
                                    int outputChannelCount) {
        this.inputChannelCount = inputChannelCount;
        this.outputChannelCount = outputChannelCount;
    }

    @Override
    public int inputChannelCount() {
        return inputChannelCount;
    }

    @Override
    public int outputChannelCount() {
        return outputChannelCount;
    }
}
