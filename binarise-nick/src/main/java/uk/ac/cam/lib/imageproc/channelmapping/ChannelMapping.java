package uk.ac.cam.lib.imageproc.channelmapping;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.filter.MultiArray2DFilter;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ChannelMapping {

    public static final int DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE = 256;

    /**
     * Create a list of channel buffer arrays of the specified type.
     *
     * @param arrayType The <b>array</b> type of the buffer. e.g. {@code float[]}.
     * @param channels The number of individual channels to allocate
     * @param length The number of elements per channel
     * @return The list of buffer arrays
     */
    public static <D> List<D> createTransferBuffers(
            Class<?> arrayType, int channels, int length) {

        assert arrayType.isArray();

        @SuppressWarnings("unchecked")
        D[] arrays = (D[]) Array.newInstance(arrayType.getComponentType(),
                channels, length);

        return Collections.unmodifiableList(Arrays.asList(arrays));
    }

    private static <T> void validateBufferAndArrayCount(
            List<T> transferBuffers, List<Array2D<T>> arrays,
            int arrayOffset, int count) {
        if(transferBuffers.size() != arrays.size())
            throw new IllegalArgumentException(
                    "mismatched number of buffers and arrays");

        validateAllSameShape(arrays);

        if(count < 1)
            throw new IllegalArgumentException("count was < 1: " + count);

        int size = arrays.get(0).size();
        if(arrayOffset < 0 || arrayOffset >= size || arrayOffset + count > size)
            throw new IllegalArgumentException(
                    "array offset and count specify a region outside the " +
                    "array bounds");
    }

    public static <T> void fillTransferBuffers(
            List<T> transferBuffers, List<Array2D<T>> arrays,
            int arrayOffset, int count) {

        validateBufferAndArrayCount(
                transferBuffers, arrays, arrayOffset, count);

        for(int i = transferBuffers.size(); i-- != 0;)
            System.arraycopy(arrays.get(i).data(), arrayOffset,
                             transferBuffers.get(i), 0, count);
    }

    public static <T> void drainTransferBuffers(
            List<T> transferBuffers, List<Array2D<T>> arrays,
            int arrayOffset, int count) {

        validateBufferAndArrayCount(transferBuffers, arrays,
                                    arrayOffset, count);

        for(int i = transferBuffers.size(); i-- != 0;) {
            System.arraycopy(transferBuffers.get(i), 0,
                             arrays.get(i).data(), arrayOffset, count);
        }
    }

    public static <T> List<Array2D<T>> createDestArrays(
            int width, int height, int count, Class<T> cls) {

        return createDestArrays(width, height, count,
                                ArrayTypes.arrayType(cls));
    }

    public static <T> List<Array2D<T>> createDestArrays(
            int width, int height, int count, ArrayType<T> type) {

        return Collections.unmodifiableList(IntStream.range(0, count)
                .mapToObj(i -> Array2D.allocate(
                        type, width, height))
                .collect(Collectors.toList()));
    }

    public static <T1, T2> Array2D<T2> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            Array2D<T1> src, Class<T2> dest) {
        return map(channelMapperFactory, src,
                Array2D.allocateSameShape(dest, src),
                DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE);
    }

    public static <T1, T2> Array2D<T2> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            Array2D<T1> src, Array2D<T2> dest, int bufferSize) {

        return map(channelMapperFactory, Collections.singletonList(src),
                   Collections.singletonList(dest), bufferSize).get(0);
    }

    private static <T> void validateAllSameShape(List<Array2D<T>> arrays) {
        if(arrays.isEmpty())
            return;

        Array2D<?> a = arrays.get(0);
        for(int i = 1; i < arrays.size(); ++i) {
            Array2D<?> b = arrays.get(i);

            if(a.width() != b.width() || a.height() != b.height())
                throw new IllegalArgumentException(
                        "Not all arrays are the same shape");
        }
    }

    private static <T1, T2> void validateAllSameShape(
            List<Array2D<T1>> a1, List<Array2D<T2>> a2) {
        validateAllSameShape(a1);
        validateAllSameShape(a2);

        if(!(a1.isEmpty() || a2.isEmpty())) {
            Array2D<?> a = a1.get(0);
            Array2D<?> b = a2.get(0);

            if(a.width() != b.width() || a.height() != b.height())
                throw new IllegalArgumentException(
                        "Not all arrays are the same shape");
        }
    }

    public static <T1, T2> List<Array2D<T2>> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            List<Array2D<T1>> src, Class<T2> destType) {

        return map(channelMapperFactory, src, ArrayTypes.arrayType(destType));
    }

    public static <T1, T2> List<Array2D<T2>> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            List<Array2D<T1>> src, ArrayType<T2> destType) {

        int outputChannels = channelMapperFactory.outputChannelCount();

        List<Array2D<T2>> dest = IntStream.range(0, outputChannels)
                .mapToObj(x -> Array2D.allocateSameShape(destType, src.get(0)))
                .collect(Collectors.toList());

        return map(channelMapperFactory, src, dest);
    }

    public static <T1, T2> List<Array2D<T2>> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            List<Array2D<T1>> src, List<Array2D<T2>> dest) {

        return map(channelMapperFactory, src, dest,
                   DEFAULT_CHANNEL_MAPPER_BUFFER_SIZE);
    }

    public static <T1, T2> List<Array2D<T2>> map(
            ChannelMapperFactory<T1, T2> channelMapperFactory,
            List<Array2D<T1>> src, List<Array2D<T2>> dest, int bufferSize) {

        if(src.isEmpty())
            throw new IllegalArgumentException("src is empty");
        if(dest.isEmpty())
            throw new IllegalArgumentException("dest is empty");

        validateAllSameShape(src, dest);

        if(src.size() != channelMapperFactory.inputChannelCount())
            throw new IllegalArgumentException(String.format(
                    "src array count does not match channel mapper input " +
                    "channel count. got: %d, expected: %d", src.size(),
                    channelMapperFactory.inputChannelCount()));

        if(dest.size() != channelMapperFactory.outputChannelCount())
            throw new IllegalArgumentException(String.format(
                    "dest array count does not match channel mapper output " +
                            "channel count. got: %d, expected: %d", dest.size(),
                    channelMapperFactory.outputChannelCount()));

        if(bufferSize < 1)
            throw new IllegalArgumentException(
                    "bufferSize was < 1: " + bufferSize);

        List<T1> srcTxBuf = createTransferBuffers(
                src.get(0).type().arrayType(), src.size(), bufferSize);
        List<T2> destTxBuf = createTransferBuffers(
                dest.get(0).type().arrayType(), dest.size(), bufferSize);

        ChannelMapper<T1, T2> mapper =
                channelMapperFactory.getMapper(bufferSize);

        for(int j = 0, max = src.get(0).size(); j < max; j += bufferSize) {

            int count = Math.min(j + bufferSize, max) - j;

            fillTransferBuffers(srcTxBuf, src, j, count);
            mapper.mapChannels(srcTxBuf, destTxBuf, count);
            drainTransferBuffers(destTxBuf, dest, j, count);
        }

        return dest;
    }

    public static <S, D> MultiArray2DFilter<S, D> asFilter(
            ChannelMapperFactory<S, D> mapperFactory, Class<D> destType) {
        return asFilter(mapperFactory, ArrayTypes.arrayType(destType));
    }

    public static <S, D> MultiArray2DFilter<S, D> asFilter(
            ChannelMapperFactory<S, D> mapperFactory, ArrayType<D> destType) {
        return new ChannelMapperMultiArray2DFilter<>(mapperFactory, destType);
    }

    private ChannelMapping() {}
}
