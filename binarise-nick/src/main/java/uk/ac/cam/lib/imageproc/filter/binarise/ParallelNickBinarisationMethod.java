package uk.ac.cam.lib.imageproc.filter.binarise;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.Arrays;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;
import uk.ac.cam.lib.imageproc.filter.Array2DFilterRecursiveAction;

import java.util.Objects;
import java.util.concurrent.ForkJoinPool;

public class ParallelNickBinarisationMethod
        extends AbstractNickBinarisationMethod<float[]> {

    public static final int DEFAULT_MAX_SYNCHRONOUS =
            Array2DFilterRecursiveAction.REASONABLE_MAX_SYNCHRONOUS;

    private final ForkJoinPool executionPool;
    private final int maxSynchronous;

    public ParallelNickBinarisationMethod(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, float k) {

        this(windowWidth, windowHeight, samplingMethod, k,
                ForkJoinPool.commonPool(), DEFAULT_MAX_SYNCHRONOUS);
    }

    public ParallelNickBinarisationMethod(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, float k,
            ForkJoinPool executionPool) {

        this(windowWidth, windowHeight, samplingMethod, k,
                executionPool, DEFAULT_MAX_SYNCHRONOUS);
    }

    public ParallelNickBinarisationMethod(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, float k,
            ForkJoinPool executionPool, int maxSynchronous) {

        super(k, windowWidth, samplingMethod, windowHeight);

        if(maxSynchronous < 1)
            throw new IllegalArgumentException(
                    "maxSynchronous must be positive, was: " +
                            maxSynchronous);

        this.executionPool = Objects.requireNonNull(executionPool);
        this.maxSynchronous = maxSynchronous;
    }

    @Override
    public Array2D<byte[]> apply(
            Array2D<float[]> src, Array2D<byte[]> dest) {

        executionPool.invoke(new NickBinarisationRecursiveAction(
                0, src.size(), maxSynchronous, src, dest));

        return dest;
    }

    private class NickBinarisationRecursiveAction
            extends Array2DFilterRecursiveAction<float[], byte[]> {

        public NickBinarisationRecursiveAction(
                int start, int end, int maxSynchronous,
                Array2D<float[]> src, Array2D<byte[]> dest) {

            super(start, end, maxSynchronous, src, dest);
        }

        @Override
        protected void calculate() {
            Array2D<float[]> s = src;
            Array2D<byte[]> d = dest;
            SamplingMethod<float[]> sampler = samplingMethod;

            final float k = ParallelNickBinarisationMethod.this.k;
            final int width = s.width();

            int sampleWidth = windowWidth;
            int sampleHeight = windowHeight;

            final Array2D<float[]> sample = Array2D.allocate(
                    src.type(), sampleWidth, sampleHeight);
            float[] window = sample.data();
            int windowSize = window.length;

            float[] pixels = s.data();
            byte[] destPixels = d.data();

            int xStart = start % width;
            int i = start;
            for(int y = start / width; i < end; ++y) {
                for(int x = xStart; x < width && i < end; ++x, ++i) {
                    sampler.sample(s, x, y, sampleWidth, sampleHeight, sample);

                    float mean = Arrays.mean(window);
                    float squareSum = Arrays.sum(Arrays.square(window));
                    float threshold = mean + k * (float)Math.sqrt(
                            (squareSum - mean * mean) / windowSize);

                    destPixels[i] = (byte)Boolean.compare(
                            pixels[y * width + x] >= threshold, false);
                }

                xStart = 0;
            }
        }

        @Override
        protected NickBinarisationRecursiveAction withRange(
                int start, int end) {

            return new NickBinarisationRecursiveAction(
                    start, end, maxSynchronous, src, dest);
        }
    }
}
