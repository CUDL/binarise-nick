package uk.ac.cam.lib.imageproc.filter.binarise;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.Arrays;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

/**
 * An implementation of the NICK binarisation method as described in the paper:
 * <a href="https://doi.org/10.1117/12.805827">Comparison of Niblack inspired
 * binarization methods for ancient documents</a>
 */
public class DefaultNickBinarisationMethod
        extends AbstractNickBinarisationMethod<float[]> {

    public DefaultNickBinarisationMethod(
            int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, float k) {
        super(k, windowWidth, samplingMethod, windowHeight);
    }

    @Override
    public Array2D<byte[]> apply(Array2D<float[]> image, Array2D<byte[]> result) {
        final SamplingMethod<float[]> s = samplingMethod;
        final float k = this.k;
        final int width = image.width();
        final int height = image.height();
        final int sampleWidth = this.windowWidth;
        final int sampleHeight = this.windowHeight;
        final Array2D<float[]> sample = Array2D.allocate(
                image.type(), this.windowWidth, this.windowHeight);
        final float[] window = sample.data();
        final float[] pixels = image.data();
        final byte[] dest = result.data();
        final float windowSize = window.length;


        for(int y = 0, i = 0; y < height; ++y) {
            for(int x = 0; x < width; ++x) {
                s.sample(image, x, y, sampleWidth, sampleHeight, sample);

                float mean = Arrays.mean(window);
                float squareSum = Arrays.sum(Arrays.square(window));
                float threshold = mean + k * (float)Math.sqrt(
                        (squareSum - mean * mean) / windowSize);

                dest[i++] = pixels[y * width + x] >= threshold ?
                        (byte)1 : (byte)0;
            }
        }

        return result;
    }
}
