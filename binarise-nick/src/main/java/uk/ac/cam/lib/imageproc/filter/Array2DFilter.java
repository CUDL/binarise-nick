package uk.ac.cam.lib.imageproc.filter;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayCopy;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.arrays.Arrays;

import java.util.Objects;

public interface Array2DFilter<S, D> {

    ArrayType<D> destType();

    Array2D<D> apply(Array2D<S> src, Array2D<D> dest);

    default Array2D<D> apply(Array2D<S> src) {
        return apply(src, Array2D.allocateSameShape(destType(), src));
    }

    static <T> Array2DFilter<T, T> noopFilter(Class<T> destClass) {
        return noopFilter(ArrayTypes.arrayType(destClass));
    }

    static <T> Array2DFilter<T, T> noopFilter(ArrayType<T> destType) {
        Objects.requireNonNull(destType, "destType was null");

        return new NoopFilter<>(destType);
    }

    class NoopFilter<T> implements Array2DFilter<T, T> {

        private final ArrayType<T> destType;

        public NoopFilter(ArrayType<T> destType) {
            this.destType = Objects.requireNonNull(destType);
        }

        @Override
        public ArrayType<T> destType() {
            return destType;
        }

        @Override
        public Array2D<T> apply(Array2D<T> src, Array2D<T> dest) {
            ArrayCopy.copy(src, 0, 0, dest, 0, 0, src.width(), src.height());
            return dest;
        }
    }
}
