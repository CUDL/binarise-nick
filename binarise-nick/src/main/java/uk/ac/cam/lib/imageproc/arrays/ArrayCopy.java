package uk.ac.cam.lib.imageproc.arrays;


import java.util.Objects;

public class ArrayCopy {

    private ArrayCopy() {}

    public static <T> Array2D<T> copy(Array2D<T> src, int sx, int sy,
                               Array2D<T> dst, int dx, int dy,
                               int width, int height) {

        Objects.requireNonNull(src);
        Objects.requireNonNull(dst);

        if(!src.type().equals(dst.type()))
            throw new IllegalArgumentException(
                    "src and dst are not of the same type");

        if(src.data() == dst.data())
            throw new IllegalArgumentException(
                    "src and dst use the same array");

        if(width < 0)
            throw new IllegalArgumentException("width was negative");
        if(height < 0)
            throw new IllegalArgumentException("height was negative");

        checkBounds(src, sx, sy, "src x, y out of bounds");
        checkBounds(src, sx + width - 1, sy + height - 1,
                    "src x, y out of bounds");
        checkBounds(dst, dx, dy, "dst x, y out of bounds");
        checkBounds(dst, dx + width - 1, dy + height - 1,
                    "dst x, y out of bounds");

        Object sd = src.data();
        Object dd = dst.data();

        for(int sw = src.width(),
                dw = dst.width(),
                isy = sy + height - 1,
                idy = dy + height - 1,
                is = isy * sw + sx,
                id = idy * dw + dx; isy >= sy; --isy, is -= sw, id -= dw) {

            System.arraycopy(sd, is, dd, id, width);
        }

        return dst;
    }

    static void checkBounds(Array2D<?> array, int x, int y, String name) {
        if(x < 0 || y < 0 || x >= array.width() || y >= array.height())
            throw new IndexOutOfBoundsException(name);
    }
}
