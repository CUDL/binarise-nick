package uk.ac.cam.lib.imageproc;

import java.util.Objects;

public enum LengthUnit {

    METERS {
        public float toMeters(float f) { return f; }
        public float toCentimeters(float f) { return f * (CM_PER_M); }
        public float toMillimeters(float f) { return f * (CM_PER_M * MM_PER_CM); }
        public float toInches(float f) { return f * (CM_PER_M / CM_PER_IN); }
    },

    CENTIMETERS {
        public float toMeters(float f) { return f / (CM_PER_M); }
        public float toCentimeters(float f) { return f; }
        public float toMillimeters(float f) { return f * (MM_PER_CM); }
        public float toInches(float f) { return f / (CM_PER_IN); }
    },

    MILLIMETERS {
        public float toMeters(float f) { return f / (MM_PER_CM * CM_PER_M); }
        public float toCentimeters(float f) { return f / MM_PER_CM; }
        public float toMillimeters(float f) { return f; }
        public float toInches(float f) { return f / (MM_PER_CM * CM_PER_IN); }
    },

    INCHES {
        public float toMeters(float f) { return f * (CM_PER_IN / CM_PER_M); }
        public float toCentimeters(float f) { return f * (CM_PER_IN); }
        public float toMillimeters(float f) {
            return f * (CM_PER_IN * MM_PER_CM);
        }
        public float toInches(float f) { return f; }
    };

    private static final float
            CM_PER_M = 100,
            MM_PER_CM = 10,
            CM_PER_IN = 2.54f;

    public abstract float toMeters(float f);
    public abstract float toCentimeters(float f);
    public abstract float toMillimeters(float f);
    public abstract float toInches(float f);

    public FloatToFloatFunction getConverterTo(LengthUnit to) {
        switch (Objects.requireNonNull(to)) {
            case METERS: return this::toMeters;
            case CENTIMETERS: return this::toCentimeters;
            case MILLIMETERS: return this::toMillimeters;
            case INCHES: return this::toInches;
        }

        throw new RuntimeException("Unknown LengthUnit: " + to);
    }

    public float to(LengthUnit unit, float f) {
        switch (Objects.requireNonNull(unit)) {
            case METERS: return this.toMeters(f);
            case CENTIMETERS: return this.toCentimeters(f);
            case MILLIMETERS: return this.toMillimeters(f);
            case INCHES: return this.toInches(f);
        }

        throw new RuntimeException("Unknown LengthUnit: " + unit);
    }
}
