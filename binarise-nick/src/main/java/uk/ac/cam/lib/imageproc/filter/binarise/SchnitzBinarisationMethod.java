package uk.ac.cam.lib.imageproc.filter.binarise;

import uk.ac.cam.lib.imageproc.LengthUnit;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.ArrayTypes;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapperFactory;
import uk.ac.cam.lib.imageproc.channelmapping.ChannelMapping;
import uk.ac.cam.lib.imageproc.channelmapping.ColourModels;
import uk.ac.cam.lib.imageproc.channelmapping.Greyscale.BlueBoostColorimetricGreyscaleChannelMapper;
import uk.ac.cam.lib.imageproc.filter.Array2DFilter;
import uk.ac.cam.lib.imageproc.filter.MedianFilter;
import uk.ac.cam.lib.imageproc.filter.MultiArray2DFilter;
import uk.ac.cam.lib.imageproc.filter.ParallelMedianFilter;
import uk.ac.cam.lib.imageproc.filter.SingleOutputMultiArray2DFilter;
import uk.ac.cam.lib.imageproc.objectlabel.DefaultImageObjectFilter;
import uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter;
import uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter.Connectivity;
import uk.ac.cam.lib.imageproc.objectlabel.ImageObjectFilters;

import java.util.List;
import java.util.Objects;
import java.util.OptionalDouble;

import static java.util.Objects.requireNonNull;
import static uk.ac.cam.lib.imageproc.filter.MultiArray2DFilter.requireIOCounts;

/**
 * An all-in-one binarisation method which involves several steps:
 *
 * <ol>
 *     <li>Noise reduction</li>
 *     <li>Colour to greyscale conversion</li>
 *     <li>Binarisation</li>
 *     <li>Object filtering</li>
 * </ol>
 *
 * <p>The following describes the typical, intended implementation, but
 * variations are of course possible.</p>
 *
 * <h3>Noise reduction</h3>
 * The colour image is filtered to reduce noise, which tends to improve
 * binarisation results by reducing noisy, rough edges around regions such as
 * characters.
 *
 * <p>Applying a
 * {@link uk.ac.cam.lib.imageproc.filter.MedianFilter median filter} works well
 * here as it is effective at reducing noise while preserving hard edges, which
 * is beneficial for clean binarisation. Additionally, applying a median filter
 * to the image in the HSV colour space does a better job of reducing colour
 * noise than in RGB.
 *
 * <h3>Colour to greyscale conversion</h3>
 * The RGB colour image is reduced to a single grey channel, for input to the
 * binarisation step.
 *
 * <p>See {@link uk.ac.cam.lib.imageproc.channelmapping.Greyscale Greyscale} for
 * some implementations.
 *
 * <h3>Binarisation</h3>
 * The continuous grey image is mapped to a binary (black and white only) image.
 * The {@link NickBinarisationMethod NICK} binarisation algorithm is implemented
 * here and works well in general.
 *
 * <h3>Object filtering</h3>
 * After binarisation, the image often has numerous areas which are falsely
 * detected as being of interest (i.e. marked black) by the binarisation
 * algorithm. For example, high frequency noise (speckley individual or small
 * clumps of pixels) in dark backgrounds and page edges where a white page lies
 * on a dark background.
 *
 * <p>These undesirable features can be effectively removed using region
 * labeling to detect contiguous objects in the image, and excluding detected
 * regions based on size. This requires some application-specific knowlege of
 * the typical size of features that will appear.
 */
public class SchnitzBinarisationMethod
        implements SingleOutputMultiArray2DFilter<float[], byte[]> {

    public static final int DENOISE_CHANNELS = 3;
    public static final int GREY_CHANNELS = 1;

    private final MultiArray2DFilter<float[], float[]> denoise;
    private final MultiArray2DFilter<float[], float[]> greyscale;
    private final BinarisationMethod<float[]> binarise;
    private final Array2DFilter<byte[], byte[]> objectFilter;

    private final SingleOutputMultiArray2DFilter<float[], byte[]>
            combinedFilters;

    /**
     * @see Builder
     */
    public SchnitzBinarisationMethod(
            MultiArray2DFilter<float[], float[]> denoise,
            MultiArray2DFilter<float[], float[]> greyscale,
            BinarisationMethod<float[]> binarise,
            Array2DFilter<byte[], byte[]> objectFilter) {

        this.denoise = requireIOCounts(
                requireNonNull(denoise), DENOISE_CHANNELS, DENOISE_CHANNELS,
                "denoise");

        this.greyscale = requireIOCounts(
                requireNonNull(greyscale), DENOISE_CHANNELS, GREY_CHANNELS,
                "greyscale");

        this.binarise = requireNonNull(binarise);
        this.objectFilter = requireNonNull(objectFilter);

        this.combinedFilters = combineFilters();
    }

    private SingleOutputMultiArray2DFilter<float[], byte[]> combineFilters() {
        return denoise.andThen(greyscale).withSingleOutput()
                .andThen(binarise)
                .andThen(objectFilter);
    }

    @Override
    public ArrayType<byte[]> destType() {
        return ArrayTypes.arrayType(byte[].class);
    }

    @Override
    public int getInputCount() {
        return DENOISE_CHANNELS;
    }

    @Override
    public Array2D<byte[]> applyWithSingleOutput(
            List<Array2D<float[]>> src, Array2D<byte[]> dest) {

        return combinedFilters.applyWithSingleOutput(src, dest);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private static final int DEFAULT_DENOISE_WINDOW = 3;
        private static final SamplingMethod<float[]> DEFAULT_SAMPLER =
                new ReflectSamplingMethod();

        private static final ChannelMapperFactory<float[], float[]>
                DEFAULT_GREYSCALE_MAPPER =
                BlueBoostColorimetricGreyscaleChannelMapper.factory();

        private static final int DEFAULT_NICK_WINDOW = 30;
        private static final float DEFAULT_NICK_K =
                AbstractNickBinarisationMethod.K_MAXIMISE_TEXT_COMPLETENESS;


        private MultiArray2DFilter<float[], float[]> denoise;
        private MultiArray2DFilter<float[], float[]> greyscale;
        private BinarisationMethod<float[]> binarise;
        private Array2DFilter<byte[], byte[]> objectFilter;

        protected Builder() {}

        public Builder withDenoiseFilter(
                MultiArray2DFilter<float[], float[]> denoise) {

            this.denoise = requireIOCounts(
                    requireNonNull(denoise), DENOISE_CHANNELS, DENOISE_CHANNELS,
                    "denoise");
            return this;
        }

        public Builder withGreyscaleFilter(
                MultiArray2DFilter<float[], float[]> greyscale) {

            this.greyscale = requireIOCounts(
                    requireNonNull(greyscale), DENOISE_CHANNELS, GREY_CHANNELS,
                    "greyscale");
            return this;
        }

        public Builder withBinarisationMethod(
                BinarisationMethod<float[]> binarisationMethod) {

            this.binarise = Objects.requireNonNull(binarisationMethod);
            return this;
        }

        public Builder withObjectFilter(
                Array2DFilter<byte[], byte[]> objectFilter) {

            this.objectFilter = Objects.requireNonNull(objectFilter);
            return this;
        }

        public Builder withHsvMedianFilter(int windowWidth, int windowHeight) {
            return withHsvMedianFilter(windowWidth, windowHeight,
                    DEFAULT_SAMPLER);
        }

        public Builder withHsvMedianFilter(
                int windowWidth, int windowHeight,
                SamplingMethod<float[]> samplingMethod) {

            MedianFilter<float[]> medianFloat = new ParallelMedianFilter(
                    windowWidth, windowHeight, samplingMethod);

            return this.withDenoiseFilter(
                ColourModels.inHsv(
                        MultiArray2DFilter.fromFilters(
                                medianFloat, medianFloat, medianFloat)));
        }

        public Builder mapToGreyscaleWith(
                ChannelMapperFactory<float[], float[]> mapperFactory) {

            return this.withGreyscaleFilter(ChannelMapping.asFilter(
                    mapperFactory, float[].class));
        }

        /**
         * Detect connected regions of the binary image and exclude regions
         * less than or greater than the specified area.
         *
         * @param resolution The resolution of the input image in pixels per
         *                   resolution unit
         * @param resolutionUnit The unit of resolution
         * @param minObjectArea The minimum area for an object to be retained
         * @param maxObjectArea The maximum area for an object to be retained
         * @param areaUnit The unit of the object area values
         * @return A filter which excludes regions based on the above.
         */
        public Builder excludeSmallAndLargeObjectsByArea(
                float resolution, LengthUnit resolutionUnit,
                OptionalDouble minObjectArea, OptionalDouble maxObjectArea,
                LengthUnit areaUnit) {

            return this.withObjectFilter(
                    new DefaultImageObjectFilter(
                        new DefaultRegionLabelFilter(Connectivity.FULL),
                        ImageObjectFilters.objectWithPhysicalSizeMatcher(
                                resolution, resolutionUnit,
                                minObjectArea, maxObjectArea, areaUnit)));
        }

        /**
         * @see #excludeSmallAndLargeObjectsByArea(float, LengthUnit, OptionalDouble, OptionalDouble, LengthUnit)
         */
        public Builder excludeSmallAndLargeObjectsByArea(
                float resolution, LengthUnit resolutionUnit,
                float minObjectArea, float maxObjectArea, LengthUnit areaUnit) {

            return excludeSmallAndLargeObjectsByArea(resolution, resolutionUnit,
                    OptionalDouble.of(minObjectArea),
                    OptionalDouble.of(maxObjectArea), areaUnit);
        }

        private void populateDefaults() {

            if(denoise == null)
                this.withHsvMedianFilter(
                        DEFAULT_DENOISE_WINDOW, DEFAULT_DENOISE_WINDOW);

            if(greyscale == null)
                this.mapToGreyscaleWith(DEFAULT_GREYSCALE_MAPPER);

            if(binarise == null)
                this.withBinarisationMethod(new ParallelNickBinarisationMethod(
                        DEFAULT_NICK_WINDOW, DEFAULT_NICK_WINDOW,
                        DEFAULT_SAMPLER, DEFAULT_NICK_K));

            if(objectFilter == null) {
                this.withObjectFilter(Array2DFilter.noopFilter(byte[].class));
            }
        }

        public SchnitzBinarisationMethod build() {
            populateDefaults();
            return new SchnitzBinarisationMethod(
                    denoise, greyscale, binarise, objectFilter);
        }
    }
}
