package uk.ac.cam.lib.imageproc.arrays;


import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;

/**
 * Encapsulates a method for sampling a rectangular region of an image,
 * potentially allowing the region to extend outside the bounds of the image.
 */
public interface SamplingMethod<T> {

    ArrayType<T> type();

    Array2D<T> sample(Array2D<T> image, int x, int y,
                      int width, int height, Array2D<T> result);

    default Array2D<T> sample(Array2D<T> image,
                              int x, int y, int width, int height) {

        return sample(image, x, y, width, height,
                      Array2D.allocate(type(), width, height));
    }
}
