package uk.ac.cam.lib.imageproc.arrays;

import java.util.Objects;

public class GridArrayReflectSamplingMethod implements SamplingMethod<float[]> {

    @Override
    public ArrayType<float[]> type() {
        return ArrayTypes.arrayType(float[].class);
    }

    @Override
    public Array2D<float[]> sample(
            Array2D<float[]> image, int x, int y,
            int width, int height, Array2D<float[]> result) {

        Objects.requireNonNull(image);
        Objects.requireNonNull(result);
        if(width < 0)
            throw new IllegalArgumentException("width was negative: " + width);
        if(height < 0)
            throw new IllegalArgumentException("height was negative: " + height);

        int left = x - (width / 2);
        int right = x + (width - (width / 2));
        int top = y - (height / 2);
        int bottom = y + (height - (height / 2));

        assert right - left == width;
        assert bottom - top == height;

        float[] src = image.data();
        float[] dest = result.data();

        // Handle fast and common case where an image border is not involved
        if(left >= 0 && top >= 0 && right <= image.width() &&
                bottom <= image.height()) {

            GridArrays.copy(src, image.width(), left, top,
                            dest, result.width(), 0, 0, width - 1, height - 1);
        }
        else if(left >= -image.width() + 1 && right <= image.width() * 2 - 1 &&
                top >= -image.height() + 1 && bottom <= image.height() * 2 - 1) {
            copyReflected(src, image.width(), dest, result.width(),
                          left, right - 1, top, bottom - 1);
        }
        else {
            // FIXME: Implement or fall back to ReflectSamplingMethod
            throw new RuntimeException(
                    "Mirroring a region larger than source image is not " +
                    "implemented");
        }

        return result;
    }

    /**
     * left, right, top and bottom are inclusive.
     */
    private void copyReflected(float[] src, int sw, float[] dst, int dw,
                               int left, int right, int top, int bottom) {

        /*
         * There are 9 quadrants to consider when reflecting. The middle (MM)
         * is not reflected. The corners are reflected in the X and Y axis. The
         * vertical middle edges are reflected on Y, the horizontal middle edges
         * are reflected on the X.
         *
         *   LT | MT | RT
         *   ------------
         *   LM | MM | RM
         *   ------------
         *   LB | MB | RB
         */

        int sh = src.length / sw;

        for(int qx = -1; qx < 2; ++qx) {
            // Note that the data on the edge are not repeated. e.g:
            //   index:  -2, -1,  0,  1,  2 =>
            //   maps to: 2,  1,  0,  1,  2
            // and with width 5:
            //   index:   2,  3,  4,  5,  6
            //   maps to: 2,  3,  4,  3,  2

            // Find quadrant edge indexes (inclusive). Because we don't
            // reflect the outer data, the reflection area is
            // shorter/narrower than the src img by 1
            int ql = Math.max(-sw + 1,    sw * qx); // -4, 0, 5 for img width 5
            int qr = Math.min(sw * 2 - 1, (sw * (qx + 1)) - 1); // -1, 4, 4

            int l = Math.max(left, ql),
                r = Math.min(right, qr);

            if(r < l)
                continue;

            for(int qy = -1; qy < 2; ++qy) {

                int qt = Math.max(-sh + 1,    sh * qy);
                int qb = Math.min(sh * 2 - 1, (sh * (qy + 1)) - 1);

                int t = Math.max(top, qt),
                    b = Math.min(bottom, qb);

                // Skip this quadrant if it's empty
                if(b < t)
                    continue;

                // Determine src rect (top, left) position
                int sxa = Math.min(ReflectSamplingMethod.reflect(l, sw), ReflectSamplingMethod.reflect(r, sw)),
                    sya = Math.min(ReflectSamplingMethod.reflect(t, sh), ReflectSamplingMethod.reflect(b, sh));

                // Determine dest rect. left and right are flipped when not
                // copying the middle portion of an axis.
                int dxa, dxb;
                if(qx == 0) {
                    dxa = l - left;
                    dxb = r - left;
                }
                else {
                    dxa = r - left;
                    dxb = l - left;
                }

                int dya, dyb;
                if(qy == 0) {
                    dya = t - top;
                    dyb = b - top;
                }
                else {
                    dya = b - top;
                    dyb = t - top;
                }

                GridArrays.copy(src, sw, sxa, sya,
                                dst, dw, dxa, dya, dxb, dyb);
            }
        }
    }
}
