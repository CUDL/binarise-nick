package uk.ac.cam.lib.imageproc.arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public final class ArrayTypes {
    private ArrayTypes() {}

    private static final FloatPrimitiveArrayType FLOAT =
            new FloatPrimitiveArrayType();
    private static final BytePrimitiveArrayType BYTE =
            new BytePrimitiveArrayType();
    private static final IntegerPrimitiveArrayType INTEGER =
            new IntegerPrimitiveArrayType();

    /**
     * Get an ArrayType instance for the given array class.
     *
     * @param arrayType The type of the array. e.g. {@code float[].class} or
     *                  {@code URI[].class}
     */
    public static <T> ArrayType<T> arrayType(Class<T> arrayType) {
        if(!arrayType.isArray())
            throw new IllegalArgumentException(
                    "arrayType is not an array class");

        if(arrayType.getComponentType().isPrimitive()) {
            @SuppressWarnings("unchecked")
            ArrayType<T> result =
                    arrayType == float[].class ? (ArrayType<T>) FLOAT :
                    arrayType == byte[].class  ? (ArrayType<T>) BYTE :
                    arrayType == int[].class   ? (ArrayType<T>) INTEGER :
                    null;

            if(result == null) {
                throw new RuntimeException(
                        "No PrimitiveArrayType implemented for: " + arrayType);
            }
            return result;
        }

        return new ReflectiveArrayType<>(
                arrayType.getComponentType(), arrayType);
    }

    private static abstract class AbstractArrayType<ET, AT>
            implements ArrayType<AT> {

        protected final Class<ET> elementType;
        protected final Class<AT> arrayType;

        public AbstractArrayType(Class<ET> elementType, Class<AT> arrayType) {
            this.elementType = Objects.requireNonNull(elementType);
            this.arrayType = Objects.requireNonNull(arrayType);

            if(!arrayType.isArray())
                throw new IllegalArgumentException(
                        "arrayType was not an array type");

            if(arrayType.getComponentType() != elementType)
                throw new IllegalArgumentException(
                        "elementType is not the component type of arrayType");
        }

        private Accessor<?, AT> accessor = null;

        private Accessor<?, AT> accessor() {
            if(accessor == null)
                accessor = new DefaultAccessor<>();
            return accessor;
        }

        @Override
        public Class<ET> elementType() {
            return elementType;
        }

        @Override
        public Class<AT> arrayType() {
            return arrayType;
        }

        @Override
        public <T> Accessor<T, AT> accessor(Class<T> elementType) {
            validateType(this.elementType, elementType, "elementType");

            // This cast is safe as we already validated that T is ET
            @SuppressWarnings("unchecked")
            Accessor<T, AT> result = (Accessor<T, AT>)accessor();
            return result;
        }

        protected abstract ET get(AT array, int i);

        protected static boolean validateType(
                Class<?> expected, Class<?> actual, String name) {

            if(expected != actual)
                throw new IllegalArgumentException(String.format(
                        "Provided %s class does not match the " +
                                "array's class. expected: %s, actual: %s",
                        name, expected, actual));

            return true;
        }

        private class DefaultAccessor<ET> implements Accessor<ET, AT> {

            @Override
            public ET get(AT array, int i) {
                validateType(arrayType, array.getClass(), "arrayType");

                // This cast is safe as we already validated that ET is the
                // element type of AT before returning this accessor.
                @SuppressWarnings("unchecked")
                ET result = (ET)AbstractArrayType.this.get(array, i);

                return result;
            }

            @Override
            public Iterator<ET> iterate(AT array) {
                validateType(arrayType, array.getClass(), "arrayType");

                final int maxIndex = length(array) - 1;

                return new Iterator<ET>() {
                    private int i = -1;

                    @Override
                    public boolean hasNext() {
                        return i < maxIndex;
                    }

                    @Override
                    public ET next() {
                        int i = ++this.i;
                        if(i > maxIndex)
                            throw new NoSuchElementException();

                        return get(array, i);
                    }
                };
            }
        }

        @Override
        public String toString() {
            return String.format("ArrayTypes.arrayType(%s.class)",
                    arrayType().getSimpleName());
        }
    }

    private static class ReflectiveArrayType<ET, AT>
            extends AbstractArrayType<ET, AT> {

        public ReflectiveArrayType(Class<ET> elementType, Class<AT> arrayType) {
            super(elementType, arrayType);

            if(elementType.isPrimitive())
                throw new IllegalArgumentException(
                        "primitive types are not supported");
        }

        @Override
        protected ET get(AT array, int i) {
            validateType(this.arrayType, array.getClass(), "arrayType");

            // This is safe as we've checked the type and the constructor
            // ensures ET is the AT array's component type.
            @SuppressWarnings("unchecked")
            ET result = (ET)Array.get(array, i);
            return result;
        }

        @Override
        public int length(AT array) {
            validateType(this.arrayType, array.getClass(), "arrayType");

            return Array.getLength(array);
        }

        @Override
        public AT allocate(int length) {
            // This is safe as the constructor ensures that ET is AT's
            // component type.
            @SuppressWarnings("unchecked")
            AT result = (AT)Array.newInstance(elementType, length);

            return result;
        }

        @Override
        public boolean areEqual(AT array1, Object array2) {
            validateType(this.arrayType, array1.getClass(), "arrayType");

            return array2 instanceof Object[] &&
                    Arrays.equals((Object[])array1, (Object[])array2);
        }

        @Override
        public int hashCodeOf(AT array) {
            validateType(this.arrayType, array.getClass(), "arrayType");

            return Arrays.hashCode((Object[])array);
        }
    }

    protected static abstract class PrimitiveArrayType<ET, AT> extends AbstractArrayType<ET, AT> {

        public PrimitiveArrayType(Class<ET> elementType, Class<AT> arrayType) {
            super(elementType, arrayType);

            if(!elementType.isPrimitive())
                throw new IllegalArgumentException(
                        "elementType was not primitive");
        }
    }

    public static final class FloatPrimitiveArrayType
            extends PrimitiveArrayType<Float, float[]> {

        public FloatPrimitiveArrayType() {
            super(Float.TYPE, float[].class);
        }

        @Override
        protected Float get(float[] array, int i) {
            return array[i];
        }

        @Override
        public int length(float[] array) {
            return array.length;
        }

        @Override
        public float[] allocate(int length) {
            return new float[length];
        }

        @Override
        public boolean areEqual(float[] array1, Object array2) {
            return array2 instanceof float[] &&
                    Arrays.equals(array1, (float[])array2);
        }

        @Override
        public int hashCodeOf(float[] array) {
            return Arrays.hashCode(array);
        }
    }

    public static final class BytePrimitiveArrayType
            extends PrimitiveArrayType<Byte, byte[]> {

        public BytePrimitiveArrayType() {
            super(Byte.TYPE, byte[].class);
        }

        @Override
        public Byte get(byte[] array, int i) {
            return array[i];
        }

        @Override
        public int length(byte[] array) {
            return array.length;
        }

        @Override
        public byte[] allocate(int length) {
            return new byte[length];
        }

        @Override
        public boolean areEqual(byte[] array1, Object array2) {
            return array2 instanceof byte[] &&
                    Arrays.equals(array1, (byte[])array2);
        }

        @Override
        public int hashCodeOf(byte[] array) {
            return Arrays.hashCode(array);
        }
    }

    public static final class IntegerPrimitiveArrayType
            extends PrimitiveArrayType<Integer, int[]> {

        public IntegerPrimitiveArrayType() {
            super(Integer.TYPE, int[].class);
        }

        @Override
        public Integer get(int[] array, int i) {
            return array[i];
        }

        @Override
        public int length(int[] array) {
            return array.length;
        }

        @Override
        public int[] allocate(int length) {
            return new int[length];
        }

        @Override
        public boolean areEqual(int[] array1, Object array2) {
            return array2 instanceof int[] &&
                    Arrays.equals(array1, (int[])array2);
        }

        @Override
        public int hashCodeOf(int[] array) {
            return Arrays.hashCode(array);
        }
    }
}
