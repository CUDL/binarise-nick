package uk.ac.cam.lib.imageproc.channelmapping;

import java.util.List;

public interface ChannelMapper<S, D> extends ChannelCount {
    int maxCount();
    void mapChannels(List<S> src, List<D> dest, int count);
}
