package uk.ac.cam.lib.imageproc.channelmapping;

import java.util.List;

public abstract class BaseChannelMapper<S, D>
        implements ChannelMapper<S, D> {

    private final int inputChannelCount, outputChannelCount, maxCount;

    public BaseChannelMapper(int maxCount, int inputOutputCount) {
        this(maxCount, inputOutputCount, inputOutputCount);
    }

    public BaseChannelMapper(int maxCount, int inputChannelCount,
                             int outputChannelCount) {
        this.maxCount = maxCount;
        this.inputChannelCount = inputChannelCount;
        this.outputChannelCount = outputChannelCount;
    }

    @Override
    public int inputChannelCount() {
        return inputChannelCount;
    }

    @Override
    public int outputChannelCount() {
        return outputChannelCount;
    }

    @Override
    public int maxCount() {
        return maxCount;
    }

    @Override
    public final void mapChannels(List<S> src, List<D> dest, int count) {
        if(src.size() != inputChannelCount)
            throw new IllegalArgumentException(String.format(
                    "received %d src channels, should be: %d",
                    src.size(), inputChannelCount()));

        if(dest.size() != outputChannelCount)
            throw new IllegalArgumentException(String.format(
                    "received %d dest channels, should be: %d",
                    dest.size(), outputChannelCount()));

        if(count < 0)
            throw new IllegalArgumentException("count was negative: " + count);
        if(count > maxCount())
            throw new IllegalArgumentException(String.format(
                    "count was greater than max count. count: %d, maxCount: %d",
                    count, maxCount));

        mapChannelsInternal(src, dest, count);
    }

    protected abstract void mapChannelsInternal(
            List<S> src, List<D> dest, int count);
}
