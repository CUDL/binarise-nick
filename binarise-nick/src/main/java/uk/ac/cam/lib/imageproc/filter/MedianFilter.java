package uk.ac.cam.lib.imageproc.filter;

import uk.ac.cam.lib.imageproc.filter.Array2DFilter;

public interface MedianFilter<T> extends Array2DFilter<T, T> {

}
