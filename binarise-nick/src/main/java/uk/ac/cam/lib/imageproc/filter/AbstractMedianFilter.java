package uk.ac.cam.lib.imageproc.filter;


import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

import java.util.Objects;

public abstract class AbstractMedianFilter<T> implements MedianFilter<T> {

    protected final int windowWidth, windowHeight;
    protected final SamplingMethod<T> samplingMethod;
    protected final ArrayType<T> arrayType;

    public AbstractMedianFilter(
            int windowWidth, int windowHeight,
            SamplingMethod<T> samplingMethod, ArrayType<T> arrayType) {

        if(windowWidth < 1)
            throw new IllegalArgumentException(
                    "windowWidth was < 1: " + windowWidth);

        if(windowHeight < 1)
            throw new IllegalArgumentException(
                    "windowHeight was < 1: " + windowHeight);

        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.samplingMethod = Objects.requireNonNull(samplingMethod);
        this.arrayType = Objects.requireNonNull(arrayType);
    }

    @Override
    public ArrayType<T> destType() {
        return arrayType;
    }
}
