package uk.ac.cam.lib.imageproc.arrays;

public interface Array2D<T> {
    T data();
    int width();
    int height();
    ArrayType<T> type();

    default int size() {
        return width() * height();
    }

    default int indexOf(int x, int y) {
        if(x >= width() || x < 0)
            throw new IndexOutOfBoundsException("x");
        if(y >= height() || y < 0)
            throw new IndexOutOfBoundsException("y");
        return x + width() * y;
    }

    default <ET> ET get(int x, int y, Class<ET> elementType) {
        return type().accessor(elementType).get(data(), indexOf(x, y));
    }

    static <T> Array2D<T> withData(Class<T> type, T data, int width) {
        return withData(ArrayTypes.arrayType(type), data, width);
    }

    static <T> Array2D<T> withData(ArrayType<T> type, T data, int width) {
        return new DefaultArray2D<>(type, data, width);
    }

    static <T> Array2D<T> allocate(Class<T> type, int width, int height) {
        return allocate(ArrayTypes.arrayType(type), width, height);
    }

    static <T> Array2D<T> allocate(ArrayType<T> type, int width, int height) {
        return new DefaultArray2D<>(type, type.allocate(width * height), width);
    }

    static <T> Array2D<T> allocateSameShape(Array2D<T> other) {
        return allocateSameShape(other.type(), other);
    }

    static <T> Array2D<T> allocateSameShape(Class<T> type, Array2D<?> other) {
        return allocate(type, other.width(), other.height());
    }

    static <T> Array2D<T> allocateSameShape(ArrayType<T> type,
                                            Array2D<?> other) {
        return allocate(type, other.width(), other.height());
    }
}
