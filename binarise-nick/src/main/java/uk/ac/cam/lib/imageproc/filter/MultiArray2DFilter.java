package uk.ac.cam.lib.imageproc.filter;

import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ArrayType;
import uk.ac.cam.lib.imageproc.filter.SingleOutputMultiArray2DFilter.MultiToSingleOutputMultiArray2DFilterAdapter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * An array filtering operation which acts on one or more input and output
 * arrays.
 *
 * <p>This is basically a workaround for not having support for arrays with
 * &gt; 2 dimentions.
 *
 * @param <S> The array type to map from
 * @param <D> The array type ot map to
 */
public interface MultiArray2DFilter<S, D> {
    ArrayType<D> destType();

    int getInputCount();
    int getOutputCount();

    List<Array2D<D>> apply(List<Array2D<S>> src, List<Array2D<D>> dest);

    default List<Array2D<D>> apply(List<Array2D<S>> src) {
        List<Array2D<D>> dest = IntStream.range(0, getOutputCount())
                .mapToObj(x -> Array2D.allocateSameShape(destType(), src.get(0)))
                .collect(Collectors.toList());

        return apply(src, dest);
    }

    default <T> MultiArray2DFilter<S, T> andThen(MultiArray2DFilter<D, T> next) {
        if(this.getOutputCount() != next.getInputCount())
            throw new IllegalArgumentException(String.format(
                    "next filter's input count does not match this filter's " +
                    "output count. output: %d, input: %d",
                    this.getOutputCount(), next.getInputCount()));

        return new BridgingMultiArray2DFilter<>(this, next);
    }

    default SingleOutputMultiArray2DFilter<S, D> withSingleOutput() {
        if(this instanceof SingleOutputMultiArray2DFilter)
            return (SingleOutputMultiArray2DFilter<S, D>)this;

        if(this.getOutputCount() != 1)
            throw new IllegalStateException(
                    "This MultiArray2DFilter has more than one output");

        return new MultiToSingleOutputMultiArray2DFilterAdapter<>(this);
    }

    static <T> void validateArray2DList(List<Array2D<T>> list, int expectedSize,
                                    String name) {
        if(list.size() != expectedSize)
            throw new IllegalArgumentException(String.format(
                    "%s list was wrong size. expected: %d, got: %d",
                    name, expectedSize, list.size()));
    }

    static <S, D> void validateApplyArrays(
            MultiArray2DFilter<S, D> filter, List<Array2D<S>> src,
            List<Array2D<D>> dest) {

        validateArray2DList(src, filter.getInputCount(), "input");
        validateArray2DList(dest, filter.getOutputCount(), "output");
    }

    static <S, D> MultiArray2DFilter<S, D> requireIOCounts(
            MultiArray2DFilter<S, D> filter, int input, int output,
            String name) {

        if(filter.getInputCount() != input || filter.getOutputCount() != output)
            throw new IllegalArgumentException(String.format(
                    "Expected %s filter to support %d input and %d output " +
                    "arrays, but got a filter with %d and %d", name, input,
                    output, filter.getInputCount(), filter.getOutputCount()));
        return filter;
    }

    class BridgingMultiArray2DFilter<S, I, D>
            implements MultiArray2DFilter<S, D> {

        private final MultiArray2DFilter<S, I> first;
        private final MultiArray2DFilter<I, D> second;

        public BridgingMultiArray2DFilter(MultiArray2DFilter<S, I> first,
                                          MultiArray2DFilter<I, D> second) {
            this.first = Objects.requireNonNull(first);
            this.second = Objects.requireNonNull(second);
        }

        @Override
        public ArrayType<D> destType() {
            return second.destType();
        }

        @Override
        public int getInputCount() {
            return first.getInputCount();
        }

        @Override
        public int getOutputCount() {
            return second.getOutputCount();
        }

        @Override
        public List<Array2D<D>> apply(List<Array2D<S>> src, List<Array2D<D>> dest) {
            MultiArray2DFilter.validateApplyArrays(this, src, dest);

            return second.apply(first.apply(src), dest);
        }
    }

    @SafeVarargs
    static <S, D> MultiArray2DFilter<S, D> fromFilters(
            Array2DFilter<S, D>...filters) {

        return new FilterToMultiFilterAdapter<>(Arrays.asList(filters));
    }

    class FilterToMultiFilterAdapter<S, D> implements MultiArray2DFilter<S, D> {

        private final List<Array2DFilter<S, D>> filters;

        public FilterToMultiFilterAdapter(List<Array2DFilter<S, D>> filters) {
            this.filters = filters.stream()
                    .peek(Objects::requireNonNull)
                    .collect(Collectors.toList());

            if(filters.isEmpty())
                throw new IllegalArgumentException("No filters provided");
        }

        @Override
        public ArrayType<D> destType() {
            return filters.get(0).destType();
        }

        @Override
        public int getInputCount() {
            return filters.size();
        }

        @Override
        public int getOutputCount() {
            return filters.size();
        }

        @Override
        public List<Array2D<D>> apply(List<Array2D<S>> src, List<Array2D<D>> dest) {
            MultiArray2DFilter.validateApplyArrays(this, src, dest);

            IntStream.range(0, src.size())
                    .parallel()
                    .forEach(i ->
                            filters.get(i).apply(src.get(i), dest.get(i)));
            return dest;
        }
    }
}
