package uk.ac.cam.lib.imageproc.objectlabel;


import uk.ac.cam.lib.imageproc.ImageValues;
import uk.ac.cam.lib.imageproc.objectlabel.ImageObjects.ImageObject;

import java.util.Objects;
import java.util.function.Predicate;

public class DefaultImageObjectFilter extends ImageObjectFilter {
    private final Predicate<ImageObject> isIncluded;

    public DefaultImageObjectFilter(
            RegionLabelFilter<byte[]> regionLabelFilter,
            Predicate<ImageObject> isIncluded) {
        this(regionLabelFilter, isIncluded, ImageValues.BLACK_1BIT);
    }

    public DefaultImageObjectFilter(
            RegionLabelFilter<byte[]> regionLabelFilter,
            Predicate<ImageObject> isIncluded, byte colour) {
        super(regionLabelFilter, colour);

        this.isIncluded = Objects.requireNonNull(isIncluded);
    }

    @Override
    public Predicate<ImageObject> isIncluded() {
        return isIncluded;
    }
}
