package uk.ac.cam.lib.imageproc.channelmapping;

import java.util.List;
import java.util.Objects;

public class BridgingChannelMapper<A, B, C>
        implements ChannelMapper<A, C> {

    private final ChannelMapper<A, B> cm1;
    private final ChannelMapper<B, C> cm2;
    private final Class<B> classB;
    private final int maxCount;
    private List<B> cachedIntermediateBuffer;

    public BridgingChannelMapper(ChannelMapper<A, B> cm1,
                                 ChannelMapper<B, C> cm2,
                                 Class<B> classB, int maxCount) {
        this.cm1 = Objects.requireNonNull(cm1);
        this.cm2 = Objects.requireNonNull(cm2);
        this.classB = Objects.requireNonNull(classB);
        this.maxCount = maxCount;

        if(maxCount < 1)
            throw new IllegalArgumentException("maxCount was < 1: " +
                                               maxCount);

        if(!(cm1.outputChannelCount() == cm2.inputChannelCount()))
            throw new IllegalArgumentException("The first channel mapper " +
                    "has a different output count to the second mapper's " +
                    "input count.");
    }

    public static <A, B, C> ChannelMapperFactory<A, C> bridgeChannelMappers(
            ChannelMapperFactory<A, B> cmf1, ChannelMapperFactory<B, C> cmf2,
            Class<B> intermediateClass) {

        return new DefaultChannelMapperFactory<>(cmf1.inputChannelCount(),
                cmf2.outputChannelCount(), maxCount -> {
            ChannelMapper<A, B> cm1 = cmf1.getMapper(maxCount);
            ChannelMapper<B, C> cm2 = cmf2.getMapper(maxCount);

            return new BridgingChannelMapper<>(
                    cm1, cm2, intermediateClass, maxCount);
        });
    }

    @Override
    public int inputChannelCount() {
        return cm1.inputChannelCount();
    }

    @Override
    public int outputChannelCount() {
        return cm2.outputChannelCount();
    }

    @Override
    public int maxCount() {
        return this.maxCount;
    }

    private List<B> getIntermediateBuffer() {
        if(cachedIntermediateBuffer == null) {
            cachedIntermediateBuffer = ChannelMapping.createTransferBuffers(
                    classB, cm1.outputChannelCount(), maxCount());
        }

        return cachedIntermediateBuffer;
    }

    @Override
    public void mapChannels(List<A> src, List<C> dest, int count) {
        validateChannelCount(inputChannelCount(), src.size(), "Input");
        validateChannelCount(outputChannelCount(), dest.size(), "Output");

        List<B> intermediate = getIntermediateBuffer();

        cm1.mapChannels(src, intermediate, count);
        cm2.mapChannels(intermediate, dest, count);
    }

    private static void validateChannelCount(
            int expected, int actual, String channelName) {

        if(expected != actual) {
            throw new IllegalArgumentException(String.format(
                    "%s channel count does not match expected. " +
                    "expected: %d, actual: %d",
                    channelName, expected, actual));
        }
    }
}
