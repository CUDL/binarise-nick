package uk.ac.cam.lib.imageproc.arrays;

import java.util.Objects;

import static uk.ac.cam.lib.imageproc.Maths.floorMod;

/**
 * A {@link SamplingMethod} which reflects edge samples.
 */
public class ReflectSamplingMethod implements SamplingMethod<float[]> {

    @Override
    public ArrayType<float[]> type() {
        return ArrayTypes.arrayType(float[].class);
    }

    @Override
    public Array2D<float[]> sample(
            Array2D<float[]> image, int x, int y,
            int width, int height, Array2D<float[]> result) {

        Objects.requireNonNull(image);
        Objects.requireNonNull(result);

        int left = x - (width / 2);
        int right = x + (width - (width / 2));
        int top = y - (height / 2);
        int bottom = y + (height - (height / 2));

        assert right - left == width;
        assert bottom - top == height;

        float[] src = image.data();
        float[] dest = result.data();
        int imgWidth = image.width();
        int imgHeight = image.height();

        for(int imgY = top, i = 0; imgY < bottom; ++imgY) {
            for(int imgX = left; imgX < right; ++imgX) {
                dest[i++] = src[
                        reflectedIndex(imgX, imgY, imgWidth, imgHeight)];
            }
        }

        return result;
    }

    private static int reflectedIndex(int x, int y, int width, int height) {
        return reflect(y, height) * width + reflect(x, width);
    }

    static int reflect(int index, int size) {
        if(index < 0)
            return Math.abs(index) % size;
        else if(index >= size)
            return floorMod((size - 2) - (index % size), size);
        else
            return index;
    }
}
