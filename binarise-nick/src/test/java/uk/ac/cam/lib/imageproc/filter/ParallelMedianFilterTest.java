package uk.ac.cam.lib.imageproc.filter;


import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

@RunWith(DataProviderRunner.class)
public class ParallelMedianFilterTest {

    @DataProvider
    public static List<List<Object>> testParams() {
        List<List<Object>> params = new ArrayList<>();

        for(int i = 0; i < 20; ++i) {
            Random r = new Random(i);
            TestParam tp = TestParam.randomInstance(r);

            Array2D<float[]> src = randomGreyscaleImage(r);

            params.add(Arrays.asList(tp, src));
        }

        return params;
    }

    public static final class TestParam {

        private final int winWidth, winHeight, maxSynchronous;
        private final SamplingMethod<float[]> samplingMethod;

        public TestParam(
                int winWidth, int winHeight,
                SamplingMethod<float[]> samplingMethod, int maxSynchronous) {

            this.maxSynchronous = maxSynchronous;
            this.winWidth = winWidth;
            this.winHeight = winHeight;

            this.samplingMethod = samplingMethod;
        }

        public DefaultMedianFilter getReference() {
            return new DefaultMedianFilter(winWidth, winHeight, samplingMethod);
        }

        public ParallelMedianFilter getParallel() {
            return new ParallelMedianFilter(
                    winWidth, winHeight, samplingMethod,
                    ForkJoinPool.commonPool(), maxSynchronous);
        }

        public static TestParam randomInstance(Random r) {
            return new TestParam(
                    1 + r.nextInt(10),
                    1 + r.nextInt(10),
                    new ReflectSamplingMethod(),
                    (int)Math.pow(2, r.nextInt(20)) + r.nextInt(50));
        }
    }

    public static Array2D<float[]> randomGreyscaleImage(Random r) {
        int width = (int)Math.pow(2, r.nextInt(8));
        int height = (int)Math.pow(2, r.nextInt(8));
        return randomGreyscaleImage(r, width, height);
    }

    public static Array2D<float[]> randomGreyscaleImage(
            Random r, int width, int height) {

        float[] pixels = new float[width * height];
        for(int i = 0; i < pixels.length; ++i)
            pixels[i] = r.nextFloat();

        return Array2D.withData(float[].class, pixels, width);
    }

    @Test
    @UseDataProvider("testParams")
    public void checkParallelBehaviourMatchesReference(
            TestParam param, Array2D<float[]> src) {

        DefaultMedianFilter ref = param.getReference();
        ParallelMedianFilter parallel = param.getParallel();

        Array2D<float[]> refResult = ref.apply(src);
        Array2D<float[]> parallelResult = parallel.apply(src);

        Assert.assertThat(refResult.width(), CoreMatchers.equalTo(parallelResult.width()));
        Assert.assertThat(refResult.height(), CoreMatchers.equalTo(parallelResult.height()));
        Assert.assertThat(refResult.data(), CoreMatchers.equalTo(parallelResult.data()));
    }
}
