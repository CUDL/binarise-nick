package uk.ac.cam.lib.imageproc.objectlabel

import spock.lang.Specification
import uk.ac.cam.lib.imageproc.arrays.Array2D

class DefaultImageObjectTest extends Specification {

    def "label image is not copied"() {
        given:
        def labelImage = Array2D.allocate(int[], 4, 4)
        def bounds = Array2D.allocate(int[], 4, 1)

        expect:
        new DefaultImageObjects(labelImage, bounds).labelImage.is(labelImage)
    }

    def "object bounds array is not copied"() {
        given:
        def labelImage = Array2D.allocate(int[], 4, 4)
        def bounds = Array2D.allocate(int[], 4, 1)

        expect:
        new DefaultImageObjects(labelImage, bounds).objectBounds.is(bounds)
    }

    def "getMaxLabel"() {
        given:
        def labelImage = Array2D.withData(int[], [
                0, 0, 0, 0,
                0, 0, 0, 0,
                1, 2, 3, 4,
                5, 6, 7, 0
        ] as int[], 4)


        def bounds = Array2D.allocate(int[], 4, 8)
        def imageObjects = new DefaultImageObjects(labelImage, bounds)

        expect:
        imageObjects.maxLabel == 7
    }

    def "getObject rejects labels out of range"() {
        given:
        def labelImage = Array2D.withData(int[], [
                0, 0, 0, 0,
                0, 0, 0, 0,
                1, 2, 3, 4,
                5, 6, 7, 0
        ] as int[], 4)

        def bounds = Array2D.allocate(int[], 4, 8)
        def imageObjects = new DefaultImageObjects(labelImage, bounds)

        when:
        imageObjects.getObject(label)

        then:
        thrown(IllegalArgumentException)

        where:
        label << [-10, -1, 8, 100]
    }

    def "getObject returns ImageObject with same label"() {
        given:
        def labelImage = Array2D.withData(int[], [
                0, 0, 0, 0,
                0, 0, 0, 0,
                1, 2, 3, 4,
                5, 6, 7, 0
        ] as int[], 4)

        def bounds = Array2D.allocate(int[], 4, 8)
        def imageObjects = new DefaultImageObjects(labelImage, bounds)

        when:
        def obj = imageObjects.getObject(label)

        then:
        obj.label == label

        where:
        label << (0..7)
    }

    def getSampleLabeledImage() {

        def labelImage = Array2D.withData(int[], [
                1, 1, 1, 1, 2, 2,
                1, 0, 0, 0, 1, 2,
                1, 0, 0, 0, 0, 1,
                1, 0, 3, 3, 0, 0,
                1, 0, 0, 0, 3, 0,
        ] as int[], 6)

        return new Tuple(labelImage, ObjectBoxer.getObjectBounds(labelImage))
    }

    def getSampleImageObjects() {
        def (Array2D<int[]> labelImage, Array2D<int[]> bounds) =
        sampleLabeledImage
        return new DefaultImageObjects(labelImage, bounds)
    }

    def "image object bounds match input image"() {
        given:
        def imageObjects = sampleImageObjects

        when:
        def obj = imageObjects.getObject(label)

        then:
        obj.XLeft == xLeft
        obj.XRight == xRight
        obj.YTop == yTop
        obj.YBottom == yBottom

        where:
        label <<   [0, 1, 2, 3]
        xLeft <<   [1, 0, 4, 2]
        xRight <<  [5, 5, 5, 4]
        yTop <<    [1, 0, 0, 3]
        yBottom << [4, 4, 1, 4]
    }

    def "image object area match input image"() {
        given:
        def imageObjects = sampleImageObjects

        when:
        def obj = imageObjects.getObject(label)

        then:
        obj.area == area

        where:
        label << [ 0,  1,  2,  3]
        area <<  [14, 10,  3,  3]
    }

    def "image object width & height match input image"() {
        given:
        def imageObjects = sampleImageObjects

        when:
        def obj = imageObjects.getObject(label)

        then:
        obj.width == width
        obj.height == height

        where:
        label <<  [0, 1, 2, 3]
        width <<  [5, 6, 2, 3]
        height << [4, 5, 2, 2]
    }

    def "streamObjects() contains objects"() {
        given:
        def imageObjects = sampleImageObjects

        when:
        def objects = imageObjects.streamObjects(includeBackground)

        then:
        def label = includeBackground ? 0 : 1
        objects.forEachOrdered { obj ->
            assert obj.label == label
            ++label
        }

        where:
        includeBackground << [true, false]
    }

    def "image object renders itself in original position"() {
        given:
        def imageObjects = sampleImageObjects

        when:
        def obj = imageObjects.getObject(label)
        def dest = Array2D.allocateSameShape(byte[], imageObjects.labelImage)
        obj.renderTo(dest, renderValue, obj.XLeft, obj.YTop)

        byte[] expectedData = Arrays.stream(imageObjects.getLabelImage().data())
            .map { i -> i == label ? (int)renderValue : 0 }
            .toArray() as byte[]
        def expected = Array2D.withData(byte[], expectedData, dest.width())

        then:
        dest == expected

        where:
        label <<       [ 0,  1, 2, 3]
        renderValue << ([-1, -4, 9, 5] as byte[])
    }
}
