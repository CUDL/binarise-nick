package uk.ac.cam.lib.imageproc

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static spock.util.matcher.HamcrestMatchers.closeTo
import static spock.util.matcher.HamcrestSupport.that

class MathsSpec extends Specification {

    static combineParams(List...params) {
        return params.collect { x -> x.transpose() }
                .combinations().collect { x -> x.flatten() }
    }

    static class FloatModSpec extends Specification {

        @Shared funcNames = [
                "Maths.floorMod(float, float)"
        ]

        @Shared modFuncs = [
                Maths.&floorMod
        ]

        def getFractionalDivisorData() {
            def dividends = [
                    -5.0,  -4.75, -4.5,  -4.25, -4.0,  -3.75, -3.5,  -3.25, -3.0,
                    -2.75, -2.5,  -2.25, -2.0,  -1.75, -1.5,  -1.25, -1.0,  -0.75,
                    -0.5,  -0.25,  0.0,   0.25,  0.5,   0.75,  1.0,   1.25,  1.5,
                    1.75,  2.0,   2.25,  2.5,   2.75,  3.0,   3.25,  3.5,   3.75,
                    4.0,   4.25,  4.5,   4.75,  5.0
            ]

            def expectations = [
                    1.0,   0.05,   0.3,   0.55,  0.8,   1.05,  0.1,   0.35,  0.6,
                    0.85,  1.1,    0.15,  0.4,   0.65,  0.9,   1.15,  0.2,   0.45,
                    0.7,   0.95,   0.0,   0.25,  0.5,   0.75,  1.0,   0.05,  0.3,
                    0.55,  0.8,    1.05,  0.1,   0.35,  0.6,   0.85,  1.1,   0.15,
                    0.4,   0.65,   0.9,   1.15,  0.2
            ]

            combineParams([funcNames, modFuncs], [dividends, expectations])
        }

        //@Unroll("#funcName should handle fractional divisors")
        def "mod(float, float) should handle fractional divisors"() {
            expect:
            that(modFunc(dividend, 1.2), closeTo(expected, 0.001))

            where:
            [funcName, modFunc, dividend, expected] << fractionalDivisorData
        }

        def getNegativeFractionalDivisorData() {
            def dividends = [
                    -5.0,  -4.75, -4.5,  -4.25, -4.0,  -3.75, -3.5,  -3.25, -3.0,
                    -2.75, -2.5,  -2.25, -2.0,  -1.75, -1.5,  -1.25, -1.0,  -0.75,
                    -0.5,  -0.25,  0.0,   0.25,  0.5,   0.75,  1.0,   1.25,  1.5,
                    1.75,  2.0,   2.25,  2.5,   2.75,  3.0,   3.25,  3.5,   3.75,
                    4.0,   4.25,  4.5,   4.75,  5.0
            ]

            def expectations = [
                    -0.2,  -1.15, -0.9,  -0.65, -0.4,  -0.15, -1.1,  -0.85, -0.6,
                    -0.35, -0.1,  -1.05, -0.8,  -0.55, -0.3,  -0.05, -1.0,  -0.75,
                    -0.5,  -0.25, -0.0,  -0.95, -0.7,  -0.45, -0.2,  -1.15, -0.9,
                    -0.65, -0.4,  -0.15, -1.1,  -0.85, -0.6,  -0.35, -0.1,  -1.05,
                    -0.8,  -0.55, -0.3,  -0.05, -1.0
            ]

            combineParams([funcNames, modFuncs], [dividends, expectations])
        }

        //@Unroll("#funcName should handle negative fractional divisors")
        def "mod(float, float) should handle negative fractional divisors"() {
            expect:
            that(modFunc(dividend, -1.2), closeTo(expected, 0.001))

            where:
            [funcName, modFunc, dividend, expected] << negativeFractionalDivisorData
        }

        def "mod(float, float) result follows positive divisor sign"() {
            expect:
            modFunc(dividend, 3f) == expected

            where:
            [funcName, modFunc, dividend, expected] << combineParams(
                    [funcNames, modFuncs],
                    [(-4..4), [2, 0, 1, 2, 0, 1, 2, 0, 1]])
        }

        @Unroll("#funcName should follow negative divisor sign")
        def "mod(float, float) result should follow negative divisor sign"(
                funcName, modFunc, float dividend, float expected) {

            expect:
            modFunc(dividend, -3f) == expected

            where:
            [funcName, modFunc, dividend, expected] << combineParams(
                    [funcNames, modFuncs],
                    [
                            (-4..4),
                            ([-1, 0, -2, -1, 0, -2, -1, 0, -2] as float[]).collect {
                                x -> x == 0 ? Math.copySign(0, -1) : x
                            }
                    ])
        }
    }

    static class IntModSpec extends Specification {

        @Shared
        funcNames = [
                "Maths.floorMod(int, int)",
                "Math.floorMod(int, int)"
        ]

        @Shared
        modFuncs = [
                Maths.&floorMod,
                Math.&floorMod
        ]

        @Unroll("#funcName result follows positive divisor sign")
        def "mod(int, int) result follows positive divisor sign"() {

            expect:
            modFunc(dividend, 3) == expected

            where:
            [funcName, modFunc, dividend, expected] << combineParams(
                    [funcNames, modFuncs],
                    [(-4..4), [2, 0, 1, 2, 0, 1, 2, 0, 1]])
        }

        @Unroll("#funcName result follows positive divisor sign")
        def "mod(int, int) result follows negative divisor sign"() {

            expect:
            modFunc(dividend, -3) == expected

            where:
            [funcName, modFunc, dividend, expected] << combineParams(
                    [funcNames, modFuncs],
                    [(-4..4), [-1, 0, -2, -1, 0, -2, -1, 0, -2]])
        }
    }
}
