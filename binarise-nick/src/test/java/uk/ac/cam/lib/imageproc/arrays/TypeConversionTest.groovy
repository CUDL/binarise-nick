package uk.ac.cam.lib.imageproc.arrays

import spock.lang.Specification


class TypeConversionTest extends Specification {

    def "toBytes buckets values evenly"() {
        given:
        def perBucket = 500
        def max = 256 * perBucket - 1
        def inputs = (0..max).collect { n -> n / max } as float[]
        def output = new byte[max + 1]
        def counts = new int[256]

        when:
        TypeConversion.toBytes(inputs, 0, output, 0, max + 1)

        for(n in output)
            counts[n & 0xff]++

        then:
        assert inputs.length == 256 * perBucket
        counts.eachWithIndex{ int count, int i ->
            assert count == perBucket
        }
    }

    def "toBytes round-trips values"() {
        given:
        def src = [floatVal] as float[]
        def dest = new byte[1]

        when:
        TypeConversion.toBytes(src, 0, dest, 0, 1);

        then:
        dest[0] == (intVal as byte)
        (dest[0] & 0xff) == intVal

        where:
        floatVal << (0..255).collect { n -> n / 255f }
        intVal << (0..255)
    }

}
