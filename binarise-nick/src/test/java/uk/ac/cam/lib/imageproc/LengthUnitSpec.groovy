package uk.ac.cam.lib.imageproc

import spock.lang.Specification

import static uk.ac.cam.lib.imageproc.LengthUnit.CENTIMETERS
import static uk.ac.cam.lib.imageproc.LengthUnit.INCHES
import static uk.ac.cam.lib.imageproc.LengthUnit.METERS
import static uk.ac.cam.lib.imageproc.LengthUnit.MILLIMETERS


class LengthUnitSpec extends Specification {
    def "units should be converted correctly"(
            LengthUnit src, float quantity, LengthUnit dest, float expected) {

        expect:
        src.getConverterTo(dest).applyAsFloat(quantity) == expected
        src.to(dest, quantity) == expected

        where:
        src         | quantity | dest        | expected
        METERS      | 1        | METERS      | 1
        METERS      | 1        | CENTIMETERS | 100
        METERS      | 1        | MILLIMETERS | 1000
        METERS      | 1        | INCHES      | 100/2.54
        METERS      | 2.54     | INCHES      | 100
        CENTIMETERS | 1        | METERS      | 1/100
        CENTIMETERS | 100      | METERS      | 1
        CENTIMETERS | 1        | CENTIMETERS | 1
        CENTIMETERS | 1        | MILLIMETERS | 10
        CENTIMETERS | 1        | INCHES      | 1/2.54
        CENTIMETERS | 2.54     | INCHES      | 1
        MILLIMETERS | 1        | METERS      | 1/1000
        MILLIMETERS | 1000     | METERS      | 1
        MILLIMETERS | 1        | CENTIMETERS | 1/10
        MILLIMETERS | 10       | CENTIMETERS | 1
        MILLIMETERS | 1        | MILLIMETERS | 1
        MILLIMETERS | 1        | INCHES      | 1/25.4
        MILLIMETERS | 10       | INCHES      | 1/2.54
        MILLIMETERS | 100      | INCHES      | 1/0.254
        MILLIMETERS | 254      | INCHES      | 10
        INCHES      | 1        | METERS      | 2.54/100
        INCHES      | 1        | METERS      | 254/10000
        INCHES      | 100      | METERS      | 2.54
        INCHES      | 10000    | METERS      | 254
        INCHES      | 1        | CENTIMETERS | 2.54
        INCHES      | 100      | CENTIMETERS | 254
        INCHES      | 10       | MILLIMETERS | 254
        INCHES      | 1        | INCHES      | 1
    }
}
