package uk.ac.cam.lib.imageproc.arrays;


import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ArrayCopyTest {


    @Test
    public void testArrayCopy1() {
        Array2D<float[]> src = Array2D.withData(float[].class, new float[]{
                0,  1,  2,
                3,  4,  5,
                6,  7,  8,
                9, 10, 11
        }, 3);

        Array2D<float[]> dst = Array2D.allocate(float[].class, 3, 4);

        assertThat(dst, is(not(equalTo(src))));

        ArrayCopy.copy(src, 0, 0, dst, 0, 0, src.width(), src.height());

        assertThat(dst, is(equalTo(src)));
    }

    @Test
    public void testArrayCopy2() {
        Array2D<float[]> src = Array2D.withData(float[].class, new float[]{
                0,  1,  2,
                3,  4,  5,
                6,  7,  8,
                9, 10, 11
        }, 3);

        Array2D<float[]> dst = Array2D.allocate(float[].class, 4, 5);

        ArrayCopy.copy(src, 1, 2, dst, 2, 1, 1, 2);

        Array2D<float[]> expected = Array2D.withData(float[].class, new float[]{
                0,  0,  0, 0,
                0,  0,  7, 0,
                0,  0, 10, 0,
                0,  0,  0, 0,
                0,  0,  0, 0
        }, 4);

        assertThat(dst, is(equalTo(expected)));
    }
}
