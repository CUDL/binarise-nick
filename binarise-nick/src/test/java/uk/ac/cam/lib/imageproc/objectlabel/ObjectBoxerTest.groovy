package uk.ac.cam.lib.imageproc.objectlabel

import spock.lang.Specification
import uk.ac.cam.lib.imageproc.arrays.Array2D


class ObjectBoxerTest extends Specification {

    def "getObjectBounds handles empty image"() {
        given:
        def emptyImage = Array2D.withData(int[], new int[4 * 4], 4)

        when:
        def bounds = ObjectBoxer.getObjectBounds(emptyImage)

        then:
        def cls = int[]
        bounds == Array2D.withData(cls, [0, 0, 3, 3] as int[], 4)
    }

    def "getObjectBounds detects single object"() {
        given:
        def img = Array2D.withData(int[], [
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 1, 0, 0,
                0, 0, 1, 1, 0, 0,
                0, 0, 0, 1, 1, 0,
                0, 0, 0, 0, 1, 0,
                0, 0, 0, 0, 0, 0,
        ] as int[], 6)

        when:
        def bounds = ObjectBoxer.getObjectBounds(img)

        then:
        def cls = int[]
        bounds == Array2D.withData(cls, [
                0, 0, 5, 5,
                2, 1, 4, 4
        ] as int[], 4)
    }

    def "getObjectBounds detects multiple object"() {
        given:
        def img = Array2D.withData(int[], [
                0, 0, 0, 0, 4, 4,
                0, 2, 2, 1, 3, 4,
                0, 0, 1, 1, 0, 4,
                0, 0, 0, 1, 1, 4,
                0, 0, 0, 0, 1, 4,
                0, 0, 0, 0, 0, 4,
        ] as int[], 6)

        when:
        def bounds = ObjectBoxer.getObjectBounds(img)

        then:
        def cls = int[]
        bounds == Array2D.withData(cls, [
                0, 0, 4, 5, // there are no zeros in the last column here
                2, 1, 4, 4,
                1, 1, 2, 1,
                4, 1, 4, 1,
                4, 0, 5, 5
        ] as int[], 4)
    }
}
