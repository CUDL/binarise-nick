package uk.ac.cam.lib.imageproc.filter.binarise;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod;
import uk.ac.cam.lib.imageproc.filter.ParallelMedianFilterTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(DataProviderRunner.class)
public class ParallelNickBinarisationMethodTest {

    @DataProvider
    public static List<List<Object>> randomParams() {
        List<List<Object>> params = new ArrayList<>();

        for(int i = 0; i < 20; ++i) {
            Random r = new Random(i);

            Array2D<float[]> src = ParallelMedianFilterTest.randomGreyscaleImage(r);
            params.add(Arrays.asList(
                    src,
                    10 + r.nextInt(40),
                    10 + r.nextInt(40),
                    new ReflectSamplingMethod(),
                    randomKValue(r),
                    (int)Math.pow(2, r.nextInt(20)) + r.nextInt(50)));
        }

        return params;
    }

    private static float randomKValue(Random r) {
        float width = (AbstractNickBinarisationMethod.K_MAX - AbstractNickBinarisationMethod.K_MIN);

        // Cover min-max plus some either side
        return (AbstractNickBinarisationMethod.K_MIN - width / 2) + (r.nextFloat() * (width * 2));
    }

    @Test
    @UseDataProvider("randomParams")
    public void checkParallelBehaviourMatchesReference(
            Array2D<float[]> src, int windowWidth, int windowHeight,
            SamplingMethod<float[]> samplingMethod, float k,
            int maxSynchronous) {

        NickBinarisationMethod<float[]> ref = new DefaultNickBinarisationMethod(
                windowWidth, windowHeight, samplingMethod, k);
        NickBinarisationMethod<float[]> parallel = new ParallelNickBinarisationMethod(
                windowWidth, windowHeight, samplingMethod, k,
                ForkJoinPool.commonPool(), Integer.MAX_VALUE);

        Array2D<byte[]> refResult = ref.apply(src);
        Array2D<byte[]> parallelResult = parallel.apply(src);

        //assertThat(parallelResult, is(equalTo(refResult)));

        assertThat(parallelResult.width(), is(equalTo(refResult.width())));
        assertThat(parallelResult.height(), is(equalTo(refResult.height())));
        assertThat(parallelResult.data(), is(equalTo(refResult.data())));
    }
}
