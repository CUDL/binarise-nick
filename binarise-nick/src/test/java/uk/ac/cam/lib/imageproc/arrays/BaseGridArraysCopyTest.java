package uk.ac.cam.lib.imageproc.arrays;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

@Ignore
public abstract class BaseGridArraysCopyTest {

    interface CopyMethod {
        void copy(float[] src, int sw, int sxa, int sya,
                  float[] dst, int dw, int dxa, int dya, int dxb, int dyb);
    }

    protected CopyMethod copier;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    protected static float[] arrayRange(int count) {
        float[] a = new float[count];

        for(int i = 0; i < a.length; ++i)
            a[i] = i;

        return a;
    }

    @Test
    public void testCopy_1() {
        float[] src = arrayRange(4*4);

        float[] dest = new float[4*4];

        copier.copy(src, 4, 0, 0,
                    dest, 4, 0, 0, 3, 3);

        assertTrue(java.util.Arrays.equals(src, dest));
    }

    @Test
    public void testCopy_flipY() {
        float[] src = arrayRange(4*4);

        float[] dest = new float[4*4];

        copier.copy(src, 4, 0, 0,
                    dest, 4, 0, 3, 3, 0);

        float[] expected = new float[]{
                12, 13, 14, 15,
                 8,  9, 10, 11,
                 4,  5,  6,  7,
                 0,  1,  2,  3
        };

        assertArrayEquals(expected, dest, 0);
    }

    @Test
    public void testCopy_flipX() {
        float[] src = arrayRange(4*4);

        float[] dest = new float[4*4];

        copier.copy(src, 4, 0, 0,
                dest, 4, 3, 0, 0, 3);

        float[] expected = new float[]{
                 3,  2,  1,  0,
                 7,  6,  5,  4,
                11, 10,  9,  8,
                15, 14, 13, 12
        };

        assertArrayEquals(expected, dest, 0);
    }

    @Test
    public void testCopy_flipXY() {
        float[] src = arrayRange(4*4);

        float[] dest = new float[4*4];

        copier.copy(src, 4, 0, 0,
                dest, 4, 3, 3, 0, 0);

        float[] expected = new float[]{
                15, 14, 13, 12,
                11, 10,  9,  8,
                 7,  6,  5,  4,
                 3,  2,  1,  0
        };

        assertArrayEquals(expected, dest, 0);
    }

    @Test
    public void testCopy_subregion() {
        float[] src = arrayRange(4*4);

        float[] dst = new float[4*4];

        // Copy 2x3 region from bottom-right of src to top-middle of dst
        copier.copy(src, 4, 2, 1,
                    dst, 4, 1, 0, 2, 2);

        float[] expected = new float[]{
                 0,  6,  7,  0,
                 0, 10, 11,  0,
                 0, 14, 15,  0,
                 0,  0,  0,  0
        };

        assertArrayEquals(expected, dst, 0);
    }

    @Test
    public void testCopy_subregion_flipX() {
        float[] src = arrayRange(4*4);

        float[] dst = new float[4*4];

        // Copy 2x3 region from bottom-right of src to top-middle of dst
        copier.copy(src, 4, 2, 1,
                dst, 4, 1, 0, 0, 2);

        float[] expected = new float[]{
                7,  6,  0,  0,
               11, 10,  0,  0,
               15, 14,  0,  0,
                0,  0,  0,  0
        };
        
        assertArrayEquals(expected, dst, 0);
    }

    protected void expectIAEx(String msg) {
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(msg);
    }

    @Test
    public void testCopyValidation_sxa_1() {
        expectIAEx("x was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, -1, 0,
                arrayRange(4*4), 4, 0, 0, 3, 3);
    }

    @Test
    public void testCopyValidation_sxa_2() {
        expectIAEx("x was out of range. x: 4, width: 4");

        copier.copy(
                arrayRange(4*4), 4, 1, 0,
                arrayRange(4*4), 4, 0, 0, 3, 3);
    }

    @Test
    public void testCopyValidation_sya() {
        expectIAEx("y was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, 0, -1,
                arrayRange(4*4), 4, 0, 0, 3, 3);
    }

    @Test
    public void testCopyValidation_dxa() {
        expectIAEx("x was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, 0, 0,
                arrayRange(4*4), 4, -1, 0, 2, 3);
    }

    @Test
    public void testCopyValidation_dya() {
        expectIAEx("y was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, 0, 0,
                arrayRange(4*4), 4, 0, -1, 3, 2);
    }

    @Test
    public void testCopyValidation_dxb() {
        expectIAEx("x was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, 0, 0,
                arrayRange(4*4), 4, 0, 0, -1, 3);
    }

    @Test
    public void testCopyValidation_dyb() {
        expectIAEx("y was negative: -1");

        copier.copy(
                arrayRange(4*4), 4, 0, 0,
                arrayRange(4*4), 4, 0, 0, 0, -1);
    }
}
