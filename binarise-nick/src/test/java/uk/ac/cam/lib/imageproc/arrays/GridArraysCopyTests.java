package uk.ac.cam.lib.imageproc.arrays;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class GridArraysCopyTests {

    public static class PublicGridArraysCopyTest extends BaseGridArraysCopyTest {
        @Before
        public void before() {
            this.copier = GridArrays::copy;
        }
    }

    public static class ElementwiseGridArraysCopyTest extends BaseGridArraysCopyTest {
        @Before
        public void before() {
            this.copier = GridArrays::copyElementwise;
        }
    }

    public static class ArrayCopyGridArraysCopyTest extends BaseGridArraysCopyTest {
        @Before
        public void before() {
            this.copier = GridArrays::copyWithArrayCopy;
        }

        @Test
        @Override
        public void testCopy_flipX() {
            expectIAEx("Reversed destination X axis is not supported");

            super.testCopy_flipX();
        }

        @Test
        @Override
        public void testCopy_flipXY() {
            expectIAEx("Reversed destination X axis is not supported");

            super.testCopy_flipXY();
        }

        @Test
        @Override
        public void testCopy_subregion_flipX() {
            expectIAEx("Reversed destination X axis is not supported");

            super.testCopy_subregion_flipX();
        }

        @Test
        @Ignore("Requires flipped X which is not supported")
        @Override
        public void testCopyValidation_dxb() {
        }
    }
}
