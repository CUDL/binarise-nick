package uk.ac.cam.lib.imageproc.objectlabel

import spock.lang.Specification
import uk.ac.cam.lib.imageproc.ImageValues
import uk.ac.cam.lib.imageproc.arrays.Array2D

import java.util.function.Predicate

import static uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter.Connectivity.FULL


class DefaultImageObjectFilterSpec extends Specification {

    private static final byte B = ImageValues.BLACK_1BIT
    private static final byte W = ImageValues.WHITE_1BIT

    RegionLabelFilter<byte[]> labeler = new DefaultRegionLabelFilter(FULL)

    def "should delegate to region label filter"() {
        given:
        RegionLabelFilter<byte[]> rlf =
                Spy(DefaultRegionLabelFilter, constructorArgs: [FULL, (byte)1])
        def iof = new DefaultImageObjectFilter(rlf, { true })

        when:
        iof.apply(Array2D.allocate(byte[], 3, 3))

        then:
        1 * rlf.apply(*_)
    }

    def "should invoke predicate on each detected object"() {
        given:
        Predicate<ImageObjects.ImageObject> pred = Mock()
        pred.test(_) >> true

        def iof = new DefaultImageObjectFilter(labeler, pred)

        when:
        iof.apply(Array2D.withData(byte[], (10..<19) as byte[], 3))

        then:
        9 * pred.test(_)
    }

    def "should not render excluded objects"() {
        given: "a filter which excludes connected regions with less than 4 pixels"
        Array2D<byte[]> img = Array2D.withData(byte[], [
                B, B, W, B, B, B,
                B, W, W, W, W, W,
                W, W, W, W, B, B,
                W, B, B, W, W, B,
                W, B, B, W, W, B,
                B, W, W, W, W, W,
        ] as byte[], 6)

        Predicate<ImageObjects.ImageObject> pred = { obj ->
            obj.area >= 4
        }
        def iof = new DefaultImageObjectFilter(labeler, pred)

        when: "the filter is applied to an image"
        def result = iof.apply(img)

        then: "the regions with less than 3 pixels are not present in the output"
        result == Array2D.withData(byte[], [
                W, W, W, W, W, W,
                W, W, W, W, W, W,
                W, W, W, W, B, B,
                W, B, B, W, W, B,
                W, B, B, W, W, B,
                B, W, W, W, W, W,
        ] as byte[], 6)
    }
}
