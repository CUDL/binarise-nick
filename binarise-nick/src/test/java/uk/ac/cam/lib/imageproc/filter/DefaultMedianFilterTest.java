package uk.ac.cam.lib.imageproc.filter;


import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import uk.ac.cam.lib.imageproc.arrays.Array2D;
import uk.ac.cam.lib.imageproc.arrays.ReflectSamplingMethod;

public class DefaultMedianFilterTest {

    @Test
    public void median() {
        DefaultMedianFilter mf =
                new DefaultMedianFilter(3, 3, new ReflectSamplingMethod());

        Array2D<float[]> median = mf.apply(
                Array2D.withData(float[].class, new float[]{1, 2, 3,
                                                            4, 5, 6,
                                                            7, 8, 9}, 3),
                Array2D.allocate(float[].class, 3, 3));

        /*
        5 4 5
        2 1 2
        5 4 5

        1 2 2 4 4 5 5 5 5
         */
        Assert.assertThat(median.data()[0], CoreMatchers.equalTo(4f));

        // median of 1..9
        Assert.assertThat(median.data()[4], CoreMatchers.equalTo(5f));
    }
}
