package uk.ac.cam.lib.imageproc

import spock.lang.Specification


class ImageValuesSpec extends Specification {

    def unsignedByteToInt(byte b) {
        return b & 0xFF
    }

    def unsignedMaxValueForBits(n) {
        return 2**n - 1
    }

    def "1 bit black is 0x00"() {
        expect: "Black in a 1-bit image is 0 -- no intensity "
        unsignedByteToInt(ImageValues.BLACK_1BIT) == 0
        unsignedByteToInt(ImageValues.BLACK_1BIT) == 0x00
    }

    def "1 bit white is 0x01"() {
        expect: "Black in a 1-bit image is 0x01 -- full intensity"
        unsignedByteToInt(ImageValues.WHITE_1BIT) == 1
        unsignedByteToInt(ImageValues.WHITE_1BIT) == 0x01
        unsignedByteToInt(ImageValues.WHITE_1BIT) == unsignedMaxValueForBits(1)
    }

    def "8 bit black is 0x00"() {
        expect: "Black in a 8-bit image is 0x00 -- no intensity "
        unsignedByteToInt(ImageValues.BLACK_8BIT) == 0
        unsignedByteToInt(ImageValues.BLACK_8BIT) == 0x00
    }

    def "8 bit white is 0xFF"() {
        expect: "Black in an 8-bit image is 0xFF -- full intensity"
        unsignedByteToInt(ImageValues.WHITE_8BIT) == 255
        unsignedByteToInt(ImageValues.WHITE_8BIT) == 0xFF
        unsignedByteToInt(ImageValues.WHITE_8BIT) == unsignedMaxValueForBits(8)
    }
}
