package uk.ac.cam.lib.imageproc.objectlabel

import spock.lang.Specification
import uk.ac.cam.lib.imageproc.arrays.Array2D

class CompressLabelRangeTest extends Specification {

    def "compressLabelRange compresses label range"() {
        when:
        RegionLabelFilter.compressLabelRange(input).compressedLabelImage

        then:
        input == expected

        where:
        [input, expected] << expectedLabelCompression
    }

    def getExpectedLabelCompression() {
        def inputs = []
        def expectations = []

        inputs.add Array2D.withData(int[], [] as int[], 0)
        expectations.add Array2D.withData(int[], [] as int[], 0)

        inputs.add Array2D.withData(int[], [
                9, 0, 7, 7,
                9, 3, 3, 0,
                0, 1, 3, 0
        ] as int[], 4)
        expectations.add Array2D.withData(int[], [
                4, 0, 3, 3,
                4, 2, 2, 0,
                0, 1, 2, 0
        ] as int[], 4)

        inputs.add Array2D.withData(int[], [
                1,  0,  5,  9,
                0,  5,  0,  5,
                14,  0,  5,  0,
                18, 19,  0,  23
        ] as int[], 4)
        expectations.add Array2D.withData(int[], [
                1,  0,  2,  3,
                0,  2,  0,  2,
                4,  0,  2,  0,
                5,  6,  0,  7
        ] as int[], 4)

        return [inputs, expectations].transpose()
    }
}
