package uk.ac.cam.lib.imageproc.objectlabel;


import org.junit.Test;
import uk.ac.cam.lib.imageproc.arrays.Array2D;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter.Connectivity.CROSS;
import static uk.ac.cam.lib.imageproc.objectlabel.DefaultRegionLabelFilter.Connectivity.FULL;

public class DefaultRegionLabelFilterTest {

    @Test
    public void testLabeling1() {
        Array2D<byte[]> img = Array2D.withData(byte[].class, new byte[]{
                1, 0, 1, 2,
                0, 1, 0, 1,
                1, 0, 1, 0,
                2, 1, 0, 1
        }, 4);

        Array2D<int[]> labels = Array2D.withData(int[].class, new int[]{
                1,  0,  2,  3,
                0,  4,  0,  5,
                6,  0,  7,  0,
                8,  9,  0, 10
        }, 4);

        assertImgIsLabeledAs(
                img, labels, new DefaultRegionLabelFilter(CROSS, (byte)0));
    }

    @Test
    public void testLabeling2() {
        Array2D<byte[]> img = Array2D.withData(byte[].class, new byte[]{
                1, 1, 1, 2,
                0, 1, 0, 1,
                1, 0, 1, 1,
                2, 1, 0, 1
        }, 4);

        Array2D<int[]> labels = Array2D.withData(int[].class, new int[]{
                1,  1,  1,  2,
                0,  1,  0,  3,
                4,  0,  3,  3,
                5,  6,  0,  3
        }, 4);

        assertImgIsLabeledAs(
                img, labels, new DefaultRegionLabelFilter(CROSS, (byte)0));
    }

    @Test
    public void testLabeling3() {
        Array2D<byte[]> img = Array2D.withData(byte[].class, new byte[]{
                1, 1, 1, 1,
                2, 0, 0, 1,
                1, 1, 1, 1,
                0, 0, 0, 0
        }, 4);

        Array2D<int[]> labels = Array2D.withData(int[].class, new int[]{
                1,  1,  1,  1,
                2,  0,  0,  1,
                1,  1,  1,  1,
                0,  0,  0,  0
        }, 4);

        assertImgIsLabeledAs(
                img, labels, new DefaultRegionLabelFilter(CROSS, (byte)0));
    }

    @Test
    public void testLabeling4() {
        // Spiral
        Array2D<byte[]> img = Array2D.withData(byte[].class, new byte[]{
                2, 2, 2, 2, 2, 2, 2, 2, 2,
                1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 0, 0, 0, 0, 0, 0, 0,
                1, 0, 1, 1, 1, 1, 1, 1, 1,
                1, 0, 1, 0, 0, 0, 0, 0, 1,
                1, 0, 1, 0, 1, 1, 1, 0, 1,
                1, 0, 1, 0, 1, 0, 1, 0, 1,
                1, 0, 1, 0, 0, 0, 1, 0, 1,
                1, 0, 1, 1, 1, 1, 1, 0, 1,
                1, 0, 0, 0, 0, 0, 0, 0, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1
        }, 9);

        Array2D<int[]> labels = Array2D.withData(int[].class, new int[]{
                1, 1, 1, 1, 1, 1, 1, 1, 1,
                2, 2, 2, 2, 2, 2, 2, 2, 2,
                2, 0, 0, 0, 0, 0, 0, 0, 0,
                2, 0, 2, 2, 2, 2, 2, 2, 2,
                2, 0, 2, 0, 0, 0, 0, 0, 2,
                2, 0, 2, 0, 2, 2, 2, 0, 2,
                2, 0, 2, 0, 2, 0, 2, 0, 2,
                2, 0, 2, 0, 0, 0, 2, 0, 2,
                2, 0, 2, 2, 2, 2, 2, 0, 2,
                2, 0, 0, 0, 0, 0, 0, 0, 2,
                2, 2, 2, 2, 2, 2, 2, 2, 2
        }, 9);

        assertImgIsLabeledAs(
                img, labels, new DefaultRegionLabelFilter(CROSS, (byte) 0));

    }

    @Test
    public void testLabeling5() {
        Array2D<byte[]> img = Array2D.withData(byte[].class, new byte[]{
                1, 1, 1, 1,
                2, 0, 0, 1,
                0, 1, 2, 1,
                1, 0, 0, 2
        }, 4);

        Array2D<int[]> labels = Array2D.withData(int[].class, new int[]{
                1,  1,  1,  1,
                2,  0,  0,  1,
                0,  3,  4,  1,
                3,  0,  0,  4
        }, 4);

        assertImgIsLabeledAs(
                img, labels, new DefaultRegionLabelFilter(FULL, (byte)0));
    }

    private void assertImgIsLabeledAs(
            Array2D<byte[]> img, Array2D<int[]> labels,
            RegionLabelFilter<byte[]> rlf) {

        Array2D<int[]> actual = RegionLabelFilter.compressLabelRange(
                rlf.apply(img)).getCompressedLabelImage();

        assertThat(actual, is(equalTo(labels)));
    }
}
