package uk.ac.cam.lib.imageproc.arrays;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.*;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;

@RunWith(DataProviderRunner.class)
public class Array2DTest {

    int width, height;
    float[] data;
    Array2D<float[]> array;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        width = 5;
        height = 4;
        data = new float[width * height];
        for(int i = 0; i < width * height; ++i)
            data[i] = i * 2;

        array = Array2D.withData(float[].class, data, width);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ctorThrowsIfDataNotMultipleOfWidth() {
        Array2D.withData(float[].class, new float[width * height - 1], width);
    }

    @Test
    public void width() {
        assertThat(array.width(), is(width));
    }

    @Test
    public void height() {
        assertThat(array.height(), is(height));
    }

    @Test
    public void dataArrayNotCopied() {
        assertThat(array.data(), is(sameInstance(data)));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOfNegativeXThrows() throws Exception {
        array.indexOf(-1, 0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOfXOutOfBoundsThrows() throws Exception {
        array.indexOf(width, 0);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOfNegativeYThrows() throws Exception {
        array.indexOf(0, -1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOfYOutOfBoundsThrows() throws Exception {
        array.indexOf(0, height);
    }

    @Test
    public void indexOf_a() {
        assertThat(array.indexOf(0, 0), is(0));
    }

    @Test
    public void indexOf_b() {
        assertThat(array.indexOf(3, 2), is(width * 2 + 3));
    }


    @Test
    public void sampleAt() throws Exception {
        assertThat(array.get(0, 0, Float.TYPE), is(0f));
        assertThat(array.get(1, 0, Float.TYPE), is(2f));
        assertThat(array.get(3, 0, Float.TYPE), is(6f));
    }

    @DataProvider
    public static List<List<Object>> testGetData() {
        Array2D<String[]> strings = Array2D.withData(String[].class, new String[]{
                "abc", "def", "123",
                "foo", "bar", "baz"
        }, 3);

        Class<String> strCls = String.class;

        return asList(
                asList(strings, 0, 0, strCls, "abc"),
                asList(strings, 1, 0, strCls, "def"),
                asList(strings, 2, 0, strCls, "123"),
                asList(strings, 0, 1, strCls, "foo"),
                asList(strings, 1, 1, strCls, "bar"),
                asList(strings, 2, 1, strCls, "baz"));
    }

    @Test
    @UseDataProvider("testGetData")
    public void testGet(Array2D<?> array, int x, int y, Class<?> elementType,
                        Object expectedValue) {
        assertThat(array.get(x, y, elementType), is(expectedValue));
    }

    @Test
    public void testGetOnPrimitiveArrayRequiresPrimitiveClass() {
        Array2D<byte[]> byteArray = Array2D.withData(
                byte[].class, new byte[]{1}, 1);

        assertThat(byteArray.get(0, 0, Byte.TYPE), is((byte)1));

        // get() with Byte wrapper class, not primitive - won't match element
        // type of array.
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(startsWith(
                "Provided elementType class does not match the array's class"));
        assertThat(byteArray.get(0, 0, Byte.class), is((byte)1));
    }


    @Test
    public void testIterate() {
        String[] stringArray = new String[]{
                "abc", "def", "123",
                "foo", "bar", "baz"
        };

        Array2D<String[]> strings = Array2D.withData(
                String[].class, stringArray, 3);

        Iterator<String> it = strings.type().accessor(String.class)
                .iterate(strings.data());

        for(int i = 0; i < stringArray.length; ++i) {
            assertThat(it.hasNext(), is(true));
            assertThat(it.next(), is(stringArray[i]));
        }
        assertThat(it.hasNext(), is(false));

        // Ensure it throws when empty
        thrown.expect(NoSuchElementException.class);
        it.next();
    }
}