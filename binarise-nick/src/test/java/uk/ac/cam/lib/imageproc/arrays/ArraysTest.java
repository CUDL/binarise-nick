package uk.ac.cam.lib.imageproc.arrays;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;


@RunWith(DataProviderRunner.class)
public class ArraysTest {
    @Test
    public void sum_1() throws Exception {
        assertThat(Arrays.sum(new float[]{1}),
                is(1f));
    }

    @Test
    public void sum_2() throws Exception {
        assertThat(Arrays.sum(new float[]{1, 2, 3}),
                is(equalTo(6f)));
    }

    @Test
    public void mean_1() throws Exception {
        assertThat(Arrays.mean(new float[]{1}),
                is(equalTo(1f)));
    }

    @Test
    public void mean_2() throws Exception {
        assertThat(Arrays.mean(new float[]{1, 2, 3}),
                is(equalTo(2f)));
    }

    @Test
    public void mean_3() throws Exception {
        assertThat(Arrays.mean(new float[]{1, 2}),
                is(equalTo(1.5f)));
    }

    @Test
    public void square_1() throws Exception {
        assertThat(Arrays.square(new float[]{1}),
                is(new float[]{1}));
    }

    @Test
    public void square_2() throws Exception {
        assertThat(Arrays.square(new float[]{1, 3, 10}),
                is(equalTo(new float[]{1, 9, 100})));
    }

    @Test
    public void medianInPlace_1() throws Exception {
        assertThat(Arrays.medianInPlace(new float[]{1, 3, 10}),
                is(equalTo(3f)));
    }

    @Test
    public void medianInPlace_2() throws Exception {
        assertThat(Arrays.medianInPlace(new float[]{1, 1, 2, 2}),
                is(equalTo(1.5f)));
    }

    @Test
    public void medianInPlace_3() throws Exception {
        assertThat(Arrays.medianInPlace(new float[]{1, 2, 3, 4, 5, 6, 8, 9}),
                is(equalTo(4.5f)));
    }

    @Test
    public void medianInPlace_4() throws Exception {
        assertThat(Arrays.medianInPlace(new float[]{1, 3, 3, 6, 7, 8, 9}),
                is(equalTo(6f)));
    }

    @Test
    @UseDataProvider(value="randomFloatArrays",
                     location=ArraysTestUtils.class)
    public void testSelectionBasedMedianInPlaceAgainstNormalMedianInPlace(
            float[] randomFloats) {
        float[] normalMedianInput = randomFloats.clone();

        float expected = Arrays.medianInPlace(normalMedianInput);
        float actual = Arrays.medianInPlace_selection(randomFloats);

        assertThat(actual, is(expected));
    }

    public static final class ArraysTestUtils {

        private ArraysTestUtils() {}

        private static final int TEST_SIZES = 50;
        private static final int TEST_TRIALS_PER_SIZE = 5;
        private static final int TEST_ARRAY_MAX_SIZE = 10000;

        @DataProvider
        public static List<List<Object>> invalidArrayRangesAndIndexes() {
            float[] array = new float[10];
            return asList(
                asList("left was negative", array.clone(), -1, 0, 9),
                asList(" was < left", array.clone(), 3, 2, 9),
                asList("right was < ", array.clone(), 3, 8, 7),
                asList("right was >= length", array.clone(), 3, 8, 10)
            );
        }

        @DataProvider
        public static List<List<Object>> invalidArrayRangesAndTwoIndexes() {
            float[] array = new float[10];
            return asList(
                    asList("left was negative", array.clone(), -1, 0, 1, 9),
                    asList("a was < left", array.clone(), 3, 2, 3, 9),
                    asList("b was < a", array.clone(), 3, 4, 3, 9),
                    asList("right was < b", array.clone(), 3, 4, 8, 7),
                    asList("right was >= length", array.clone(), 3, 4, 8, 10)
            );
        }

        private static float[] randomFloats(int count, Random r) {
            float[] f = new float[count];
            for (int i = 0; i < f.length; ++i)
                f[i] = r.nextFloat();
            return f;
        }

        @DataProvider(format="%m[%i: [...]]")
        public static List<float[]> randomFloatArrays() {
            return randomFloatArrays(new Random(1234));
        }

        public static List<float[]> randomFloatArrays(Random r) {
            List<float[]> arrays = new ArrayList<>();

            for(int size = 0; size < TEST_SIZES; ++size) {
                for(int trial = 0; trial < TEST_TRIALS_PER_SIZE; ++trial) {
                    int length = (int)Math.max(
                            1, (size / (float) TEST_SIZES) * TEST_ARRAY_MAX_SIZE);

                    arrays.add(randomFloats(length, r));
                }
            }
            return arrays;
        }

        @DataProvider(format="%m[%i: [...], %p[1..-1]]")
        public static List<List<Object>> arrayWithRangeAndOneIndex() {
            return arrayWithRangeAndOneIndex(new Random(1234));
        }

        public static List<List<Object>> arrayWithRangeAndOneIndex(Random r) {

            List<List<Object>> params = new ArrayList<>();

            for(int size = 0; size < TEST_SIZES; ++size) {
                for(int trial = 0; trial < TEST_TRIALS_PER_SIZE; ++trial) {
                    int length = (int) Math.max(
                            1, (size / (float) TEST_SIZES) * TEST_ARRAY_MAX_SIZE);

                    float[] f = randomFloats(length, r);
                    int left = r.nextInt(length);
                    int partition = left + r.nextInt(length - left);
                    int right = partition + r.nextInt(length - partition);

                    params.add(asList(f, left, partition, right));
                }
            }

            return params;
        }

        @DataProvider(format="%m[%i: [...], %p[1..-1]]")
        public static List<List<Object>> arrayWithRangeAndTwoIndexes() {
            Random r = new Random(5678);

            return arrayWithRangeAndOneIndex(r).stream().map(params -> {
                params = new ArrayList<>(params);

                int a = (int)params.get(2);
                int right = (int)params.get(3);

                int b = a + r.nextInt((right - a) + 1);

                params.add(3, b);

                return params;
            }).collect(Collectors.toList());
        }

        public static void ensureExcludedArrayAreaNotModified(
                float[] actual, int left, int right, float[] expected) {

            assert actual.length == expected.length;

            for(int i = 0; i < left; ++i) {
                assertThat(actual[i], is(expected[i]));
            }

            for(int i = right + 1; i < actual.length; ++i) {
                assertThat(actual[i], is(expected[i]));
            }
        }
    }

    @RunWith(DataProviderRunner.class)
    public static class PartitionTest {

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        @UseDataProvider(value="invalidArrayRangesAndIndexes",
                         location=ArraysTestUtils.class)
        public void testErrorHandling(
                String msg, float[] items, int left, int i, int right) {
            thrown.expect(IllegalArgumentException.class);
            thrown.expectMessage(msg);

            Arrays.partition(items, i, left, right);
        }

        @Test
        @UseDataProvider(value="arrayWithRangeAndOneIndex",
                         location = ArraysTestUtils.class)
        public void testPartition(
                float[] items, int left, int partition, int right) {

            float[] itemsBeforePartition = items.clone();

            final float pval = items[partition];

            int partitioned = Arrays.partition(items, partition, left, right);

            assertThat(partitioned, is(greaterThanOrEqualTo(left)));
            assertThat(partitioned, is(lessThanOrEqualTo(right)));
            assertThat(items[partitioned], is(pval));

            // Check lower partition invariant
            for(int i = left; i < partitioned; ++i) {
                assertThat(items[i], Matchers.lessThan(pval));
            }

            for(int i = partitioned + 1; i <= right; ++i) {
                assertThat(items[i], greaterThanOrEqualTo(pval));
            }

            ArraysTestUtils.ensureExcludedArrayAreaNotModified(
                    items, left, right, itemsBeforePartition);
        }
    }

    @RunWith(DataProviderRunner.class)
    public static class SelectTest {

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        @UseDataProvider(value="invalidArrayRangesAndIndexes",
                location=ArraysTestUtils.class)
        public void testErrorHandling(
                String msg, float[] items, int left, int i, int right) {
            thrown.expect(IllegalArgumentException.class);
            thrown.expectMessage(msg);

            Arrays.select(items, i, left, right);
        }

        @Test
        @UseDataProvider(value="arrayWithRangeAndOneIndex",
                location = ArraysTestUtils.class)
        public void testSelect(
                float[] items, int left, int partition, int right) {

            float[] itemsBeforeSelect = items.clone();

            float[] itemsSorted = items.clone();
            java.util.Arrays.sort(itemsSorted, left, right + 1);

            Arrays.select(items, partition, left, right);

            // Check that the ith item was correctly identified
            assertThat(items[partition], is(itemsSorted[partition]));

            // Check the invariant that elements before the ith position are
            // less or equal to than i. Note that this is slightly different to
            // parition()'s invariant in that items before the ith may be equal
            // to the ith. The simplest example being an array of identical
            // values.
            for(int i = left; i < partition; ++i) {
                assertThat(items[i], lessThanOrEqualTo(items[partition]));
            }

            for(int i = partition + 1; i <= right; ++i) {
                assertThat(items[i], greaterThanOrEqualTo(items[partition]));
            }

            ArraysTestUtils.ensureExcludedArrayAreaNotModified(
                    items, left, right, itemsBeforeSelect);
        }
    }

    @RunWith(DataProviderRunner.class)
    public static class SelectRangeTest {

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        @UseDataProvider(value="invalidArrayRangesAndTwoIndexes",
                         location=ArraysTestUtils.class)
        public void testErrorHandling(
                String msg, float[] items, int left, int a, int b, int right) {
            thrown.expect(IllegalArgumentException.class);
            thrown.expectMessage(msg);

            Arrays.selectRange(items, a, b, left, right);
        }

        @Test
        @UseDataProvider(value="arrayWithRangeAndTwoIndexes",
                         location=ArraysTestUtils.class)
        public void testSelectRange(
                float[] items, int left, int a, int b, int right) {

            float[] itemsBeforeSelect = items.clone();

            float[] itemsSorted = items.clone();
            java.util.Arrays.sort(itemsSorted, left, right + 1);

            Arrays.selectRange(items, a, b, left, right);

            // Check that the items at index a and b were correctly identified
            assertThat(items[a], is(itemsSorted[a]));
            assertThat(items[b], is(itemsSorted[b]));

            // Check the ordering invariants for the three areas of the input
            for(int i = left; i < a; ++i) {
                assertThat(items[i], lessThanOrEqualTo(items[a]));
            }

            for(int i = a + 1; i < b; ++i) {
                assertThat(items[i], greaterThanOrEqualTo(items[a]));
                assertThat(items[i], lessThanOrEqualTo(items[b]));
            }

            for(int i = right + 1; i <= right; ++i) {
                assertThat(items[i], greaterThanOrEqualTo(items[b]));
            }

            ArraysTestUtils.ensureExcludedArrayAreaNotModified(
                    items, left, right, itemsBeforeSelect);
        }
    }
}