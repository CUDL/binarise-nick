package uk.ac.cam.lib.imageproc.arrays;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


public abstract class BaseReflectSamplingMethodTest {

    public static class GridArrayReflectSamplingMethodTest
            extends BaseReflectSamplingMethodTest {

        @Override
        protected SamplingMethod<float[]> createSamplingMethod() {
            return new GridArrayReflectSamplingMethod();
        }
    }

    public static class ReflectSamplingMethodTest
            extends BaseReflectSamplingMethodTest {

        @Override
        protected SamplingMethod<float[]> createSamplingMethod() {
            return new ReflectSamplingMethod();
        }
    }

    Array2D<float[]> image;
    SamplingMethod<float[]> rsm;

    @Before
    public void setUp() {
        float[] pixels = new float[100];
        for(int i = 0; i < pixels.length; i++)
            pixels[i] = i;

        image = Array2D.withData(float[].class, pixels, 10);
        rsm = createSamplingMethod();
    }

    protected abstract SamplingMethod<float[]> createSamplingMethod();

    @Test
    public void sampleInRange() {
        assertThat(rsm.sample(image, 3, 3, 4, 3), equalTo(Array2D.withData(
            float[].class,
            // 1, 2 is the top-left corner
            new float[]{
                21, 22, 23, 24,
                31, 32, 33, 34,
                41, 42, 43, 44
            }, 4)));
    }

    @Test
    public void sampleReflected1() {
        assertThat(rsm.sample(image, 0, 0, 4, 3), equalTo(Array2D.withData(
                float[].class,
                // 1, 2 is the top-left corner
                new float[]{
                        12, 11, 10, 11,
                        2,  1,  0,  1,
                        12, 11, 10, 11
                }, 4)));
    }

    @Test
    public void sampleReflected2() {
        assertThat(rsm.sample(image, 9, 9, 5, 3), equalTo(Array2D.withData(
                float[].class,
                // 7, 8 is the top-left corner
                new float[]{
                        87, 88, 89, 88, 87,
                        97, 98, 99, 98, 97,
                        87, 88, 89, 88, 87
                }, 5)));
    }
}