package uk.ac.cam.lib.imageproc.binarise.cmdline

import groovy.transform.PackageScope
import org.docopt.Docopt
import uk.ac.cam.lib.imageproc.LengthUnit
import uk.ac.cam.lib.imageproc.arrays.GridArrayReflectSamplingMethod
import uk.ac.cam.lib.imageproc.arrays.SamplingMethod
import uk.ac.cam.lib.imageproc.filter.binarise.ParallelNickBinarisationMethod
import uk.ac.cam.lib.imageproc.filter.binarise.SchnitzBinarisationMethod
import uk.ac.cam.lib.imageproc.filter.binarise.SchnitzBinarisationMethod.Builder
import uk.ac.cam.lib.imageproc.interop.awt.Array2DIoAwt

import javax.imageio.ImageIO
import java.awt.image.BufferedImage
import java.nio.charset.Charset
import java.util.concurrent.ForkJoinPool

import static java.lang.System.err
import static java.util.Objects.requireNonNull

/**
 * A command line interface to perform NICK binarisation.
 */
class CommandLineInterface {

    @PackageScope static final UNIT_SUFFIXES = [
        i: LengthUnit.INCHES,
        mm: LengthUnit.MILLIMETERS,
        cm: LengthUnit.CENTIMETERS,
        m: LengthUnit.METERS
    ].asImmutable()

    @PackageScope static Tuple2<Number, LengthUnit> parseLength(String length) {
        return parseLength(length, /^(\d*(?:\.\d*)?)(i|mm|cm|m)$/)
    }

    @PackageScope
    static Tuple2<Number, LengthUnit> parseLength(String length, String regex) {
        def match = length =~ regex
        if(!match || !match.group(1).isBigDecimal())
            throw new IllegalArgumentException(
                    "not a valid length unit: $length")

        return new Tuple2<>(
                match.group(1) as BigDecimal, UNIT_SUFFIXES[match.group(2)])
    }

    @PackageScope
    static Tuple2<Integer, Integer> parseWindowSize(String windowSpec) {
        def match = windowSpec =~ /^(\d+)?(?:x(\d+))?$/
        if(!match)
            throw new IllegalArgumentException(
                    "not a valid window size: $windowSpec")

        def values = (1..2).collect { i -> match[0][i] as Integer }.findAll()
        return values * (3 - values.size())
    }

    @PackageScope static parser(Map<String, Object> args) {
        return { name, closure ->
            if(!(args.containsKey(name))) {
                def argNames = args.keySet().sort().join(", ")
                throw new IllegalArgumentException(
                        "no such argument: $name. " +
                        "Available arguments: ${argNames}")
            }

            try {
                return closure(args[name])
            }
            catch(IllegalArgumentException e) {
                exit(1, "Error parsing $name: ${e.message}")
            }
        }
    }

    @PackageScope static parseResolution(res) {
        parseLength(res, /^(.*)dp(i|mm|cm|m)$/)
    }

    @PackageScope static parseFixedValueOr(Map<String, Object> fixedValues, closure) {
        return { value ->
            if(fixedValues.containsKey(value))
                return fixedValues[value]
            return closure(value)
        }
    }

    @PackageScope static parseRange(value) {
        def match = value =~ /^(.+)?:(.+)?$/
        if(!match)
            throw new IllegalArgumentException("not a valid range: $value")

        try {
            return (1..2).collect { i ->
                def val = match[0][i]
                val == null ? null : parseLength(val)
            } as Tuple2
        }
        catch(IllegalArgumentException e) {
            throw new IllegalArgumentException(
                    "not a valid range: $value - ${e.message}", e)
        }
    }

    @PackageScope static parseDecimal(String value) {
        if(!value.isBigDecimal())
            throw new IllegalArgumentException("not a decimal number: $value")
        return value as BigDecimal
    }

    static class SystemExit extends RuntimeException {
        final int exitStatus

        SystemExit(int status) {
            this.exitStatus = status
        }

        def doExit() {
            System.exit(exitStatus)
        }
    }

    @PackageScope static void exit(int status, String message=null) {
        if(message)
            throw new SystemExitWithMessage(status, message)
        throw new SystemExit(status)
    }

    static class SystemExitWithMessage extends SystemExit {
        final String message

        SystemExitWithMessage(status, message) {
            super(status)
            this.message = requireNonNull(message)
        }

        def doExit() {
            err.println(message)
            super.doExit()
        }
    }

    static void main(String[] argv) {
        try {
            CommandLineInterface.fromArguments(argv).run()
        }
        catch(SystemExit exit) {
            exit.doExit()
        }
    }

    /**
     * Create a CommandLineInterface instance by parsing the provided arguments
     * according to the format specified in the {@code usage.txt} resource file
     * in this package.
     *
     * <p>This method throws a {@link SystemExit} object if the argument array
     * contains unparsable arguments.
     *
     * @param argv The command line argument list
     * @return A cli object paramertised by the arguments
     */
    static CommandLineInterface fromArguments(String[] argv) {

        def args = new Docopt(
                CommandLineInterface.class.getResourceAsStream("usage.txt"),
                Charset.forName("UTF-8"))
                .parse(argv)

        def parseArg = parser(args)

        def resolution = parseArg("--resolution", this.&parseResolution)
        def nickK = parseArg("--nick-k", this.&parseDecimal)
        def nickWindow = parseArg("--nick-size", this.&parseWindowSize)
        def denoiseWindow = parseArg(
                "--denoise",
                parseFixedValueOr([none: null], this.&parseWindowSize))
        def objectAreaRange = parseArg("--filter-objects", this.&parseRange)

        def src = new File(args["<src-image>"] as String)
        def dest = new File(args["<output>"] as String)

        return new CommandLineInterface(
                src, dest, resolution, denoiseWindow, nickK, nickWindow,
                objectAreaRange, ForkJoinPool.commonPool())
    }

    final File src
    final File dest

    final Tuple2<Number, LengthUnit> resolution

    final Tuple2<Integer, Integer> medianFilterWindow

    final float nickK
    final Tuple2<Integer, Integer> nickWindow

    final Tuple2<Tuple2<Number, LengthUnit>,
                         Tuple2<Number, LengthUnit>> objectAreaRange

    final ForkJoinPool pool

    private static void handleIOError(String msg, IOException e, int status) {
        exit(status, msg + e)
    }

    CommandLineInterface(
            File src, File dest, Tuple2<Number, LengthUnit> resolution,
            Tuple2<Integer, Integer> medianFilterWindow, Number nickK,
            Tuple2<Integer, Integer> nickWindow,
            Tuple2<Tuple2<Number, LengthUnit>, Tuple2<Number, LengthUnit>>
                    objectAreaRange, ForkJoinPool pool) {

        this.src = requireNonNull(src)
        this.dest = requireNonNull(dest)
        this.resolution = requireNonNull(resolution)
        this.medianFilterWindow = requireNonNull(medianFilterWindow)
        this.nickK = nickK
        this.nickWindow = requireNonNull(nickWindow)
        this.objectAreaRange = requireNonNull(objectAreaRange)
        this.pool = requireNonNull(pool)
    }

    SamplingMethod<float[]> getSamplingMethod() {
        return new GridArrayReflectSamplingMethod()
    }

    SchnitzBinarisationMethod getBinariser() {
        def builder = SchnitzBinarisationMethod.builder()

        builder.withHsvMedianFilter(
                medianFilterWindow.first, medianFilterWindow.second,
                samplingMethod)

        builder.withBinarisationMethod(new ParallelNickBinarisationMethod(
                nickWindow.first, nickWindow.second, samplingMethod,
                nickK, pool))

        configureObjectExclusion(builder)
        return builder.build()
    }

    def getLengthAs(LengthUnit dest, Tuple2<Number, LengthUnit> value) {
        return value.second.to(dest, value.first)
    }

    def configureObjectExclusion(Builder b) {
        if(!(objectAreaRange.first || objectAreaRange.second))
            return

        def unit = resolution.second
        def (min, max) = objectAreaRange.collect { length ->
            if(length == null)
                return OptionalDouble.empty()
            return OptionalDouble.of(getLengthAs(unit, length))
        }

        b.excludeSmallAndLargeObjectsByArea(
                resolution.first, resolution.second, min, max, unit)
    }

    def getOutputFormat() {
        dest.name.find(/\.(\w+)$/) { _, ext -> ext } ?: "png"
    }

    void run() {
        BufferedImage img = null
        try {
            img = ImageIO.read(src)
            if(!img)
                exit(2, "Unable to load source src image: " +
                        "format not supported by java.awt")
        }
        catch (IOException e) {
            handleIOError("Unable to read input image: ", e, 1)
        }

        def io = new Array2DIoAwt()
        def rgb = io.getNormalisedChannels(img.data)

        def binarised = binariser.applyWithSingleOutput(rgb)

        try {
            if(!ImageIO.write(Array2DIoAwt.binaryImage(binarised),
                              outputFormat, dest))
                exit(3, "Unable to write output image: format not supported " +
                        "by java.awt: $outputFormat")
        }
        catch (IOException e) {
            handleIOError("Unable to write output image: ", e, 2)
        }
    }
}
